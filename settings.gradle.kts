include(":android")
include(":support-annotations")
include(":support-processor")
include(":tools")
include(":core")
include(":desktop")

pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
        maven("https://dl.bintray.com/kotlin/kotlin-eap")
    }
}
