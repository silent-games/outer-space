package com.silentgames.support // ktlint-disable filename

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class EcsComponent
