package com.silentgames.tools

import com.badlogic.gdx.tools.texturepacker.TexturePacker

object AtlasGenerator {

    @JvmStatic
    fun main(args: Array<String>) {
        val settings = TexturePacker.Settings()
        settings.maxWidth = 2048
        settings.maxHeight = 2048
        TexturePacker.process(settings, "tools/assets/texture", "core/assets/atlas", "game")
        TexturePacker.process(settings, "tools/assets/ui", "core/assets/atlas", "ui")
    }
}
