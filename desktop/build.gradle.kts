import org.gradle.jvm.tasks.Jar
import org.jetbrains.kotlin.config.KotlinCompilerVersion
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(kotlin("stdlib", KotlinCompilerVersion.VERSION))

    implementation("com.badlogicgames.gdx:gdx-backend-lwjgl3:${properties["version.gdx"]}")
    implementation("com.badlogicgames.gdx:gdx-platform:${properties["version.gdx"]}:natives-desktop")
    implementation("com.badlogicgames.gdx:gdx-freetype-platform:${properties["version.gdx"]}:natives-desktop")

    implementation(project(":core"))
}

sourceSets {
    named("main") {
        java.setSrcDirs(listOf("src/main/kotlin"))
        resources.setSrcDirs(listOf("../core/assets"))
    }
}

project.extra["mainClassName"] = "com.silentgames.desktop.DesktopLauncher"
project.extra["assetsDir"] = File("../core/assets")

tasks.register<JavaExec>("run") {
    dependsOn("classes")
    mainClass.set(project.extra["mainClassName"] as String)
    classpath(project.the<SourceSetContainer>()["main"].runtimeClasspath)
    standardInput = System.`in`
    workingDir = project.extra["assetsDir"] as File
    isIgnoreExitValue = true
}

tasks.register<JavaExec>("debug") {
    dependsOn("classes")
    mainClass.set(project.extra["mainClassName"] as String)
    classpath(project.the<SourceSetContainer>()["main"].runtimeClasspath)
    standardInput = System.`in`
    workingDir = project.extra["assetsDir"] as File
    isIgnoreExitValue = true
    debug = true
}
tasks.register<Jar>("dist") {
    dependsOn("classes")
    manifest {
        attributes["Main-Class"] = project.extra["mainClassName"]
    }
    from(
        project.the<SourceSetContainer>()["main"].compileClasspath.map {
            if (it.isDirectory) it else zipTree(
                it
            )
        }
    )
    with(tasks["jar"] as CopySpec)
}
