package com.silentgames.desktop

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration
import com.silentgames.core.RenderConstants.VIRTUAL_H
import com.silentgames.core.RenderConstants.VIRTUAL_W
import com.silentgames.desktop.screen.DesktopOuterSpaceGame

object DesktopLauncher {

    @JvmStatic
    fun main(arg: Array<String>) {
        val config = Lwjgl3ApplicationConfiguration().apply {
            setWindowSizeLimits(VIRTUAL_W, VIRTUAL_H, -1, -1)
            setTitle("Outer Space")
        }
        Lwjgl3Application(DesktopOuterSpaceGame(), config)
    }
}
