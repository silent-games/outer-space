package com.silentgames.desktop.hud

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import com.kotcrab.vis.ui.VisUI
import com.kotcrab.vis.ui.widget.VisTextButton
import com.silentgames.core.ecs.component.HungerComponent
import com.silentgames.core.model.SelectedObjectData
import com.silentgames.core.ui.AppContainer
import com.silentgames.core.ui.Images
import com.silentgames.core.ui.get

class SelectedObjectWidget(uiSkin: Skin, private val onBteClick: () -> Unit) : Table(uiSkin) {

    private val padding = 10f

    private val btnDebugStyle: VisTextButton.VisTextButtonStyle by lazy {
        VisUI.getSkin().get(VisTextButton.VisTextButtonStyle::class.java)
    }

    init {
        isVisible = false
        align(Align.top)
        background = skin[Images.UI_BLOCK]
    }

    private val hungerLabelTitle = Label("Сытость", skin)
    private val hungerLabelValue = Label("", skin)

    private fun Table.addWidget(selectedObject: SelectedObjectData) {
        this.also {
            it.row().expandX()
            it.add(
                AppContainer(Label(selectedObject.name, skin)).apply {
                    pad(padding)
                    align(Align.left)
                    background = skin[Images.UI_BLOCK]
                }
            ).fillX()
            it.add(
                AppContainer(Label("id: ${selectedObject.id}", skin)).apply {
                    pad(padding)
                    align(Align.right)
                }
            ).fillX()
            it.add(
                AppContainer(Label(selectedObject.node.toString(), skin)).apply {
                    pad(padding)
                    align(Align.right)
                }
            ).fillX()
            if (selectedObject.showBteEditorButton) {
                it.add(
                    AppContainer(VisTextButton("BTE", btnDebugStyle)).apply {
                        pad(padding)
                        align(Align.right)
                        addListener(object : ClickListener() {
                            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                                onBteClick.invoke()
                            }
                        })
                    }
                ).fillX()
            }
            it.row().expand()
            it.add(
                Table(skin).apply {
                    add(Image(Texture(selectedObject.pixmap)))
                        .maxWidth(64f)
                        .maxHeight(64f)
                        .pad(padding)
                    add(getParamsTable(selectedObject.description)).pad(padding).top()
                }
            ).pad(5f).align(Align.left)
            it.row()
        }
    }

    private fun getParamsTable(description: String) =
        Table(skin).apply {
            add(Label(description, skin)).colspan(2)
            row()
            add(hungerLabelTitle).left()
            add(hungerLabelValue).left()
        }

    private fun Label.fillHunger(hungerComponent: HungerComponent) {
        val text = hungerComponent.satiety.toString()
        this.setText(text)
        hungerComponent.onSatietyChanged = {
            hungerLabelValue.setText("$it")
        }
    }

    fun update(selectedObject: SelectedObjectData) {
        val hungerComponent = selectedObject.hungerComponent
        clear()
        this.addWidget(selectedObject)
        if (hungerComponent != null) {
            hungerLabelValue.fillHunger(hungerComponent)
            hungerLabelTitle.isVisible = true
            hungerLabelValue.isVisible = true
        } else {
            hungerLabelTitle.isVisible = false
            hungerLabelValue.isVisible = false
        }
    }
}
