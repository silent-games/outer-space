package com.silentgames.desktop.hud

import com.badlogic.gdx.math.Vector2
import com.silentgames.core.model.ContentItem
import com.silentgames.core.screen.base.Context
import com.silentgames.core.screen.game.hud.ContentItemAction

class DesktopContentItemAction : ContentItemAction {

    private lateinit var contentMenu: ContentMenu

    override fun onAttachContext(context: Context) {
        this.contentMenu = ContentMenu(context.uiSkin)
        context.stage.addActor(contentMenu)
    }

    override fun create(
        screenPos: Vector2,
        actionList: List<ContentItem>,
        onClick: (ContentItem) -> Unit
    ) {
        contentMenu.create(screenPos, actionList, onClick)
    }

    override fun changeVisibility(visible: Boolean) {
        contentMenu.isVisible = visible
    }
}
