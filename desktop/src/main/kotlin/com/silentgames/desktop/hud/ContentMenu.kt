package com.silentgames.desktop.hud

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.silentgames.core.model.ContentItem
import com.silentgames.core.ui.AppContainer
import com.silentgames.core.ui.Images
import com.silentgames.core.ui.get

class ContentMenu(uiSkin: Skin) : Table(uiSkin) {

    init {
        top()
        left()
    }

    fun create(
        screenPosition: Vector2,
        actions: List<ContentItem>,
        onClick: (ContentItem) -> Unit
    ) {
        clearChildren()
        setPosition(screenPosition.x, screenPosition.y)
        actions.forEach { contentItem ->
            row()
            add(
                AppContainer(
                    Label(contentItem.action.description, skin)
                ).apply {
                    addListener(object : ClickListener() {
                        override fun clicked(event: InputEvent?, x: Float, y: Float) {
                            onClick(contentItem)
                        }
                    })
                    background = skin[Images.UI_BLOCK]
                    pad(6f)
                }
            ).fillX()
        }
    }
}
