package com.silentgames.desktop.hud

import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import com.kotcrab.vis.ui.building.OneColumnTableBuilder
import com.kotcrab.vis.ui.building.OneRowTableBuilder
import com.kotcrab.vis.ui.building.utilities.Padding
import com.kotcrab.vis.ui.widget.VisCheckBox
import com.kotcrab.vis.ui.widget.VisTextButton
import com.kotcrab.vis.ui.widget.VisWindow
import com.silentgames.core.model.SelectedObjectData
import com.silentgames.core.screen.base.Context
import com.silentgames.core.screen.game.hud.DebugHudEvent
import com.silentgames.core.screen.game.hud.GameHud
import com.silentgames.core.screen.game.hud.HudEvent
import com.silentgames.core.screen.game.hud.SelectedEntityHud
import ktx.actors.onClick

class DesktopGameHud(private val behaviourTreeEditor: BehaviourTreeEditor) :
    GameHud,
    SelectedEntityHud {

    private lateinit var stage: Stage
    private lateinit var inputMultiplexer: InputMultiplexer
    private lateinit var hudEvent: HudEvent
    private lateinit var debugHudEvent: DebugHudEvent

    private val table by lazy { Table() }

    private lateinit var selected: SelectedObjectWidget

    override fun onAttachContext(context: Context) {
        this.stage = context.stage
        this.selected = SelectedObjectWidget(context.uiSkin) {
            behaviourTreeEditor.showEditor(stage.root)
        }
    }

    override fun onAttachInputMultiplexer(inputMultiplexer: InputMultiplexer) {
        this.inputMultiplexer = inputMultiplexer
    }

    override fun onAttach() {
        stage.addActor(
            table.apply {
                setFillParent(true)
                row().grow()
                add(
                    OneRowTableBuilder(Padding(10f))
                        .append(
                            VisTextButton("Save").apply {
                                onClick {
                                    hudEvent.onSaveClick()
                                }
                            }
                        )
                        .append(
                            VisTextButton("Debug").apply {
                                onClick {
                                    stage.addDebugWindow()
                                }
                            }
                        ).build()
                ).align(Align.topLeft).maxWidth(150f).maxHeight(25f)
                add()
                add()
                row().grow()
                add()
                add()
                add()
                row().align(Align.bottomLeft)
                add(selected).minWidth(300f).minHeight(150f)
                add()
                add()
            }
        )
        inputMultiplexer.addProcessor(stage)
    }

    override fun onUpdate() {
        stage.render()
    }

    override fun onHide() {
        stage.clear()
        inputMultiplexer.removeProcessor(stage)
    }

    override fun onDetach() {
        onHide()
        stage.dispose()
    }

    private fun Stage.render() {
        act()
        draw()
    }

    override fun onEntityAdded(selectedObjectData: SelectedObjectData) {
        selected.update(selectedObjectData)
        selected.isVisible = true
    }

    override fun onEntityRemoved() {
        selected.isVisible = false
    }

    override fun processEvents(hudEvent: HudEvent) {
        this.hudEvent = hudEvent
    }

    override fun processDebugEvents(debugHudEvent: DebugHudEvent) {
        this.debugHudEvent = debugHudEvent
    }

    private fun Stage.addDebugWindow() {
        addActor(
            VisWindow("DEBUG").apply {
                add(
                    OneColumnTableBuilder()
                        .append(
                            VisCheckBox("Visual Path A*").apply {
                                onClick {
                                    debugHudEvent.onPathFinderDebugChangeState(
                                        isChecked
                                    )
                                }
                            }
                        )
                        .build()
                )
                addCloseButton()
                centerWindow()
            }
        )
    }
}
