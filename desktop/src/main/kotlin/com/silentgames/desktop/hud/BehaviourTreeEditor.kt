package com.silentgames.desktop.hud

import com.artemis.Entity
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.Stage
import com.silentbugs.bte.AIEditor
import com.silentgames.core.ecs.component.ai.StepProcessorATBComponent
import com.silentgames.core.ecs.component.ai.aiTreeBehaviourComponent
import com.silentgames.core.ecs.component.ai.stepProcessorATBComponentOrNull
import com.silentgames.core.ecs.model.task.*
import com.silentgames.core.ecs.model.task.condition.IsNearNode
import com.silentgames.core.ecs.model.task.condition.IsNearTarget
import com.silentgames.core.screen.base.Context
import com.silentgames.core.screen.game.hud.BehaviourTreeEditor
import com.silentgames.core.utils.add
import com.silentgames.core.utils.remove

class BehaviourTreeEditor : BehaviourTreeEditor {

    companion object {
        const val EDITOR_TAG_ACTION = "Action"
        const val EDITOR_TAG_CONDITION = "Condition"
    }

    private lateinit var editor: AIEditor

    private var lastEntityId: Int? = null

    override fun initialize(context: Context) {
        editor = AIEditor(context.uiSkin)

        editor.addDefaultTaskClasses()
        editor.addTaskClass(EDITOR_TAG_ACTION, MoveToNodeTask::class.java)
        editor.addTaskClass(EDITOR_TAG_ACTION, MoveToTargetTask::class.java)
        editor.addTaskClass(EDITOR_TAG_ACTION, OpenDoorTask::class.java)
        editor.addTaskClass(EDITOR_TAG_ACTION, CloseDoorTask::class.java)
        editor.addTaskClass(EDITOR_TAG_ACTION, DropItemTask::class.java)
        editor.addTaskClass(EDITOR_TAG_ACTION, AddToInventoryTask::class.java)
        editor.addTaskClass(EDITOR_TAG_ACTION, RemoveFromInventoryTask::class.java)
        editor.addTaskClass(EDITOR_TAG_CONDITION, IsNearNode::class.java)
        editor.addTaskClass(EDITOR_TAG_CONDITION, IsNearTarget::class.java)
//        editor.setUpdateStrategy(object : AIEditor.BehaviorTreeStepStrategy {
//            var timer = 0f
//            override fun shouldStep(tree: BehaviorTree<*>?, delta: Float): Boolean {
//                timer += delta
//                if (timer >= 1) {
//                    timer -= 1f
//                    return true
//                }
//                return false
//            }
//        })
        editor.setSaveLoadDirectory(FileHandle("core/assets/ai"))
        editor.setBackupDirectory(FileHandle("core/assets/ai/backup"))
    }

    override fun step(entity: Entity, delta: Float) {
        val aiTreeBehaviourComponent = entity.aiTreeBehaviourComponent
        val hasStepProcessorATBComponentOrNull = entity.stepProcessorATBComponentOrNull != null
        if (entity.id != lastEntityId) {
            lastEntityId = entity.id
            editor.initialize(aiTreeBehaviourComponent.behaviorTree)
        }
        if (editor.isWindowVisible) {
            if (hasStepProcessorATBComponentOrNull) {
                entity.remove<StepProcessorATBComponent>()
            }
            editor.update(delta)
        } else {
            if (!hasStepProcessorATBComponentOrNull) {
                entity.add(StepProcessorATBComponent())
            }
        }
    }

    private fun Stage.render() {
        act()
        draw()
    }

    fun showEditor(group: Group) {
        editor.showWindow(group)
    }
}
