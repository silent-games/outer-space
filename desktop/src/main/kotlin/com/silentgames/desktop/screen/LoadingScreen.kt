package com.silentgames.desktop.screen

import com.kotcrab.vis.ui.VisUI
import com.kotcrab.vis.ui.VisUI.SkinScale
import com.silentgames.core.screen.base.AppScreenAdapter
import com.silentgames.core.screen.base.Context
import com.silentgames.core.screen.game.OuterSpaceGameScreen
import com.silentgames.core.ui.LoadingBarWidget
import com.silentgames.core.utils.TextureAtlasAssets
import com.silentgames.core.utils.loadAsync
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ktx.actors.centerPosition
import ktx.async.KtxAsync
import javax.inject.Inject

class LoadingScreen @Inject constructor(
    context: Context
) : AppScreenAdapter(context) {

    private val loadingBar = LoadingBarWidget(context.uiSkin)

    private var loaded = false

    override fun show() {
        context.stage.clear()
        context.stage.addActor(loadingBar)
        loadingBar.centerPosition(context.stage.width * 0.5f, context.stage.height * 0.15f)

        val assetReferences = listOf(
            // add large resources here
            TextureAtlasAssets.values().filter { it != TextureAtlasAssets.UI }
                .map { context.assets.loadAsync(it) }
        ).flatten()

        KtxAsync.launch {
            assetReferences.joinAll()
            VisUI.load(
                withContext(context.assets.asyncContext) {
                    SkinScale.X1.skinFile
                }
            )
            withContext(context.assets.asyncContext) {
                initiateResources()
            }
            loaded = true
        }
    }

    override fun hide() {
        context.stage.clear()
    }

    override fun resize(width: Int, height: Int) {
        context.stage.viewport.update(width, height, true)
    }

    override fun render(delta: Float) {
        loadingBar.scaleTo(context.assets.progress.percent)

        if (loaded) {
            context.game.setScreen<OuterSpaceGameScreen>()
            context.game.removeScreen<LoadingScreen>()
            dispose()
        }

        context.stage.viewport.apply()
        context.stage.act()
        context.stage.draw()
    }

    private fun initiateResources() {
        context.game.addScreen(context.game.gameDiComponent.getOuterSpaceGameScreen())
    }
}
