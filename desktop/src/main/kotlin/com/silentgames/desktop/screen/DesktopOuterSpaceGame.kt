package com.silentgames.desktop.screen

import com.badlogic.gdx.Application
import com.badlogic.gdx.Gdx
import com.silentgames.core.di.*
import com.silentgames.core.screen.base.OuterSpaceGame
import com.silentgames.core.utils.Font
import com.silentgames.core.utils.TextureAtlasAssets
import com.silentgames.core.utils.loadAsync
import com.silentgames.desktop.controller.DesktopGameInputController
import com.silentgames.desktop.hud.BehaviourTreeEditor
import com.silentgames.desktop.hud.DesktopContentItemAction
import com.silentgames.desktop.hud.DesktopGameHud
import kotlinx.coroutines.launch
import ktx.async.KtxAsync
import ktx.freetype.async.registerFreeTypeFontLoaders

class DesktopOuterSpaceGame : OuterSpaceGame() {

    private val behaviourTreeEditor = BehaviourTreeEditor()
    private val desktopGameHud = DesktopGameHud(behaviourTreeEditor)

    override val gameDiComponent: GameDiComponent by lazy {
        DaggerGameDiComponent.builder()
            .setGameResourcesModule(GameResourceModule(this))
            .setGameInputControllerModule(GameInputControllerModule(DesktopGameInputController()))
            .setHudModule(HudModule(DesktopContentItemAction(), desktopGameHud, desktopGameHud))
            .setDebugHudModule(DebugHudModule(behaviourTreeEditor))
            .build()
    }

    override fun create() {
        Gdx.app.logLevel = Application.LOG_DEBUG

        KtxAsync.initiate()

        KtxAsync.launch {
            Gdx.input.inputProcessor = gameDiComponent.getInputMultiplexer()

            gameDiComponent.getAssetStorage().loadAsync(TextureAtlasAssets.UI).await()
            gameDiComponent.getAssetStorage().registerFreeTypeFontLoaders()

            gameDiComponent.getAssetStorage().loadAsync(Font.SMALL)
            gameDiComponent.getAssetStorage().loadAsync(Font.REGULAR)
            gameDiComponent.getAssetStorage().loadAsync(Font.LARGE)

            addScreen(LoadingScreen(gameDiComponent.getContext()))
            setScreen<LoadingScreen>()
        }
    }
}
