package com.silentgames.desktop.controller

import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.viewport.Viewport
import com.silentgames.core.RenderConstants
import com.silentgames.core.ecs.system.GameInputListener
import com.silentgames.core.ecs.system.MouseButton.*
import ktx.log.logger

class GameCameraInputController(
    viewport: Viewport,
    camera: Camera,
    gameCameraInputListener: GameInputListener
) : CameraInputController(
    GameCameraGestureListener(viewport, camera, gameCameraInputListener),
    camera
) {

    companion object {
        private val LOG = logger<GameCameraInputController>()
    }

    private var scaleFactor: Float

    init {
        if (camera is OrthographicCamera) {
            camera.zoom = RenderConstants.defaultZoom
            scaleFactor = camera.zoom
        } else {
            scaleFactor = 1f
        }
        rotateButton = -1
        forwardButton = -1
        updateTranslateUnits()
    }

    private fun updateTranslateUnits() {
        translateUnits = RenderConstants.translateUnits * scaleFactor
    }

    override fun scrolled(amountX: Float, amountY: Float): Boolean {
        return if (camera is OrthographicCamera) {
            if (amountX > 0 || amountY > 0) {
                scaleFactor += RenderConstants.zoomScaleFactor
            } else {
                scaleFactor -= RenderConstants.zoomScaleFactor
            }
            updateTranslateUnits()
            zoom(scaleFactor)
        } else {
            super.scrolled(amountX, amountY)
        }
    }

    override fun zoom(amount: Float): Boolean {
        val camera = camera
        if (camera is OrthographicCamera) {
            camera.zoom = when {
                amount > RenderConstants.maxZoomValue -> {
                    RenderConstants.maxZoomValue
                }
                amount < RenderConstants.minZoomValue -> {
                    RenderConstants.minZoomValue
                }
                else -> {
                    amount
                }
            }
            scaleFactor = camera.zoom
            updateTranslateUnits()
            return true
        } else {
            return super.zoom(amount)
        }
    }

    private class GameCameraGestureListener(
        private val viewport: Viewport,
        private val camera: Camera,
        private val gameCameraInputListener: GameInputListener
    ) : CameraGestureListener() {

        override fun tap(x: Float, y: Float, count: Int, button: Int): Boolean {
            val vector = camera.unproject(
                Vector3(x, y, 0f),
                viewport.screenX.toFloat(),
                viewport.screenY.toFloat(),
                viewport.screenWidth.toFloat(),
                viewport.screenHeight.toFloat()
            )
            LOG.debug { "${vector.x.toInt()} ${vector.y.toInt()} gameVector: $vector screenX: $x screenY: $y" }
            when (values()[button]) {
                LEFT -> gameCameraInputListener.select(vector.x, vector.y, x, y)
                RIGHT -> gameCameraInputListener.action(vector.x, vector.y, x, y)
                MIDDLE -> gameCameraInputListener.changeLevel()
                BACK -> {
                }
                FORWARD -> {
                }
            }
            return super.tap(x, y, count, button)
        }
    }
}
