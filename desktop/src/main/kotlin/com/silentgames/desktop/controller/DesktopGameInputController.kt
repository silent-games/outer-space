package com.silentgames.desktop.controller

import com.badlogic.gdx.InputProcessor
import com.silentgames.core.ecs.system.SimpleGameInputController

class DesktopGameInputController : SimpleGameInputController() {

    override fun getInputProcessor(): InputProcessor =
        GameCameraInputController(viewport, orthographicCamera, gameCameraInputListener)
}
