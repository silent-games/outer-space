import org.jetbrains.kotlin.config.KotlinCompilerVersion
import org.jetbrains.kotlin.konan.properties.Properties

plugins {
    id("com.android.application")
    kotlin("android")
}

android {
    compileSdk = 33
    buildToolsVersion = "30.0.3"

    defaultConfig {
        applicationId = "com.silentgames.outerSpace"
        minSdk = 26
        targetSdk = 31
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }

    sourceSets {
        named("main") {
            manifest.srcFile("src/AndroidManifest.xml")
            java.setSrcDirs(listOf("src"))
            aidl.setSrcDirs(listOf("src"))
            renderscript.setSrcDirs(listOf("src"))
            res.setSrcDirs(listOf("src/res"))
            assets.setSrcDirs(listOf("../core/assets"))
            jniLibs.setSrcDirs(listOf("libs"))
        }
    }

    buildTypes {
        named("release") {
            isMinifyEnabled = false
            setProguardFiles(
                listOf(
                    getDefaultProguardFile("proguard-android-optimize.txt"),
                    "proguard-rules.pro"
                )
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

configurations { create("natives") }

fun DependencyHandler.natives(dependencyNotation: Any): Dependency? =
    add("natives", dependencyNotation)

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(kotlin("stdlib", KotlinCompilerVersion.VERSION))
    implementation("androidx.appcompat:appcompat:1.2.0")
    implementation("com.google.android.material:material:1.2.1")
    implementation("androidx.constraintlayout:constraintlayout:2.0.3")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:${properties["version.kotlin.coroutines"]}")
    implementation("androidx.coordinatorlayout:coordinatorlayout:1.1.0")
    testImplementation("junit:junit:4.13")
    androidTestImplementation("com.android.support.test:runner:1.0.2")
    androidTestImplementation("com.android.support.test.espresso:espresso-core:3.0.2")

    implementation(project(":core"))

    api("com.badlogicgames.gdx:gdx-backend-android:${properties["version.gdx"]}")
    natives("com.badlogicgames.gdx:gdx-platform:${properties["version.gdx"]}:natives-armeabi-v7a")
    natives("com.badlogicgames.gdx:gdx-platform:${properties["version.gdx"]}:natives-arm64-v8a")
    natives("com.badlogicgames.gdx:gdx-platform:${properties["version.gdx"]}:natives-x86")
    natives("com.badlogicgames.gdx:gdx-platform:${properties["version.gdx"]}:natives-x86_64")

    implementation("com.badlogicgames.gdx:gdx-freetype:${properties["version.gdx"]}")
    natives("com.badlogicgames.gdx:gdx-freetype-platform:${properties["version.gdx"]}:natives-armeabi-v7a")
    natives("com.badlogicgames.gdx:gdx-freetype-platform:${properties["version.gdx"]}:natives-arm64-v8a")
    natives("com.badlogicgames.gdx:gdx-freetype-platform:${properties["version.gdx"]}:natives-x86")
    natives("com.badlogicgames.gdx:gdx-freetype-platform:${properties["version.gdx"]}:natives-x86_64")
}

// called every time gradle gets executed, takes the native dependencies of
// the natives(configuration, and extracts them to the proper libs/ folders
// so they get packed with the APK.
tasks.register("copyAndroidNatives") {
    doFirst {
        file("libs/armeabi/").mkdirs()
        file("libs/armeabi-v7a/").mkdirs()
        file("libs/arm64-v8a/").mkdirs()
        file("libs/x86_64/").mkdirs()
        file("libs/x86/").mkdirs()

        configurations.getByName("natives").files.forEach { jar ->
            var outputDir: File? = null
            if (jar.name.endsWith("natives-arm64-v8a.jar")) outputDir = file("libs/arm64-v8a")
            if (jar.name.endsWith("natives-armeabi-v7a.jar")) outputDir = file("libs/armeabi-v7a")
            if (jar.name.endsWith("natives-armeabi.jar")) outputDir = file("libs/armeabi")
            if (jar.name.endsWith("natives-x86_64.jar")) outputDir = file("libs/x86_64")
            if (jar.name.endsWith("natives-x86.jar")) outputDir = file("libs/x86")
            if (outputDir != null) {
                copy {
                    from(zipTree(jar))
                    into(outputDir)
                    include("*.so")
                }
            }
        }
    }
}

tasks.whenTaskAdded {
    if (name.contains("package")) {
        this.dependsOn.add("copyAndroidNatives")
    }
}

tasks.register<Exec>("run") {
    val localProperties = project.file("../local.properties")
    val path = if (localProperties.exists()) {
        val properties = Properties()
        properties.load(localProperties.inputStream())
        val sdkDir = properties.getProperty("sdk.dir")
        if (sdkDir.isNotEmpty()) {
            sdkDir
        } else {
            "${System.getenv()["ANDROID_HOME"]}"
        }
    } else {
        "${System.getenv()["ANDROID_HOME"]}"
    }

    val adb = "$path/platform-tools/adb"
    setCommandLine(
        adb,
        "shell",
        "am",
        "start",
        "-n",
        "com.silentgames/com.silentgames.silent_planet.AndroidLauncher"
    )
}
