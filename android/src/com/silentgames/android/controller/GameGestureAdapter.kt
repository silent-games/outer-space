package com.silentgames.android.controller

import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.viewport.Viewport
import com.silentgames.core.RenderConstants
import com.silentgames.core.ecs.system.GameInputListener
import ktx.log.logger

class GameBoardZoomGestureDetector(listener: GestureAdapter) : GestureDetector(listener)

class GameGestureAdapter(
    private val viewport: Viewport,
    private val camera: OrthographicCamera,
    private val gameCameraInputListener: GameInputListener
) : GestureDetector.GestureAdapter() {

    companion object {
        private val LOG = logger<GameGestureAdapter>()
    }

    private var scaleFactor: Float = 1f
    private var isNowPinch = false
    private var zoomPoint: Vector2? = null
    private var translateUnits = RenderConstants.translateUnits

    init {
        camera.zoom = RenderConstants.defaultZoom
        scaleFactor = camera.zoom
        updateTranslateUnits()
    }

    private fun updateTranslateUnits() {
        translateUnits = RenderConstants.translateUnits / scaleFactor
    }

    private fun zoomCamera(zoom: Float) {
        camera.zoom = when {
            zoom > RenderConstants.maxZoomValue -> {
                RenderConstants.maxZoomValue
            }
            zoom < RenderConstants.minZoomValue -> {
                RenderConstants.minZoomValue
            }
            else -> {
                zoom
            }
        }
        updateTranslateUnits()
        checkCameraBorders()
    }

    override fun zoom(initialDistance: Float, distance: Float): Boolean {
        val ratio: Float = initialDistance / distance
        zoomCamera(scaleFactor * ratio)
        return false
    }

    private var selected = false

    override fun tap(x: Float, y: Float, count: Int, button: Int): Boolean {
        if (count == 2) {
            if (scaleFactor < RenderConstants.defaultZoom) {
                camera.zoom = RenderConstants.defaultZoom
                scaleFactor = camera.zoom
            } else {
                camera.zoom = RenderConstants.minZoomValue
                scaleFactor = camera.zoom
                val vector = camera.unProject(Vector2(x, y))
                camera.position.x = vector.x
                camera.position.y = vector.y
            }
            checkCameraBorders()
            return true
        } else {
            val vector = camera.unProject(Vector2(x, y))
            LOG.debug { "${vector.x.toInt()} ${vector.y.toInt()} gameVector: $vector screenX: $x screenY: $y" }
            if (selected) {
                selected = gameCameraInputListener.action(vector.x, vector.y, x, y)
                if (!selected) {
                    gameCameraInputListener.select(vector.x, vector.y, x, y)
                }
            } else {
                selected = gameCameraInputListener.select(vector.x, vector.y, x, y)
            }
            return false
        }
    }

    override fun longPress(x: Float, y: Float): Boolean {
        val vector = camera.unProject(Vector2(x, y))
        LOG.debug { "longPress ${vector.x.toInt()} ${vector.y.toInt()} gameVector: $vector screenX: $x screenY: $y" }
        selected = gameCameraInputListener.select(vector.x, vector.y, x, y)
        return true
    }

    override fun pan(x: Float, y: Float, deltaX: Float, deltaY: Float): Boolean {
        if (!isNowPinch) {
            camera.translate(-deltaX / translateUnits, deltaY / translateUnits)
            checkCameraBorders()
        }
        return false
    }

    override fun pinch(
        initialPointer1: Vector2,
        initialPointer2: Vector2,
        pointer1: Vector2,
        pointer2: Vector2
    ): Boolean {
        isNowPinch = true
        val initialPointerFirst = camera.unProject(initialPointer1)
        val initialPointerSecond = camera.unProject(initialPointer2)
        if (zoomPoint == null) {
            zoomPoint = Vector2(
                (initialPointerFirst.x + initialPointerSecond.x) / 2,
                (initialPointerFirst.y + initialPointerSecond.y) / 2
            )
        }
        zoomPoint?.let { camera.setPosition(it) }
        return false
    }

    override fun pinchStop() {
        isNowPinch = false
        zoomPoint = null
        scaleFactor = camera.zoom
        super.pinchStop()
    }

    private fun Camera.setPosition(vector2: Vector2) {
        position.x = vector2.x
        position.y = vector2.y
    }

    private fun checkCameraBorders() {
//        val effectiveViewportWidth: Float = camera.viewportWidth * camera.zoom
//        val effectiveViewportHeight: Float = camera.viewportHeight * camera.zoom
//        camera.position.x = MathUtils.clamp(
//                camera.position.x,
//                effectiveViewportWidth / 2f,
//                camera.viewportWidth - effectiveViewportWidth / 2f
//        )
//        camera.position.y = MathUtils.clamp(
//                camera.position.y,
//                effectiveViewportHeight / 2f,
//                camera.viewportHeight - effectiveViewportHeight / 2f
//        )
    }

    private fun Camera.unProject(vector: Vector2): Vector3 =
        unproject(
            vector.toVector3(),
            viewport.screenX.toFloat(),
            viewport.screenY.toFloat(),
            viewport.screenWidth.toFloat(),
            viewport.screenHeight.toFloat()
        )

    private fun Vector2.toVector3() = Vector3(x, y, 0f)

    private fun Vector3.toVector2() = Vector2(x, y)
}
