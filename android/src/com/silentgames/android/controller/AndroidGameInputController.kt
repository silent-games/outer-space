package com.silentgames.android.controller

import com.badlogic.gdx.InputProcessor
import com.silentgames.core.ecs.system.SimpleGameInputController

class AndroidGameInputController : SimpleGameInputController() {

    override fun getInputProcessor(): InputProcessor =
        GameBoardZoomGestureDetector(
            GameGestureAdapter(viewport, orthographicCamera, gameCameraInputListener)
        )
}
