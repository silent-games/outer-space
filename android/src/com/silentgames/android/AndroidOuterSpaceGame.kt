package com.silentgames.android

import com.artemis.Entity
import com.badlogic.gdx.Application
import com.badlogic.gdx.Gdx
import com.silentgames.android.controller.AndroidGameInputController
import com.silentgames.core.di.*
import com.silentgames.core.screen.base.Context
import com.silentgames.core.screen.base.OuterSpaceGame
import com.silentgames.core.screen.game.hud.BehaviourTreeEditor
import com.silentgames.core.utils.Font
import com.silentgames.core.utils.TextureAtlasAssets
import com.silentgames.core.utils.loadAsync
import kotlinx.coroutines.launch
import ktx.async.KtxAsync
import ktx.freetype.async.registerFreeTypeFontLoaders

class AndroidOuterSpaceGame(private val hudActions: HudActions) : OuterSpaceGame() {

    override val gameDiComponent: GameDiComponent by lazy {
        DaggerGameDiComponent.builder()
            .setGameResourcesModule(GameResourceModule(this))
            .setGameInputControllerModule(GameInputControllerModule(AndroidGameInputController()))
            .setHudModule(HudModule(hudActions, hudActions, hudActions))
            .setDebugHudModule(
                DebugHudModule(
                    object : BehaviourTreeEditor {
                        override fun initialize(context: Context) {
                        }

                        override fun step(entity: Entity, delta: Float) {
                        }
                    }
                )
            )
            .build()
    }

    override fun create() {
        Gdx.app.logLevel = Application.LOG_DEBUG

        KtxAsync.initiate()

        KtxAsync.launch {
            Gdx.input.inputProcessor = gameDiComponent.getInputMultiplexer()

            gameDiComponent.getAssetStorage().loadAsync(TextureAtlasAssets.UI).await()
            gameDiComponent.getAssetStorage().registerFreeTypeFontLoaders()

            gameDiComponent.getAssetStorage().loadAsync(Font.SMALL)
            gameDiComponent.getAssetStorage().loadAsync(Font.REGULAR)
            gameDiComponent.getAssetStorage().loadAsync(Font.LARGE)

            addScreen(LoadingScreen(gameDiComponent.getContext()))
            setScreen<LoadingScreen>()
        }
    }
}
