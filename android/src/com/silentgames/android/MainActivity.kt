package com.silentgames.android

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.backends.android.AndroidFragmentApplication
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.PixmapIO
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.StreamUtils
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.silentgames.core.model.ContentItem
import com.silentgames.core.model.SelectedObjectData
import com.silentgames.core.screen.base.Context
import com.silentgames.core.screen.game.hud.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream

interface HudActions : ContentItemAction, GameHud, SelectedEntityHud

class MainActivity :
    AppCompatActivity(),
    AndroidFragmentApplication.Callbacks,
    HudActions,
    CoroutineScope by MainScope() {

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    private lateinit var infoBottomSheet: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        infoBottomSheet = findViewById(R.id.infoBottomSheet)
        bottomSheetBehavior = BottomSheetBehavior.from(infoBottomSheet)
        bottomSheetBehavior.isHideable = true

        supportFragmentManager.beginTransaction().add(R.id.fragmentHolder, OuterSpaceGameFragment())
            .commit()
    }

    override fun exit() {
    }

    override fun onAttachContext(context: Context) {
    }

    override fun onAttachInputMultiplexer(inputMultiplexer: InputMultiplexer) {
    }

    override fun onAttach() {
    }

    override fun onUpdate() {
    }

    override fun onHide() {
    }

    override fun onDetach() {
    }

    override fun create(
        screenPos: Vector2,
        actionList: List<ContentItem>,
        onClick: (ContentItem) -> Unit
    ) {
    }

    override fun changeVisibility(visible: Boolean) {
    }

    override fun onEntityAdded(selectedObjectData: SelectedObjectData) {
        val satietyTextView = infoBottomSheet.findViewById<TextView>(R.id.satietyTextView)
        launch {
            infoBottomSheet.findViewById<TextView>(R.id.titleTextView).text =
                selectedObjectData.name
            infoBottomSheet.findViewById<TextView>(R.id.descriptionTextView).text =
                selectedObjectData.description
            infoBottomSheet.findViewById<ImageView>(R.id.textureImageView)
                .setImageBitmap(selectedObjectData.pixmap.convertToBitmap())
            val hungerComponent = selectedObjectData.hungerComponent
            if (hungerComponent != null) {
                satietyTextView.text = "Cытость: ${hungerComponent.satiety.toInt()}"
                satietyTextView.visibility = View.VISIBLE
            } else {
                satietyTextView.visibility = View.GONE
            }
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        selectedObjectData.hungerComponent?.let {
            it.onSatietyChanged = {
                launch {
                    satietyTextView.text = "Cытость: ${it.toInt()}"
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
            }
        }
    }

    override fun onEntityRemoved() {
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun Pixmap.convertToBitmap(): Bitmap {
        val writer = PixmapIO.PNG((this.width * this.height * 1.5).toInt())
        writer.setFlipY(false)
        val output = ByteArrayOutputStream()
        try {
            writer.write(output, this)
        } finally {
            StreamUtils.closeQuietly(output)
            writer.dispose()
            this.dispose()
        }

        val byteArray = output.toByteArray()
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
    }

    override fun processEvents(hudEvent: HudEvent) {
    }

    override fun processDebugEvents(debugHudEvent: DebugHudEvent) {
    }
}
