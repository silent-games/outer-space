import org.jetbrains.kotlin.config.KotlinCompilerVersion
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("java-library")
    kotlin("jvm")
    kotlin("kapt")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = freeCompilerArgs + "-Xuse-experimental=kotlin.Experimental"
        jvmTarget = "1.8"
    }
}

sourceSets {
    named("main") {
        java.setSrcDirs(
            listOf(
                "src/main/java",
                "$buildDir/generated/source/kapt/main/",
                "$buildDir/generated/source/kaptKotlin/main/"
            )
        )
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(kotlin("stdlib", KotlinCompilerVersion.VERSION))
    api("org.jetbrains.kotlinx:kotlinx-coroutines-core:${properties["version.kotlin.coroutines"]}")

    implementation(project(":support-annotations"))
    kapt(project(":support-processor"))

    api("com.silent-bugs:bt-editor:0.8.0-SNAPSHOT")
    api("io.github.libktx:ktx-app:${properties["version.gdx.ktx"]}")
    api("io.github.libktx:ktx-log:${properties["version.gdx.ktx"]}")
    api("io.github.libktx:ktx-actors:${properties["version.gdx.ktx"]}")
    api("io.github.libktx:ktx-scene2d:${properties["version.gdx.ktx"]}")
    api("io.github.libktx:ktx-style:${properties["version.gdx.ktx"]}")
    api("io.github.libktx:ktx-assets-async:${properties["version.gdx.ktx"]}")
    api("io.github.libktx:ktx-freetype-async:${properties["version.gdx.ktx"]}")
    api("io.github.libktx:ktx-tiled:${properties["version.gdx.ktx"]}")
    api("io.github.libktx:ktx-collections:${properties["version.gdx.ktx"]}")
    api("io.github.libktx:ktx-graphics:${properties["version.gdx.ktx"]}")
    api("io.github.libktx:ktx-math:${properties["version.gdx.ktx"]}")
    api("space.earlygrey:shapedrawer:2.3.0")
    api("net.onedaybeard.artemis:artemis-odb-serializer-kryo:2.3.0")
    api("com.esotericsoftware:kryo:5.0.3")

    api("com.kotcrab.vis:vis-ui:${properties["version.visui"]}")
    api("io.github.libktx:ktx-vis:${properties["version.gdx.ktx"]}")
    api("io.github.libktx:ktx-vis-style:${properties["version.gdx.ktx"]}")

    kapt("com.google.dagger:dagger-compiler:${properties["version.dagger"]}")
    api("com.google.dagger:dagger:${properties["version.dagger"]}")

    api("net.onedaybeard.artemis:artemis-odb:${properties["version.artemis"]}")
    implementation("net.mostlyoriginal.artemis-odb:contrib-core:2.4.0")
    api(kotlin("stdlib", KotlinCompilerVersion.VERSION))
    api("com.badlogicgames.gdx:gdx:${properties["version.gdx"]}")
    api("com.badlogicgames.gdx:gdx-freetype:${properties["version.gdx"]}")
    api("com.badlogicgames.gdx:gdx-ai:${properties["version.gdx.ai"]}")
    api("com.badlogicgames.gdx:gdx-box2d:${properties["version.gdx"]}")
}
java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}
