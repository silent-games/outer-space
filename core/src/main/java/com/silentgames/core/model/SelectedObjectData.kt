package com.silentgames.core.model

import com.badlogic.gdx.graphics.Pixmap
import com.silentgames.core.ecs.component.HungerComponent
import com.silentgames.core.ecs.model.Node

class SelectedObjectData(
    val id: Int,
    val node: Node,
    val name: String,
    val description: String,
    val pixmap: Pixmap,
    val hungerComponent: HungerComponent?,
    val showBteEditorButton: Boolean = false
)
