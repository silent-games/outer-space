package com.silentgames.core.model

import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.silentgames.core.ui.createGameTextureAtlas
import ktx.assets.async.AssetStorage

class GameState(assetStorage: AssetStorage, var currentLevel: Int = 0) {

    // Do not use on init! only lazy access!
    val gameTextureAtlas: TextureAtlas by lazy { assetStorage.createGameTextureAtlas() }
}
