package com.silentgames.core.model

import com.artemis.Entity
import com.silentgames.core.ecs.model.action.Action

class ContentItem(val action: Action, val entity: Entity)
