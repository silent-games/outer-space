package com.silentgames.core.parallax

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.Vector2

abstract class ParallaxLayer {

    enum class TileMode {
        REPEAT, SINGLE
    }

    var parallaxRatio: Vector2 = Vector2()
    var tileModeX = TileMode.REPEAT
    var tileModeY = TileMode.SINGLE
    abstract val width: Float
    abstract val height: Float

    fun draw(batch: Batch, pos: Vector2) {
        this.draw(batch, pos.x, pos.y)
    }

    abstract fun draw(batch: Batch, x: Float, y: Float)
}
