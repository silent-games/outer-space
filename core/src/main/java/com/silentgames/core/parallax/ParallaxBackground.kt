package com.silentgames.core.parallax

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.Array
import com.silentgames.core.parallax.ParallaxBackground.Orientation.X
import com.silentgames.core.parallax.ParallaxBackground.Orientation.Y
import kotlin.math.abs

class ParallaxBackground {

    private var layers: Array<ParallaxLayer> = Array()
    private var cachedProjectionView: Matrix4 = Matrix4()
    private var cachedPosition: Vector3 = Vector3()
    private var cachedZoom = 0f

    fun addLayers(vararg layers: ParallaxLayer?) {
        this.layers.addAll(*layers)
    }

    companion object {
        const val PARALLAX_WITHOUT_DISPLACEMENT = 0f
        const val PARALLAX_RATIO_DISPLACEMENT = .5f
        const val PARALLAX_RATIO_ZOOM = .5f
    }

    fun draw(worldCamera: OrthographicCamera, batch: Batch) {
        cachedProjectionView.set(worldCamera.combined)
        cachedPosition.set(worldCamera.position)
        cachedZoom = worldCamera.zoom
        layers.forEach {
            val origCameraPos = Vector2(cachedPosition.x, cachedPosition.y)
            worldCamera.position[origCameraPos.scl(it.parallaxRatio)] = cachedPosition.z
            worldCamera.update()
            batch.projectionMatrix = worldCamera.combined
            it.calculateDisplacement(worldCamera, X) { x ->
                it.calculateDisplacement(worldCamera, Y) { y ->
                    it.draw(x, y, batch, worldCamera)
                }
            }
        }
        worldCamera.combined.set(cachedProjectionView)
        worldCamera.position.set(cachedPosition)
        worldCamera.zoom = cachedZoom
        worldCamera.update()
        batch.projectionMatrix = worldCamera.combined
    }

    private fun ParallaxLayer.calculateDisplacement(
        worldCamera: OrthographicCamera,
        orientation: Orientation,
        block: (currentDisplacement: Float) -> Unit
    ) {
        if (this.getTitleMode(orientation) == ParallaxLayer.TileMode.SINGLE) {
            block.invoke(PARALLAX_WITHOUT_DISPLACEMENT)
        } else {
            var currentDisplacement: Float = displacementClone(
                worldCamera.getPosition(orientation),
                worldCamera.getViewPortSize(orientation),
                worldCamera.zoom,
                this.getSize(orientation),
                this.getParallaxRatio(orientation)
            )
            do {
                block.invoke(currentDisplacement)
                currentDisplacement += this.getSize(orientation)
            } while (currentDisplacement < worldCamera.getPosition(orientation) + worldCamera.getParallaxSize(
                    orientation
                )
            )
        }
    }

    private fun ParallaxLayer.draw(
        currentX: Float,
        currentY: Float,
        batch: Batch,
        worldCamera: OrthographicCamera
    ) {
        val firstCondition: Boolean =
            worldCamera.position.x - worldCamera.parallaxWidth() <= currentX + this.width
        val secondCondition: Boolean =
            worldCamera.position.x + worldCamera.parallaxWidth() >= currentX
        val thirdCondition: Boolean =
            worldCamera.position.y - worldCamera.parallaxHeight() <= currentY + this.height
        val fourthCondition: Boolean =
            worldCamera.position.y + worldCamera.parallaxHeight() >= currentY
        if (firstCondition || secondCondition || thirdCondition || fourthCondition) {
            this.draw(batch, currentX, currentY)
        }
    }

    private fun ParallaxLayer.getParallaxRatio(orientation: Orientation) =
        when (orientation) {
            X -> this.parallaxRatio.x
            Y -> this.parallaxRatio.y
        }

    private fun ParallaxLayer.getTitleMode(orientation: Orientation) =
        when (orientation) {
            X -> this.tileModeX
            Y -> this.tileModeY
        }

    private fun ParallaxLayer.getSize(orientation: Orientation) =
        when (orientation) {
            X -> this.width
            Y -> this.height
        }

    private fun OrthographicCamera.getPosition(orientation: Orientation) =
        when (orientation) {
            X -> this.position.x
            Y -> this.position.y
        }

    private fun OrthographicCamera.getParallaxSize(orientation: Orientation) =
        when (orientation) {
            X -> this.parallaxWidth()
            Y -> this.parallaxHeight()
        }

    private fun OrthographicCamera.getViewPortSize(orientation: Orientation) =
        when (orientation) {
            X -> this.viewportWidth
            Y -> this.viewportHeight
        }

    private fun OrthographicCamera.parallaxWidth() =
        this.viewportWidth * this.zoom * PARALLAX_RATIO_ZOOM

    private fun OrthographicCamera.parallaxHeight() =
        this.viewportHeight * this.zoom * PARALLAX_RATIO_ZOOM

    private fun displacementClone(
        camPos: Float,
        viewport: Float,
        zoom: Float,
        layerSize: Float,
        layerParallaxRatio: Float,
        displacement: Float = PARALLAX_RATIO_DISPLACEMENT
    ): Float {
        return ((camPos - viewport * displacement * zoom) / layerSize) * layerSize - abs((1 - layerParallaxRatio) % 1) * viewport * displacement
    }

    private enum class Orientation {
        X,
        Y
    }
}
