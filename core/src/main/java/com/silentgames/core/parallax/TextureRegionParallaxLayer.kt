package com.silentgames.core.parallax

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2

class TextureRegionParallaxLayer(
    private var texRegion: TextureRegion,
    private var regionWidth: Float,
    private var regionHeight: Float,
    parallaxScrollRatio: Vector2
) : ParallaxLayer() {

    private var padLeft = 0f
    private var padRight = 0f
    private var padBottom = 0f
    private var padTop = 0f

    init {
        parallaxRatio.set(parallaxScrollRatio)
    }

    override fun draw(batch: Batch, x: Float, y: Float) {
        batch.draw(texRegion, x + padLeft, y + padBottom, regionWidth, regionHeight)
    }

    override val width: Float
        get() = padLeft + regionWidth + padRight

    override val height: Float
        get() = padTop + regionHeight + padBottom
}
