package com.silentgames.core.screen.game

import com.artemis.World
import com.artemis.io.KryoArtemisSerializer
import com.artemis.io.SaveFileFormat
import com.artemis.managers.WorldSerializationManager
import com.artemis.utils.IntBag
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ai.btree.BehaviorTree
import com.badlogic.gdx.ai.utils.random.UniformFloatDistribution
import com.esotericsoftware.kryo.Kryo
import com.esotericsoftware.kryo.Serializer
import com.esotericsoftware.kryo.io.Input
import com.esotericsoftware.kryo.io.Output
import com.esotericsoftware.kryo.serializers.FieldSerializer
import com.silentgames.core.di.GameScope
import com.silentgames.core.ecs.model.task.base.BehaviorQueue
import javax.inject.Inject

@GameScope
class WorldLoader @Inject constructor(private val worldSerializationManager: WorldSerializationManager) {

    companion object {
        private const val SAVE_FILE = "data/save.bin"
    }

    fun save(entities: IntBag) {
        try {
            val file = Gdx.files.local(SAVE_FILE)
            worldSerializationManager.save(file.write(false), SaveFileFormat(entities))
        } catch (e: Exception) {
            Gdx.app.error("WorldLoader", "Save Failed", e)
        }
    }

    fun load(): Boolean {
        try {
            val file = Gdx.files.local(SAVE_FILE)
            if (file.exists()) {
                worldSerializationManager.load(file.read(), SaveFileFormat::class.java)
                return true
            }
        } catch (e: java.lang.Exception) {
            Gdx.app.error("WorldLoader", "Load Failed", e)
        }
        return false
    }

    fun setSerializer(world: World) {
        worldSerializationManager.setSerializer(
            KryoArtemisSerializer(world).apply {
                register(
                    BehaviorTree::class.java,
                    object : FieldSerializer<BehaviorTree<*>>(kryo, BehaviorTree::class.java) {
                        override fun write(kryo: Kryo, output: Output, `object`: BehaviorTree<*>) {
                            val value = `object`.`object`
                            val listeners = `object`.listeners
                            `object`.`object` = null
                            `object`.listeners = null
                            super.write(kryo, output, `object`)
                            `object`.`object` = value
                            `object`.listeners = listeners
                        }
                    }
                )

                register(
                    BehaviorQueue::class.java,
                    object : FieldSerializer<BehaviorQueue<*>>(kryo, BehaviorQueue::class.java) {
                        override fun write(kryo: Kryo, output: Output, `object`: BehaviorQueue<*>) {
                            val value = `object`.`object`
                            val listeners = `object`.listeners
                            `object`.`object` = null
                            `object`.listeners = null
                            super.write(kryo, output, `object`)
                            `object`.`object` = value
                            `object`.listeners = listeners
                        }
                    }
                )

                register(
                    UniformFloatDistribution::class.java,
                    object : Serializer<UniformFloatDistribution>() {
                        override fun write(
                            kryo: Kryo?,
                            output: Output,
                            value: UniformFloatDistribution
                        ) {
                            output.writeFloat(value.low)
                            output.writeFloat(value.high)
                        }

                        override fun read(
                            kryo: Kryo?,
                            input: Input,
                            type: Class<out UniformFloatDistribution>?
                        ): UniformFloatDistribution {
                            return UniformFloatDistribution(input.readFloat(), input.readFloat())
                        }
                    }
                )
            }
        )
    }
}
