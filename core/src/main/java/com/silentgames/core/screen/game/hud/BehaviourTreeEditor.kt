package com.silentgames.core.screen.game.hud

import com.artemis.Entity
import com.silentgames.core.screen.base.Context

interface BehaviourTreeEditor {

    fun initialize(context: Context)

    fun step(entity: Entity, delta: Float)
}
