package com.silentgames.core.screen.game

import com.artemis.World
import com.artemis.WorldConfigurationBuilder
import com.artemis.managers.WorldSerializationManager
import com.badlogic.gdx.graphics.OrthographicCamera
import com.silentgames.core.ecs.entity.*
import com.silentgames.core.ecs.entity.inventory.addItem
import com.silentgames.core.ecs.system.*
import com.silentgames.core.ecs.system.hud.DebugBteSystem
import com.silentgames.core.ecs.system.hud.GameHudSystem
import com.silentgames.core.ecs.system.map.TerrainMapSystem
import com.silentgames.core.ecs.system.render.MapRendererSystem
import com.silentgames.core.ecs.system.render.batch.*
import com.silentgames.core.ecs.system.render.debug.PathFinderDebugRenderSystem
import com.silentgames.core.ecs.system.render.tile.*
import com.silentgames.core.screen.base.AppScreenAdapter
import com.silentgames.core.screen.base.Context
import com.silentgames.core.utils.resize
import com.silentgames.core.utils.withBatchRenderSystems
import javax.inject.Inject

class OuterSpaceGameScreen @Inject constructor(
    context: Context,
    private val camera: OrthographicCamera,
    private val worldSerializationManager: WorldSerializationManager,
    private val worldLoader: WorldLoader,
    private val emptyTileSystem: EmptyTileSystem,
    private val tileSystem: TileSystem,
    private val wallTileSystem: WallTileSystem,
    private val doorSystem: DoorSystem,
    private val cracksSystem: CracksSystem,
    private val gameInputSystem: GameInputSystem,
    private val selectedSystem: SelectedSystem,
    private val mapRendererSystem: MapRendererSystem,
    private val backgroundRenderSystem: BackgroundRenderSystem,
    private val foregroundRenderSystem: ForegroundRenderSystem,
    private val pathFinderDebugRenderSystem: PathFinderDebugRenderSystem,
    private val unitRenderSystem: UnitRenderSystem,
    private val lineRenderSystem: LineRenderSystem,
    private val itemRenderSystem: ItemRenderSystem,
    private val movementSystem: MovementSystem,
    private val pathBuilderSystem: PathBuilderSystem,
    private val contextMenuSystem: ContextMenuSystem,
    private val inventorySystem: InventorySystem,
    private val itemGenerationSystem: ItemGenerationSystem,
    private val taskQueueSystem: TaskQueueSystem,
    private val aiTreeBehaviourSystem: AiTreeBehaviourSystem,
    private val gameHudSystem: GameHudSystem,
    private val debugBteSystem: DebugBteSystem,
    private val terrainMapSystem: TerrainMapSystem,
    private val timepieceSystem: TimepieceSystem,
    private val miningResourceSystem: MiningResourceSystem,
    private val cookingSystem: CookingSystem,
    private val saturationSystem: SaturationSystem
) : AppScreenAdapter(context) {

    private lateinit var world: World

    private fun setupWorld() {
        val setup: WorldConfigurationBuilder = WorldConfigurationBuilder()
            .dependsOn()
            .with(worldSerializationManager)
            .with(emptyTileSystem)
            .with(tileSystem)
            .with(wallTileSystem)
            .with(doorSystem)
            .with(cracksSystem)
            .with(terrainMapSystem)
            .with(cookingSystem)
            .with(pathBuilderSystem)
            .with(gameInputSystem)
            .with(selectedSystem)
            .with(movementSystem)
            .with(HungerSystem())
            .with(saturationSystem)
            .with(miningResourceSystem)
            .with(contextMenuSystem)
            .with(inventorySystem)
            .with(taskQueueSystem)
            .with(aiTreeBehaviourSystem)
            .with(itemGenerationSystem)
            .with(timepieceSystem)

        setup.withBatchRenderSystems(
            context.spriteBatch,
            camera,
            backgroundRenderSystem,
            foregroundRenderSystem
        )

        setup.with(mapRendererSystem)
        setup.withBatchRenderSystems(
            context.spriteBatch,
            camera,
            pathFinderDebugRenderSystem,
            itemRenderSystem,
            lineRenderSystem,
            unitRenderSystem
        )
        setup.with(gameHudSystem)
        setup.with(debugBteSystem)

        world = World(setup.build())

        pathFinderDebugRenderSystem.isEnabled = false

        worldLoader.setSerializer(world)
    }

    private val floor1: Array<Array<String>> = arrayOf(
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "R1",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "ST",
            "F",
            "F",
            "F",
            "F",
            "G",
            "F",
            "D",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "U",
            "F",
            "F",
            "F",
            "F",
            "I",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "R2",
            "S"
        ),
        arrayOf(
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        )
    )

    private val floor2: Array<Array<String>> = arrayOf(
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "ST",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "C",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "F",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        ),
        arrayOf(
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "W",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S",
            "S"
        )
    )

    private fun Array<Array<String>>.fillFloor(level: Int, targetLevel: Int) {
        world.apply {
            forEachIndexed { x, array ->
                array.forEachIndexed { y, s ->
                    when (s) {
                        "S" -> {
                            addSpace(x, y, level)
                        }
                        "F" -> {
                            addShipFloor(x, y, level)
                        }
                        "W" -> {
                            addWall(x, y, level)
                            addShipFloor(x, y, level)
                        }
                        "D" -> {
                            addDoor(x, y, level)
                            addShipFloor(x, y, level)
                        }
                        "U" -> {
                            addUnit(x, y, level, "Человечек", "Наш первый уродец")
                            addShipFloor(x, y, level)
                        }
                        "I" -> {
                            addItem(x, y, level)
                            addShipFloor(x, y, level)
                        }
                        "G" -> {
                            addItemGenerator(x, y, level)
                            addShipFloor(x, y, level)
                        }
                        "ST" -> {
                            addStairs(x, y, level, targetLevel)
                            addShipFloor(x, y, level)
                        }
                        "C" -> {
                            addFoodGenerator(x, y, level)
                            addShipFloor(x, y, level)
                        }
                        "R1" -> {
                            addResourceAqua(x, y, level)
                            addSpace(x, y, level)
                        }
                        "R2" -> {
                            addResourceRedCrystal(x, y, level)
                            addSpace(x, y, level)
                        }
                    }
                }
            }
        }
    }

    override fun show() {
        setupWorld()

        if (!worldLoader.load()) {
            floor1.fillFloor(0, 1)
            floor2.fillFloor(1, 0)
        }
    }

    override fun render(delta: Float) {
        world.setDelta(delta)
        world.process()
    }

    override fun resize(width: Int, height: Int) {
        context.stage.resize(width, height, true)
    }

    override fun hide() {
        gameHudSystem.hide()
    }
}
