package com.silentgames.core.screen.game.hud

interface DebugHudEvent {

    fun onPathFinderDebugChangeState(visible: Boolean)
}
