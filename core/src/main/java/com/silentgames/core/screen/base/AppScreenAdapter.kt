package com.silentgames.core.screen.base

import ktx.app.KtxScreen

open class AppScreenAdapter(val context: Context) : KtxScreen
