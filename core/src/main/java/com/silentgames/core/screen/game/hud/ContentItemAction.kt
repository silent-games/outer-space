package com.silentgames.core.screen.game.hud

import com.badlogic.gdx.math.Vector2
import com.silentgames.core.model.ContentItem
import com.silentgames.core.screen.base.Context

interface ContentItemAction {

    fun onAttachContext(context: Context)

    fun create(screenPos: Vector2, actionList: List<ContentItem>, onClick: (ContentItem) -> Unit)

    fun changeVisibility(visible: Boolean)
}
