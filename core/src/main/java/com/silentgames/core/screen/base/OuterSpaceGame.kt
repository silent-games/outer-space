package com.silentgames.core.screen.base

import com.silentgames.core.di.GameDiComponent
import ktx.app.KtxGame
import ktx.app.KtxScreen

abstract class OuterSpaceGame : KtxGame<KtxScreen>() {

    abstract val gameDiComponent: GameDiComponent
}
