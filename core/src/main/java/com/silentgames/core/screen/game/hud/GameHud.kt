package com.silentgames.core.screen.game.hud

import com.badlogic.gdx.InputMultiplexer
import com.silentgames.core.screen.base.Context

interface GameHud {

    fun onAttachContext(context: Context)

    fun onAttachInputMultiplexer(inputMultiplexer: InputMultiplexer)

    fun onAttach()

    fun onUpdate()

    fun onHide()

    fun onDetach()

    fun processEvents(hudEvent: HudEvent)

    fun processDebugEvents(debugHudEvent: DebugHudEvent)
}
