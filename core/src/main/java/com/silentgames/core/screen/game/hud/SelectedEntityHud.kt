package com.silentgames.core.screen.game.hud

import com.silentgames.core.model.SelectedObjectData

interface SelectedEntityHud {

    fun onEntityAdded(selectedObjectData: SelectedObjectData)

    fun onEntityRemoved()
}
