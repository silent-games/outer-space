package com.silentgames.core.screen.base

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import ktx.assets.async.AssetStorage

class Context(
    val assets: AssetStorage,
    val uiSkin: Skin,
    val spriteBatch: SpriteBatch,
    val stage: Stage,
    val game: OuterSpaceGame
)
