package com.silentgames.core.settings

class GameBoardSize(val x: Int, val y: Int, val z: Int) {

    val size = x * y * z
}
