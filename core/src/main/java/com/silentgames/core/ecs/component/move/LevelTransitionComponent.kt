package com.silentgames.core.ecs.component.move

import com.artemis.Component
import com.silentgames.core.ecs.component.CloneableComponent
import com.silentgames.core.ecs.model.Node
import com.silentgames.support.EcsComponent

@EcsComponent
class LevelTransitionComponent(val targetList: List<Node> = listOf()) : CloneableComponent() {
    override fun clone(): Component = LevelTransitionComponent(targetList.toMutableList())
}
