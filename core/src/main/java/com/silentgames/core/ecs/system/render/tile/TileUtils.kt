package com.silentgames.core.ecs.system.render.tile // ktlint-disable filename

import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.tiled.TiledMapTile
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile
import com.silentgames.core.ecs.component.TextureComponent
import ktx.collections.GdxArray
import ktx.collections.gdxArrayOf
import ktx.collections.map

fun TextureAtlas.getTiledMapTile(textureComponent: TextureComponent): TiledMapTile? =
    getTiledMapTile(textureComponent.textureName, textureComponent.interval)

fun TextureAtlas.getTiledMapTile(textureName: String, interval: Float): TiledMapTile? {
    val regions = findRegions(textureName) ?: gdxArrayOf()
    val region = findRegion(textureName)
    return getTiledMapTile(regions, region, interval)
}

fun getTiledMapTile(
    regions: GdxArray<out TextureRegion>,
    region: TextureRegion?,
    interval: Float
): TiledMapTile? {
    return when {
        regions.size > 0 -> {
            AnimatedTiledMapTile(interval, regions.map { StaticTiledMapTile(it) })
        }
        region != null -> {
            StaticTiledMapTile(region)
        }
        else -> {
            null
        }
    }
}
