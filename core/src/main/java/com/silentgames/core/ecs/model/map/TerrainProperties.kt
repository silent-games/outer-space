package com.silentgames.core.ecs.model.map

import com.silentgames.core.ecs.model.Node

class TerrainProperties(
    val allowMove: Boolean,
    val moveCoast: Int,
    val connectionList: List<Node>,
    val transitionList: List<Node>
)
