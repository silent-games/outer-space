package com.silentgames.core.ecs.component.debug

import com.artemis.Component
import com.silentgames.core.ecs.model.Node
import com.silentgames.support.EcsComponent

@EcsComponent
class PathFinderDebugComponent(
    val checkNodes: MutableList<Node> = mutableListOf(),
    var path: List<Node> = listOf(),
    val startNode: Node = Node(),
    val endNode: Node = Node()
) : Component() {

    fun addCheckNode(node: Node) {
        checkNodes.add(node)
    }
}
