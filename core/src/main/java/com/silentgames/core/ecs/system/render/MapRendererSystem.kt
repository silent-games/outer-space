package com.silentgames.core.ecs.system.render

import com.artemis.BaseSystem
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.maps.tiled.TiledMap
import com.silentgames.core.RenderConstants.UNIT_SCALE
import com.silentgames.core.ecs.entity.LayerType
import com.silentgames.core.ecs.system.DefaultGameInputListener
import com.silentgames.core.ecs.system.MultiInputListener
import com.silentgames.core.ecs.system.render.tile.TileIterationSystem
import com.silentgames.core.model.GameState
import javax.inject.Inject

class MapRendererSystem @Inject constructor(
    spriteBatch: SpriteBatch,
    private val tiledMap: TiledMap,
    private val camera: OrthographicCamera,
    private val multiInputListener: MultiInputListener,
    private val gameState: GameState
) : BaseSystem() {

    private val orthogonalTiledMapRenderer =
        AppOrthogonalTiledMapRenderer(tiledMap, UNIT_SCALE, spriteBatch)

    private val defaultGameInputListener by lazy {
        object : DefaultGameInputListener {
            override fun changeLevel() {
                tiledMap.layers.forEach { mapLayer ->
                    val mapNames = LayerType.values().map {
                        TileIterationSystem.getLayerName(gameState.currentLevel, it.order)
                    }
                    mapLayer.isVisible = mapNames.contains(mapLayer.name)
                }
                gameState.currentLevel = if (gameState.currentLevel == 0) 1 else 0
            }
        }
    }

    override fun processSystem() {
        orthogonalTiledMapRenderer.setView(camera)
        orthogonalTiledMapRenderer.render()
    }

    override fun initialize() {
        multiInputListener.addListener(defaultGameInputListener)
    }

    override fun dispose() {
        multiInputListener.removeListener(defaultGameInputListener)
    }
}
