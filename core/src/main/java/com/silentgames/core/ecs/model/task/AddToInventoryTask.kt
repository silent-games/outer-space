package com.silentgames.core.ecs.model.task

import com.artemis.Entity
import com.silentgames.core.ecs.component.inventory.InventoryComponent
import com.silentgames.core.ecs.component.inventory.InventoryItemComponent
import com.silentgames.core.ecs.component.inventory.inventoryComponent
import com.silentgames.core.ecs.component.inventory.inventoryItemComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.select.CanSelectedComponent
import com.silentgames.core.ecs.model.task.base.TargetIdTask
import com.silentgames.core.utils.getAroundOnLevelPositionList
import com.silentgames.core.utils.has
import com.silentgames.core.utils.remove
import com.silentgames.core.utils.toNode

class AddToInventoryTask : TargetIdTask {

    constructor(item: Entity) : super(item.id)

    constructor(id: Int) : super(id)

    constructor()

    override fun execute(): Status {
        val item = targetEntity
        return if (
            entity.canPickup(item) &&
            entity.has<InventoryComponent>() &&
            item.has<InventoryItemComponent>()
        ) {
            RemoveFromInventoryTask.execute(item)
            entity.inventoryComponent.items.add(item.id)
            item.inventoryItemComponent.ownerId = entity.id
            item.transformComponent.gamePosition.set(entity.transformComponent.gamePosition)
            item.remove<CanSelectedComponent>()
            Status.SUCCEEDED
        } else {
            Status.FAILED
        }
    }

    private fun Entity.canPickup(item: Entity): Boolean {
        val unitNode = this.transformComponent.gamePosition.toNode()
        val itemNode = item.transformComponent.gamePosition.toNode()
        return if (unitNode == itemNode) {
            true
        } else {
            unitNode.getAroundOnLevelPositionList().contains(itemNode)
        }
    }
}
