package com.silentgames.core.ecs.system.render.batch

import com.artemis.Entity
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.silentgames.core.RenderConstants.UNIT_SIZE_IN_TILES_X
import com.silentgames.core.RenderConstants.UNIT_SIZE_IN_TILES_Y
import com.silentgames.core.ecs.component.TextureComponent
import com.silentgames.core.ecs.component.UnitComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.textureComponent
import com.silentgames.core.model.GameState
import com.silentgames.core.utils.allOf
import ktx.log.logger
import javax.inject.Inject

private val LOG = logger<UnitRenderSystem>()

class UnitRenderSystem @Inject constructor(
    private val spriteBatch: SpriteBatch,
    private val gameState: GameState
) : BatchSortedIterationSystem(
    allOf(TextureComponent::class, TransformComponent::class, UnitComponent::class),
    compareBy {
        prepareLevelPosition(
            it.transformComponent.gamePosition.levelNumber,
            it.transformComponent.gamePosition.positionOnLevel
        )
    }
) {

    companion object {
        private fun prepareLevelPosition(levelNumber: Int, positionOnLevel: Int): Float =
            "$levelNumber.$positionOnLevel".toFloat()
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        if (entity.transformComponent.gamePosition.levelNumber != gameState.currentLevel) return
        gameState.gameTextureAtlas.findRegion(
            entity.textureComponent.getTextureNameFor(entity.transformComponent.direction)
        ).run {
            if (texture == null) {
                LOG.error { "Entity is without a texture for rendering" }
                return
            }
            val transform = entity.transformComponent

            val isFlipX = (entity.transformComponent.direction == TransformComponent.Direction.LEFT)

            spriteBatch.draw(
                this,
                transform.gamePosition.vector.x,
                transform.gamePosition.vector.y,
                UNIT_SIZE_IN_TILES_X / 2,
                0f,
                UNIT_SIZE_IN_TILES_X,
                UNIT_SIZE_IN_TILES_Y,
                if (isFlipX) -transform.scale.x else transform.scale.x,
                transform.scale.y,
                transform.rotation
            )
        }
    }

    private fun TextureComponent.getTextureNameFor(direction: TransformComponent.Direction): String {
        val sidePostfix = "_side"
        val backPostfix = "_back"
        val frontPostfix = "_front"
        val textureName = if (this.textureName.contains(frontPostfix, true)) {
            this.textureName.replace(frontPostfix, "")
        } else {
            this.textureName
        }
        return when (direction) {
            TransformComponent.Direction.RIGHT, TransformComponent.Direction.LEFT -> textureName + sidePostfix
            TransformComponent.Direction.TOP -> textureName + backPostfix
            TransformComponent.Direction.DOWN -> textureName + frontPostfix
        }
    }
}
