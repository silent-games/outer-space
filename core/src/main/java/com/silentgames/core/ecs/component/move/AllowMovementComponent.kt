package com.silentgames.core.ecs.component.move

import com.artemis.Component
import com.silentgames.core.ecs.component.CloneableComponent
import com.silentgames.support.EcsComponent

@EcsComponent
class AllowMovementComponent : CloneableComponent() {
    override fun clone(): Component = AllowMovementComponent()
}
