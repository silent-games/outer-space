package com.silentgames.core.ecs.system.render.tile

import com.artemis.Aspect
import com.artemis.Component
import com.artemis.Entity
import com.badlogic.gdx.maps.MapLayers
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTile
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.silentgames.core.RenderConstants
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.model.GamePosition
import com.silentgames.core.ecs.system.DefaultGameInputListener
import com.silentgames.core.ecs.system.MultiInputListener
import com.silentgames.core.ecs.system.base.AppIteratingSystem
import com.silentgames.core.model.GameState
import com.silentgames.core.settings.GameSettings
import com.silentgames.core.utils.add
import com.silentgames.core.utils.entities
import com.silentgames.core.utils.remove

abstract class TileIterationSystem<U>(
    private val tiledMap: TiledMap,
    protected val gameState: GameState,
    private val multiInputListener: MultiInputListener,
    gameSettings: GameSettings,
    aspect: Aspect.Builder?
) : AppIteratingSystem(aspect) where U : Component, U : TileIterationSystem.Updated {

    companion object {
        private const val TILE_LAYER = "TILE_LAYER_"

        fun getLayerName(level: Int, positionOnLevel: Int): String {
            return TILE_LAYER + level + "_" + positionOnLevel
        }
    }

    protected val width: Int = gameSettings.gameBoardSize.x
    protected val height: Int = gameSettings.gameBoardSize.y
    private val tileWidth: Int = RenderConstants.UNIT_SIZE
    private val tileHeight: Int = RenderConstants.UNIT_SIZE

    private val defaultGameInputListener by lazy {
        object : DefaultGameInputListener {
            override fun changeLevel() {
                val actives = subscription.entities
                val ids = actives.data
                for (i in 0 until actives.size()) {
                    world.edit(ids[i]).add(createUpdateComponent())
                }
            }
        }
    }

    override fun initialize() {
        multiInputListener.addListener(defaultGameInputListener)
    }

    override fun dispose() {
        multiInputListener.removeListener(defaultGameInputListener)
    }

    override fun firstProcessed() {
        entities.forEach {
            it.add(createUpdateComponent())
        }
    }

    override fun process(entity: Entity) {
        val updated = getUpdateComponent(entity)
        if (updated != null && needUpdate(entity, updated)) {
            processTile(entity)
            updated.processed = isProcessed(entity)
        } else if (updated != null && isUpdateComponentCanRemove(entity, updated)) {
            entity.remove(updated)
        }
    }

    protected open fun processTile(entity: Entity) {
        val gamePosition = entity.transformComponent.gamePosition
        val cell = TiledMapTileLayer.Cell().apply {
            tile = createTiledMapTile(entity, world.delta)
        }
        setupCellOnLayer(cell, gamePosition)
    }

    abstract fun createUpdateComponent(): U

    abstract fun getUpdateComponent(entity: Entity): U?

    protected open fun isProcessed(entity: Entity): Boolean = true

    protected open fun needUpdate(entity: Entity, updated: Updated): Boolean = !updated.processed

    protected open fun isUpdateComponentCanRemove(entity: Entity, updated: Updated): Boolean =
        updated.processed

    abstract fun createTiledMapTile(entity: Entity, deltaTime: Float): TiledMapTile

    protected fun setupCellOnLayer(cell: TiledMapTileLayer.Cell, gamePosition: GamePosition) {
        val entityVector = gamePosition.vector
        val layer = getLayer(gamePosition.levelNumber, gamePosition.positionOnLevel)
        layer.setCell(entityVector.x.toInt(), entityVector.y.toInt(), cell)
        layer.isVisible = gamePosition.levelNumber == gameState.currentLevel
    }

    protected fun getLayer(level: Int, positionOnLevel: Int): TiledMapTileLayer {
        val name = getLayerName(level, positionOnLevel)
        return tiledMap.layers.get(name) as? TiledMapTileLayer ?: createLevel(name)
    }

    private fun createLevel(name: String): TiledMapTileLayer {
        val tiledMapTileLayer = TiledMapTileLayer(
            width,
            height,
            tileWidth,
            tileHeight
        ).apply {
            this.name = name
        }
        tiledMap.layers.add(tiledMapTileLayer)
        if (tiledMap.layers.wrongOrder()) {
            val sorted = tiledMap.layers.sortedBy { getLevelPosition(it.name) }
            val size = tiledMap.layers.size()
            for (i in 0 until size) {
                tiledMap.layers.remove(tiledMap.layers.size() - 1)
            }
            sorted.forEach {
                tiledMap.layers.add(it)
            }
        }
        return tiledMapTileLayer
    }

    private fun MapLayers.wrongOrder(): Boolean {
        var prevPos = 0f
        this.forEach {
            val pos = getLevelPosition(it.name)
            if (prevPos > pos) {
                return true
            } else {
                prevPos = pos
            }
        }
        return false
    }

    private fun getLevelPosition(name: String): Float {
        val string = name.removeRange(0, TILE_LAYER.length).split("_")
        val level = string[0].toInt()
        val positionOnLevel = string[1].toInt()
        return "$level.$positionOnLevel".toFloat()
    }

    protected fun remove(gamePosition: GamePosition) {
        val entityVector = gamePosition.vector
        val layer = getLayer(gamePosition.levelNumber, gamePosition.positionOnLevel)
        layer.setCell(entityVector.x.toInt(), entityVector.y.toInt(), null)
    }

    interface Updated {
        var processed: Boolean
    }
}
