package com.silentgames.core.ecs.component

import com.artemis.Component
import com.silentgames.support.EcsComponent

@EcsComponent
data class DescriptionComponent(val name: String = "", val description: String = "") :
    CloneableComponent() {
    override fun clone(): Component = this.copy()
}
