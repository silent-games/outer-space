package com.silentgames.core.ecs.system

import com.artemis.BaseSystem
import com.silentgames.core.ecs.component.inventory.InventoryComponent
import com.silentgames.core.ecs.component.inventory.inventoryComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.model.event.GameEvents
import com.silentgames.core.utils.has
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch
import ktx.async.RenderingScope
import javax.inject.Inject

class InventorySystem @Inject constructor(
    private val gameEvents: GameEvents
) : BaseSystem(), CoroutineScope by RenderingScope() {

    override fun initialize() {
        launch {
            // moving inventory entities with owner entity
            gameEvents.onUnitMove.observe().collect { unitMoveEvent ->
                if (unitMoveEvent.entity.has<InventoryComponent>()) {
                    unitMoveEvent.entity.inventoryComponent.getEntityItems(world).forEach {
                        it.transformComponent.gamePosition.set(
                            unitMoveEvent.entity.transformComponent.gamePosition
                        )
                    }
                }
            }
        }
    }

    override fun dispose() {
        this.coroutineContext.cancelChildren()
    }

    override fun processSystem() {}
}
