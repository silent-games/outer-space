package com.silentgames.core.ecs.system.map

import com.artemis.Entity
import com.artemis.EntitySubscription
import com.artemis.systems.IteratingSystem
import com.artemis.utils.IntBag
import com.silentgames.core.ecs.component.EnvironmentComponent
import com.silentgames.core.ecs.component.move.*
import com.silentgames.core.ecs.component.tile.UpdatedComponent
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.map.GameMap
import com.silentgames.core.ecs.model.map.TerrainProperties
import com.silentgames.core.utils.*
import javax.inject.Inject

class TerrainMapSystem @Inject constructor(private val gameMap: GameMap) : IteratingSystem(
    allOf(UpdatedComponent::class, TransformComponent::class)
) {

    private val allTilesSubscription by lazy {
        world.aspectSubscriptionManager.get(
            allOf(TransformComponent::class, EnvironmentComponent::class)
        )
    }

    private val allTransitionSubscription by lazy {
        world.aspectSubscriptionManager.get(
            allOf(
                TransformComponent::class,
                EnvironmentComponent::class,
                LevelTransitionComponent::class
            )
        )
    }

    private val allTransitionTiles: Map<Int, MutableList<Entity>> by lazy {
        allTransitionSubscription.getEntitiesFor(world).groupBy(
            { gameMap.terrainMap.getIndex(it.transformComponent.gamePosition.toNode()) },
            { it }
        )
    }

    private val allTiles: Map<Int, MutableList<Entity>> by lazy {
        allTilesSubscription.getEntitiesFor(world).groupBy(
            { gameMap.terrainMap.getIndex(it.transformComponent.gamePosition.toNode()) },
            { it }
        )
    }

    override fun initialize() {
        allTransitionSubscription.addSubscriptionListener(
            RemovedEntitySubscriptionListener { entityId ->
                allTransitionTiles.removeEntity(entityId)
                process(entityId)
            }
        )
        allTilesSubscription.addSubscriptionListener(
            RemovedEntitySubscriptionListener { entityId ->
                allTiles.removeEntity(entityId)
                process(entityId)
            }
        )
    }

    private fun Map<Int, MutableList<Entity>>.removeEntity(entityId: Int) {
        val entity = world.getEntity(entityId)
        val node = entity.transformComponent.gamePosition.toNode()
        val index = gameMap.terrainMap.getIndex(node)
        this[index]?.remove(entity)
    }

    override fun process(entityId: Int) {
        val entity = world.getEntity(entityId)
        val node = entity.transformComponent.gamePosition.toNode()
        val index = gameMap.terrainMap.getIndex(node)
        val transitionList = getTransitionList(index)
        gameMap.terrainMap.set(
            node,
            TerrainProperties(
                isAllowMovement(index),
                1,
                transitionList + node.getAroundOnLevelPositionList(),
                transitionList
            )
        )
    }

    private fun Node.getAroundOnLevelPositionList(): List<Node> {
        val gameBoardSize = gameMap.terrainMap.gameBoardSize
        return getAroundOnLevelPositionList(gameBoardSize.x, gameBoardSize.y)
    }

    private fun getTransitionList(index: Int): List<Node> {
        val entity = allTransitionTiles[index]?.first()
        return entity?.levelTransitionComponent?.targetList ?: listOf()
    }

    private fun isAllowMovement(index: Int): Boolean {
        val allTilesOnPosition = allTiles[index]
        return allTilesOnPosition?.find { it.hasNot<AllowMovementComponent>() } == null &&
            allTilesOnPosition?.find { it.has<AllowMovementComponent>() } != null
    }

    private inline fun <T, K, V> Iterable<T>.groupBy(
        keySelector: (T) -> K,
        valueTransform: (T) -> V
    ): Map<K, MutableList<V>> {
        return groupByTo(LinkedHashMap(), keySelector, valueTransform)
    }
}

private class RemovedEntitySubscriptionListener(
    val onRemoved: (entityId: Int) -> Unit
) : EntitySubscription.SubscriptionListener {
    override fun inserted(entities: IntBag?) {}

    override fun removed(entities: IntBag?) {
        val ids = entities?.data
        entities?.forEachIndex { index ->
            ids?.get(index)?.let { entityId ->
                onRemoved.invoke(entityId)
            }
        }
    }
}
