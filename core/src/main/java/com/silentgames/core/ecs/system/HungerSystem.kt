package com.silentgames.core.ecs.system

import com.artemis.systems.IntervalIteratingSystem
import com.silentgames.core.ecs.component.HungerComponent
import com.silentgames.core.ecs.component.hungerComponent
import com.silentgames.core.utils.allOf
import ktx.log.logger

private val LOG = logger<HungerSystem>()
private const val INTERVAL_SECONDS = 60f

class HungerSystem : IntervalIteratingSystem(allOf(HungerComponent::class), INTERVAL_SECONDS) {

    override fun process(entityId: Int) {
        val entity = world.getEntity(entityId)
        val hungerComponent = entity.hungerComponent
        hungerComponent.reduceSatiety()
        LOG.debug { "satiety - ${hungerComponent.satiety}" }
    }
}
