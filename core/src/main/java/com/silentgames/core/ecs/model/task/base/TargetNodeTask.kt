package com.silentgames.core.ecs.model.task.base

import com.badlogic.gdx.ai.btree.Task
import com.badlogic.gdx.ai.btree.annotation.TaskAttribute
import com.silentgames.core.ecs.model.Node

abstract class TargetNodeTask : TaskBoardTask {

    constructor()

    constructor(id: Int, node: Node) {
        this.id = id
        this.x = node.x
        this.y = node.y
        this.z = node.z
    }

    @TaskAttribute(required = true)
    @JvmField
    var id: Int = -1

    @TaskAttribute(required = true)
    @JvmField
    var x: Int = 0

    @TaskAttribute(required = true)
    @JvmField
    var y: Int = 0

    @TaskAttribute(required = true)
    @JvmField
    var z: Int = 0

    val node get() = Node(x, y, z)

    val targetEntity get() = world.getEntity(this.id)

    override fun copyTo(task: Task<TaskBoard>): Task<TaskBoard> {
        val copyTask = task as TargetNodeTask
        copyTask.x = x
        copyTask.y = y
        copyTask.z = z
        copyTask.id = id
        return task
    }
}
