package com.silentgames.core.ecs.system.render

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.renderers.BatchTiledMapRenderer
import kotlin.math.max
import kotlin.math.min

class AppOrthogonalTiledMapRenderer : BatchTiledMapRenderer {
    constructor(map: TiledMap?) : super(map) {}
    constructor(map: TiledMap?, batch: Batch?) : super(map, batch) {}
    constructor(map: TiledMap?, unitScale: Float) : super(map, unitScale) {}
    constructor(map: TiledMap?, unitScale: Float, batch: Batch?) : super(map, unitScale, batch) {}

    override fun renderTileLayer(layer: TiledMapTileLayer) {
        val batchColor = batch.color
        val color = Color.toFloatBits(
            batchColor.r,
            batchColor.g,
            batchColor.b,
            batchColor.a * layer.opacity
        )
        val layerWidth = layer.width
        val layerHeight = layer.height
        val layerTileWidth = layer.tileWidth * unitScale
        val layerTileHeight = layer.tileHeight * unitScale
        val layerOffsetX = layer.renderOffsetX * unitScale
        // offset in tiled is y down, so we flip it
        val layerOffsetY = -layer.renderOffsetY * unitScale
        val col1 = max(0, ((viewBounds.x - layerOffsetX) / layerTileWidth).toInt())
        val col2 = min(
            layerWidth,
            ((viewBounds.x + viewBounds.width + layerTileWidth - layerOffsetX) / layerTileWidth).toInt()
        )
        val row1 = max(0, ((viewBounds.y - layerOffsetY) / layerTileHeight).toInt())
        val row2 = min(
            layerHeight,
            ((viewBounds.y + viewBounds.height + layerTileHeight - layerOffsetY) / layerTileHeight).toInt()
        )
        var y = row2 * layerTileHeight + layerOffsetY
        val xStart = col1 * layerTileWidth + layerOffsetX
        val vertices = vertices
        for (row in row2 downTo row1) {
            var x = xStart
            for (col in col1 until col2) {
                val cell = layer.getCell(col, row)
                if (cell == null) {
                    x += layerTileWidth
                    continue
                }
                val tile = cell.tile
                if (tile != null) {
                    val flipX = cell.flipHorizontally
                    val flipY = cell.flipVertically
                    val rotations = cell.rotation
                    val region = tile.textureRegion
                    val x1 = x + tile.offsetX * unitScale
                    val y1 = y + tile.offsetY * unitScale
                    val x2 = x1 + region.regionWidth * unitScale
                    val y2 = y1 + region.regionHeight * unitScale
                    val u1 = region.u
                    val v1 = region.v2
                    val u2 = region.u2
                    val v2 = region.v
                    vertices[Batch.X1] = x1
                    vertices[Batch.Y1] = y1
                    vertices[Batch.C1] = color
                    vertices[Batch.U1] = u1
                    vertices[Batch.V1] = v1
                    vertices[Batch.X2] = x1
                    vertices[Batch.Y2] = y2
                    vertices[Batch.C2] = color
                    vertices[Batch.U2] = u1
                    vertices[Batch.V2] = v2
                    vertices[Batch.X3] = x2
                    vertices[Batch.Y3] = y2
                    vertices[Batch.C3] = color
                    vertices[Batch.U3] = u2
                    vertices[Batch.V3] = v2
                    vertices[Batch.X4] = x2
                    vertices[Batch.Y4] = y1
                    vertices[Batch.C4] = color
                    vertices[Batch.U4] = u2
                    vertices[Batch.V4] = v1
                    if (flipX) {
                        var temp = vertices[Batch.U1]
                        vertices[Batch.U1] = vertices[Batch.U3]
                        vertices[Batch.U3] = temp
                        temp = vertices[Batch.U2]
                        vertices[Batch.U2] = vertices[Batch.U4]
                        vertices[Batch.U4] = temp
                    }
                    if (flipY) {
                        var temp = vertices[Batch.V1]
                        vertices[Batch.V1] = vertices[Batch.V3]
                        vertices[Batch.V3] = temp
                        temp = vertices[Batch.V2]
                        vertices[Batch.V2] = vertices[Batch.V4]
                        vertices[Batch.V4] = temp
                    }
                    if (rotations != 0) {
                        when (rotations) {
                            TiledMapTileLayer.Cell.ROTATE_90 -> {
                                val tempV = vertices[Batch.V1]
                                vertices[Batch.V1] = vertices[Batch.V2]
                                vertices[Batch.V2] = vertices[Batch.V3]
                                vertices[Batch.V3] = vertices[Batch.V4]
                                vertices[Batch.V4] = tempV
                                val tempU = vertices[Batch.U1]
                                vertices[Batch.U1] = vertices[Batch.U2]
                                vertices[Batch.U2] = vertices[Batch.U3]
                                vertices[Batch.U3] = vertices[Batch.U4]
                                vertices[Batch.U4] = tempU
                            }
                            TiledMapTileLayer.Cell.ROTATE_180 -> {
                                var tempU = vertices[Batch.U1]
                                vertices[Batch.U1] = vertices[Batch.U3]
                                vertices[Batch.U3] = tempU
                                tempU = vertices[Batch.U2]
                                vertices[Batch.U2] = vertices[Batch.U4]
                                vertices[Batch.U4] = tempU
                                var tempV = vertices[Batch.V1]
                                vertices[Batch.V1] = vertices[Batch.V3]
                                vertices[Batch.V3] = tempV
                                tempV = vertices[Batch.V2]
                                vertices[Batch.V2] = vertices[Batch.V4]
                                vertices[Batch.V4] = tempV
                            }
                            TiledMapTileLayer.Cell.ROTATE_270 -> {
                                val tempV = vertices[Batch.V1]
                                vertices[Batch.V1] = vertices[Batch.V4]
                                vertices[Batch.V4] = vertices[Batch.V3]
                                vertices[Batch.V3] = vertices[Batch.V2]
                                vertices[Batch.V2] = tempV
                                val tempU = vertices[Batch.U1]
                                vertices[Batch.U1] = vertices[Batch.U4]
                                vertices[Batch.U4] = vertices[Batch.U3]
                                vertices[Batch.U3] = vertices[Batch.U2]
                                vertices[Batch.U2] = tempU
                            }
                        }
                    }
                    batch.draw(region.texture, vertices, 0, NUM_VERTICES)
                }
                x += layerTileWidth
            }
            y -= layerTileHeight
        }
    }
}
