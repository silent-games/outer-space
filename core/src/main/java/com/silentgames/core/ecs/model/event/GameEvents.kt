package com.silentgames.core.ecs.model.event

import com.silentgames.core.utils.FlowType
import com.silentgames.core.utils.KotlinFlow
import com.silentgames.core.utils.flow

class GameEvents(
    val onUnitMove: KotlinFlow<UnitMoveEvent> = flow(FlowType.EVENT)
)
