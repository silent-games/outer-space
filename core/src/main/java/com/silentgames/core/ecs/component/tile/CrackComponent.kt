package com.silentgames.core.ecs.component.tile

import com.artemis.Component
import com.artemis.Entity
import com.silentgames.core.ecs.component.healthComponentOrNull
import com.silentgames.core.ecs.component.tile.CrackComponent.State.*
import com.silentgames.core.ecs.system.render.tile.TileIterationSystem
import com.silentgames.core.utils.add
import com.silentgames.support.EcsComponent

@EcsComponent
class CrackComponent(
    val scale: Float = 1f
) : Component() {

    var state: State = REMOVED
        private set
    var frameId: Int = 0
        private set
    var maxValue: Float = 0f
        private set
    var currentValue: Float = maxValue
        private set

    fun applyCurrentValue(currentValue: Float, maxValue: Float) {
        state = when {
            currentValue > this.currentValue -> REPAIR
            currentValue < this.currentValue -> DAMAGE
            else -> IDLE
        }
        this.currentValue = currentValue
        this.maxValue = maxValue
    }

    fun setRemoved() {
        state = REMOVED
    }

    fun setIdle() {
        state = IDLE
    }

    fun nextFrame() {
        when (state) {
            REPAIR -> frameId--
            DAMAGE -> frameId++
            IDLE, REMOVED -> {
            }
        }
    }

    fun nextFrameIfNeeded(frameSize: Int) {
        if (maxValue != 0f) {
            val step = maxValue / frameSize
            val invertedHealth = maxValue - currentValue
            frameId = (invertedHealth / step).toInt()
        } else {
            nextFrame()
        }
    }

    fun getRealFrameId(size: Int) =
        when {
            frameId < 0 -> 0
            frameId in size..-1 -> size - 1
            else -> frameId
        }

    enum class State {
        REMOVED,
        IDLE,
        REPAIR,
        DAMAGE,
    }
}

class UpdatedCrackComponent(override var processed: Boolean = false) :
    Component(),
    TileIterationSystem.Updated

fun Entity.applyCrackWithHealthComponent() {
    val healthComponent = healthComponentOrNull
    val crackComponent = crackComponentOrNull
    if (crackComponent != null && healthComponent != null) {
        crackComponent.applyCurrentValue(healthComponent.currentHealth, healthComponent.maxHealth)
        add(UpdatedCrackComponent())
    }
}
