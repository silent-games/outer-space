package com.silentgames.core.ecs.system.render.batch

import com.artemis.Aspect
import com.artemis.Entity
import com.silentgames.core.utils.SortedIteratingSystem

abstract class BatchSortedIterationSystem(
    aspect: Aspect.Builder,
    comparator: Comparator<Entity>
) : SortedIteratingSystem(aspect, comparator), BatchRenderSystem {

    override var onUpdateBegin: (() -> Unit)? = null
    override var onUpdateEnd: (() -> Unit)? = null

    override fun begin() {
        onUpdateBegin?.invoke()
    }

    override fun end() {
        onUpdateEnd?.invoke()
    }
}
