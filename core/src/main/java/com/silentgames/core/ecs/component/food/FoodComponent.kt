package com.silentgames.core.ecs.component.food

import com.artemis.Component
import com.silentgames.core.ecs.component.CloneableComponent
import com.silentgames.support.EcsComponent

@EcsComponent
data class FoodComponent(
    val energy: Float = 10f,
    val saturationSpeed: Float = 2f
) : CloneableComponent() {
    override fun clone(): Component = this.copy()
}
