package com.silentgames.core.ecs.system

import com.artemis.BaseSystem
import com.artemis.Component
import com.artemis.Entity
import com.artemis.World
import com.artemis.utils.Bag
import com.silentgames.core.ecs.component.descriptionComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.select.CanSelectedComponent
import com.silentgames.core.ecs.component.select.SelectComponent
import com.silentgames.core.model.GameState
import com.silentgames.core.utils.*
import ktx.log.logger
import javax.inject.Inject

private val LOG = logger<SelectedSystem>()

class SelectedSystem @Inject constructor(
    private val multiInputListener: MultiInputListener,
    private val gameState: GameState
) : BaseSystem(), DefaultGameInputListener {

    private var nowSelected: Entity? = null

    override fun select(worldX: Float, worldY: Float, screenX: Float, screenY: Float): Boolean {
        val x = worldX.toInt()
        val y = worldY.toInt()
        val entitiesOnPosition = getWorld().getEntitiesOnPosition(x, y, gameState.currentLevel)
        val selectedEntity = entitiesOnPosition.getNextSelectedEntity()
        LOG.debug { "button: select entity: ${selectedEntity?.components?.toComponentNames()} position (x: $x y: $y)" }
        nowSelected?.remove<SelectComponent>()
        nowSelected = selectedEntity
        nowSelected?.add(SelectComponent())
        return selectedEntity != null
    }

    override fun action(worldX: Float, worldY: Float, screenX: Float, screenY: Float): Boolean {
        LOG.debug { "button: action position x: ${worldX.toInt()} y: ${worldY.toInt()}: ${gameState.currentLevel}" }
        return false
    }

    private fun List<Entity>.getNextSelectedEntity(): Entity? {
        val sortedList = this.sortedBy { it.transformComponent.gamePosition.levelNumber }
        LOG.debug { sortedList.joinToString { it.descriptionComponent.name + "_" + it.transformComponent.gamePosition.levelNumber } }
        return when (val nowSelected = nowSelected) {
            null -> sortedList.lastOrNull()
            else -> sortedList.getOrNull(sortedList.indexOf(nowSelected) - 1)
        }
    }

    override fun initialize() {
        multiInputListener.addListener(this)
    }

    override fun dispose() {
        multiInputListener.removeListener(this)
    }

    private fun Bag<Component>.toComponentNames(): String {
        return this.joinToString {
            it.javaClass.simpleName
        }
    }

    private fun World.getEntitiesOnPosition(x: Int, y: Int, z: Int) =
        this.getEntitiesFor(allOf(TransformComponent::class, CanSelectedComponent::class))
            .filter {
                it.transformComponent.gamePosition.vector.x.toInt() == x &&
                    it.transformComponent.gamePosition.vector.y.toInt() == y &&
                    it.transformComponent.gamePosition.levelNumber == z
            }

    override fun processSystem() {}
}
