package com.silentgames.core.ecs.component

import com.artemis.Component
import com.silentgames.support.EcsComponent

@EcsComponent
class SaturationComponent(energy: Float = 0f, val saturationSpeed: Float = 0f) : Component() {
    var energy: Float = energy
        private set

    fun reduceEnergy() {
        if (energy - saturationSpeed > 0) {
            energy -= saturationSpeed
        } else {
            energy = 0f
        }
    }
}
