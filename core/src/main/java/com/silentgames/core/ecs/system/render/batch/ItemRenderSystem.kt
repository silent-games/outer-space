package com.silentgames.core.ecs.system.render.batch

import com.artemis.Entity
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Scaling
import com.silentgames.core.ecs.component.TextureComponent
import com.silentgames.core.ecs.component.inventory.InventoryItemComponent
import com.silentgames.core.ecs.component.inventory.inventoryItemComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.textureComponent
import com.silentgames.core.model.GameState
import com.silentgames.core.utils.allOf
import ktx.log.logger
import javax.inject.Inject

private val LOG = logger<ItemRenderSystem>()

class ItemRenderSystem @Inject constructor(
    private val spriteBatch: SpriteBatch,
    private val gameState: GameState
) : BatchSortedIterationSystem(
    allOf(TextureComponent::class, TransformComponent::class, InventoryItemComponent::class),
    compareBy {
        prepareLevelPosition(
            it.transformComponent.gamePosition.levelNumber,
            it.transformComponent.gamePosition.positionOnLevel
        )
    }
) {
    companion object {
        private fun prepareLevelPosition(levelNumber: Int, positionOnLevel: Int): Float =
            "$levelNumber.$positionOnLevel".toFloat()
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        if (entity.inventoryItemComponent.ownerId == null &&
            entity.transformComponent.gamePosition.levelNumber == gameState.currentLevel
        ) {
            gameState.gameTextureAtlas.findRegion(entity.textureComponent.textureName).run {
                if (texture == null) {
                    LOG.error { "Entity is without a texture for rendering" }
                    return
                }
                val transform = entity.transformComponent

                val size = Scaling.fit.apply(
                    regionWidth.toFloat(),
                    regionHeight.toFloat(),
                    1f,
                    1f
                )

                val isFlipX =
                    (entity.transformComponent.direction == TransformComponent.Direction.LEFT)

                spriteBatch.draw(
                    this,
                    transform.gamePosition.vector.x,
                    transform.gamePosition.vector.y,
                    size.x / 2,
                    size.y / 2,
                    size.x,
                    size.y,
                    if (isFlipX) -transform.scale.x else transform.scale.x,
                    transform.scale.y,
                    transform.rotation
                )
            }
        }
    }
}
