package com.silentgames.core.ecs.system.render.tile

import com.artemis.Entity
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTile
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile
import com.badlogic.gdx.utils.Array
import com.silentgames.core.ecs.component.EnvironmentComponent
import com.silentgames.core.ecs.component.TextureComponent
import com.silentgames.core.ecs.component.move.AllowMovementComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.textureComponent
import com.silentgames.core.ecs.component.tile.DoorComponent
import com.silentgames.core.ecs.component.tile.DoorComponent.State.*
import com.silentgames.core.ecs.component.tile.doorComponent
import com.silentgames.core.ecs.system.MultiInputListener
import com.silentgames.core.model.GameState
import com.silentgames.core.settings.GameSettings
import com.silentgames.core.utils.add
import com.silentgames.core.utils.allOf
import com.silentgames.core.utils.remove
import ktx.collections.gdxArrayOf
import ktx.collections.lastIndex
import javax.inject.Inject

class DoorSystem @Inject constructor(
    tiledMap: TiledMap,
    gameState: GameState,
    gameSettings: GameSettings,
    multiInputListener: MultiInputListener
) : UpdatedTileIterationSystem(
    tiledMap,
    gameState,
    multiInputListener,
    gameSettings,
    allOf(
        TransformComponent::class,
        TextureComponent::class,
        EnvironmentComponent::class,
        DoorComponent::class
    )
) {

    override fun createTiledMapTile(entity: Entity, deltaTime: Float): TiledMapTile {
        val tileMap = entity.textureComponent.getTiledMapTile(entity.doorComponent, deltaTime)
        entity.changeAllowMovementState()
        return tileMap
    }

    override fun isProcessed(entity: Entity): Boolean {
        return entity.doorComponent.state == OPEN || entity.doorComponent.state == CLOSE
    }

    private fun Entity.changeAllowMovementState() {
        if (doorComponent.state == OPEN) {
            add(AllowMovementComponent())
        } else {
            remove<AllowMovementComponent>()
        }
    }

    private fun TextureComponent.getTiledMapTile(
        doorComponent: DoorComponent,
        deltaTime: Float
    ): TiledMapTile {
        val textureName = this.textureName + "_" + doorComponent.state.toPostfix().text
        val customTile = gameState.gameTextureAtlas.getTiledMapTile(textureName, this.interval)
        val regions = gameState.gameTextureAtlas.findRegions(this.textureName) ?: gdxArrayOf()
        return customTile ?: getChangingTiledMap(regions, doorComponent, deltaTime)
    }

    private fun getChangingTiledMap(
        regions: Array<TextureAtlas.AtlasRegion>,
        doorComponent: DoorComponent,
        deltaTime: Float
    ): TiledMapTile {
        val frameInterval: Float =
            if (doorComponent.state == OPENING || doorComponent.state == CLOSING) {
                doorComponent.changeProgressTime += deltaTime
                doorComponent.changeStateTime / regions.size
            } else {
                0.1f
            }

        if (doorComponent.isChangeStateDone(regions)) {
            doorComponent.setNextState()
            if (doorComponent.state == OPEN) {
                doorComponent.frameId = regions.lastIndex
            } else if (doorComponent.state == CLOSE) {
                doorComponent.frameId = 0
            }
        }
        val region = when (doorComponent.state) {
            OPEN -> {
                regions.last()
            }
            CLOSE -> {
                regions.first()
            }
            OPENING -> {
                val region = regions.get(doorComponent.frameId)
                if (doorComponent.changeProgressTime > frameInterval * doorComponent.frameId) {
                    doorComponent.incrementFrameId(regions.size)
                }
                region
            }
            CLOSING -> {
                val region = regions.get(doorComponent.frameId)
                if (doorComponent.changeProgressTime > frameInterval * (regions.lastIndex - doorComponent.frameId)) {
                    doorComponent.decrementFrameId()
                }
                region
            }
        }

        return StaticTiledMapTile(region)
    }

    private fun DoorComponent.isChangeStateDone(regions: Array<TextureAtlas.AtlasRegion>) =
        state == OPENING && frameId == regions.lastIndex || state == CLOSING && frameId == 0

    private fun DoorComponent.incrementFrameId(size: Int) {
        if (size > frameId + 1) {
            frameId++
        }
    }

    private fun DoorComponent.decrementFrameId() {
        if (frameId - 1 >= 0) {
            frameId--
        }
    }

    private fun DoorComponent.State.toPostfix() =
        when (this) {
            OPEN -> Postfix.OPEN
            OPENING -> Postfix.OPENING
            CLOSING -> Postfix.CLOSING
            CLOSE -> Postfix.CLOSE
        }

    private enum class Postfix(val text: String) {
        OPEN("open"),
        OPENING("opening"),
        CLOSING("closing"),
        CLOSE("close"),
    }
}
