package com.silentgames.core.ecs.component

import com.artemis.Component
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.model.GamePosition
import com.silentgames.support.EcsComponent

@EcsComponent
class StructureComponent(
    val structureId: Int,
    val gamePosition: GamePosition = GamePosition(Vector3(), 0),
    val speed: Float = 4f,
    val scale: Vector2 = Vector2(1.0f, 1.0f),
    var rotation: Float = 0.0f,
    var direction: TransformComponent.Direction = TransformComponent.Direction.DOWN
) : Component()
