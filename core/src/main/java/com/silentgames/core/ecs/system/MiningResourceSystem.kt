package com.silentgames.core.ecs.system

import com.artemis.systems.IntervalIteratingSystem
import com.silentgames.core.ecs.component.healthComponentOrNull
import com.silentgames.core.ecs.component.mining.MiningProgressComponent
import com.silentgames.core.ecs.component.mining.minerComponentOrNull
import com.silentgames.core.ecs.component.mining.miningProgressComponent
import com.silentgames.core.ecs.component.mining.miningResourceComponentOrNull
import com.silentgames.core.ecs.component.tile.UpdatedComponent
import com.silentgames.core.ecs.component.tile.applyCrackWithHealthComponent
import com.silentgames.core.utils.allOf
import com.silentgames.core.utils.remove
import ktx.log.logger
import javax.inject.Inject

private const val INTERVAL_SECONDS = 1f
private val LOG = logger<MiningResourceSystem>()

class MiningResourceSystem @Inject constructor() : IntervalIteratingSystem(
    allOf(MiningProgressComponent::class),
    INTERVAL_SECONDS
) {

    override fun process(entityId: Int) {
        val entity = world.getEntity(entityId)
        val targetEntity = world.getEntity(entity.miningProgressComponent.targetId)

        val minerComponent = entity.minerComponentOrNull
        val miningResourceComponent = targetEntity.miningResourceComponentOrNull
        val healthComponent = targetEntity.healthComponentOrNull

        if (miningResourceComponent != null && minerComponent != null && healthComponent != null) {
            if (healthComponent.isNotDestroyed) {
                targetEntity.applyCrackWithHealthComponent()
                healthComponent.currentHealth -= minerComponent.skillLevel / miningResourceComponent.difficulty
                LOG.debug { "Mining health ${healthComponent.percentage}" }
            } else {
                entity.remove<MiningProgressComponent>()
                healthComponent.reset()

                world.delete(targetEntity.id)

                val newEntity = world.createEntity().edit()

                miningResourceComponent.addComponentList.forEach {
                    newEntity.add(it.clone())
                }

                newEntity.add(UpdatedComponent())
                LOG.debug { "Mining complete" }
            }
        }
    }
}
