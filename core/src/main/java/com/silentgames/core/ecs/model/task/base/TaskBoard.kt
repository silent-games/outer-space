package com.silentgames.core.ecs.model.task.base

import com.artemis.Entity
import com.artemis.World

class TaskBoard(
    val currentEntity: Entity,
    val world: World
)
