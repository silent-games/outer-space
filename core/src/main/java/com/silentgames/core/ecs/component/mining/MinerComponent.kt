package com.silentgames.core.ecs.component.mining

import com.artemis.Component
import com.silentgames.support.EcsComponent

@EcsComponent
class MinerComponent(val skillLevel: Float = 1f) : Component()
