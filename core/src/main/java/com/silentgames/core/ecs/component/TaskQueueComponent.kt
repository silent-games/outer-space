package com.silentgames.core.ecs.component

import com.artemis.Component
import com.artemis.Entity
import com.artemis.World
import com.badlogic.gdx.ai.btree.Task
import com.silentgames.core.ecs.model.task.base.BehaviorQueue
import com.silentgames.core.ecs.model.task.base.TaskBoard
import com.silentgames.core.utils.getComponentLazy
import com.silentgames.support.EcsComponent

@EcsComponent
class TaskQueueComponent : Component() {

    private val behaviorQueue: BehaviorQueue<TaskBoard> = BehaviorQueue()

    fun setCurrentEntity(entity: Entity, world: World) {
        if (behaviorQueue.`object` == null) {
            behaviorQueue.`object` = TaskBoard(entity, world)
        }
    }

    fun newTaskChain(vararg tasks: Task<TaskBoard>) {
        behaviorQueue.newTaskChain(*tasks)
    }

    fun addTask(vararg tasks: Task<TaskBoard>) {
        behaviorQueue.addTask(*tasks)
    }

    fun step() {
        behaviorQueue.step()
    }

    fun hasTasks() = behaviorQueue.childCount > 0
}

fun Entity.newTaskChain(vararg tasks: Task<TaskBoard>) {
    getComponentLazy { TaskQueueComponent() }.newTaskChain(*tasks)
}

fun Entity.addTask(vararg tasks: Task<TaskBoard>) {
    getComponentLazy { TaskQueueComponent() }.addTask(*tasks)
}
