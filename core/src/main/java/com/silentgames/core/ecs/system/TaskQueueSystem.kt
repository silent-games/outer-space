package com.silentgames.core.ecs.system

import com.artemis.Entity
import com.artemis.systems.EntityProcessingSystem
import com.silentgames.core.ecs.component.TaskQueueComponent
import com.silentgames.core.ecs.component.taskQueueComponent
import com.silentgames.core.utils.allOf
import javax.inject.Inject

class TaskQueueSystem @Inject constructor() :
    EntityProcessingSystem(allOf(TaskQueueComponent::class)) {

    override fun process(entity: Entity) {
        entity.taskQueueComponent.setCurrentEntity(entity, getWorld())
        entity.taskQueueComponent.step()
    }
}
