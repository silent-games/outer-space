package com.silentgames.core.ecs.model.action

import com.artemis.Entity
import com.badlogic.gdx.ai.btree.branch.Sequence
import com.silentgames.core.ecs.component.inventory.InventoryComponent
import com.silentgames.core.ecs.component.inventory.inventoryComponent
import com.silentgames.core.ecs.component.newTaskChain
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.task.AddToInventoryTask
import com.silentgames.core.ecs.model.task.MoveToNodeTask
import com.silentgames.core.utils.has

object PickupToAction : Action {
    override val description: String = "Забрать"

    override fun isAllowAction(selected: Entity, target: Entity): Boolean {
        return selected.has<InventoryComponent>() &&
            !selected.inventoryComponent.isFull() &&
            target.has<InventoryComponent>() &&
            !target.inventoryComponent.isEmpty()
    }

    override fun executeAction(selected: Entity, target: Entity, node: Node) {
        target.inventoryComponent.items.firstOrNull()?.let { entityId ->
            selected.newTaskChain(
                Sequence(
                    MoveToNodeTask(node.copy(x = node.x + 1)),
                    AddToInventoryTask(entityId)
                )
            )
        }
    }
}
