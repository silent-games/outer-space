package com.silentgames.core.ecs.component

import com.artemis.Component
import com.silentgames.core.ecs.model.action.Action
import com.silentgames.support.EcsComponent

@EcsComponent
data class ActionComponent(val actionTypeList: List<Action> = listOf()) : CloneableComponent() {
    constructor(vararg actionTypes: Action) : this(actionTypes.toList())

    override fun clone(): Component = this.copy()
}
