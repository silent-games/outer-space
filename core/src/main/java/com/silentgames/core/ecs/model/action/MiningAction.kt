package com.silentgames.core.ecs.model.action

import com.artemis.Entity
import com.badlogic.gdx.ai.btree.branch.Sequence
import com.silentgames.core.ecs.component.mining.MinerComponent
import com.silentgames.core.ecs.component.mining.MiningResourceComponent
import com.silentgames.core.ecs.component.newTaskChain
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.task.MiningTask
import com.silentgames.core.ecs.model.task.MoveToNodeTask
import com.silentgames.core.ecs.model.task.condition.IsNearNode
import com.silentgames.core.utils.has

object MiningAction : Action {

    override val description: String = "Добыть"

    override fun isAllowAction(selected: Entity, target: Entity): Boolean {
        return selected.has<MinerComponent>() && target.has<MiningResourceComponent>()
    }

    override fun executeAction(selected: Entity, target: Entity, node: Node) {
        selected.newTaskChain(
            // todo add automatic find right pos
            Sequence(
                MoveToNodeTask(node.copy(x = node.x - 1)),
                IsNearNode(target),
                MiningTask(target.id)
            )
        )
    }
}
