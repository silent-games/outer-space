package com.silentgames.core.ecs.component.cooking

import com.artemis.Component
import com.silentgames.support.EcsComponent

@EcsComponent
class ProgressCookingComponent(val targetId: Int = 0) : Component()
