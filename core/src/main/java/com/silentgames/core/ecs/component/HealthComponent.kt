package com.silentgames.core.ecs.component

import com.artemis.Component
import com.silentgames.support.EcsComponent

@EcsComponent
class HealthComponent(val maxHealth: Float = 100f, var currentHealth: Float = maxHealth) :
    Component() {

    val isDamaged get() = maxHealth > currentHealth

    val isDestroyed get() = 0 >= currentHealth

    val isNotDestroyed get() = !isDestroyed

    val percentage: Int get() = ((currentHealth / maxHealth) * 100).toInt()

    fun reset() {
        currentHealth = maxHealth
    }
}
