package com.silentgames.core.ecs.model.action

import com.artemis.Entity
import com.silentgames.core.ecs.component.move.AllowMovementComponent
import com.silentgames.core.ecs.component.move.CanMoveComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.newTaskChain
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.task.MoveToNodeTask
import com.silentgames.core.utils.has

object MoveAction : Action {

    override val description: String = "Переместиться сюда"

    override fun isAllowAction(selected: Entity, target: Entity): Boolean {
        return selected.has<CanMoveComponent>() &&
            target.has<AllowMovementComponent>() &&
            target.transformComponent.gamePosition != selected.transformComponent.gamePosition
    }

    override fun executeAction(selected: Entity, target: Entity, node: Node) {
        selected.newTaskChain(MoveToNodeTask(node))
    }
}
