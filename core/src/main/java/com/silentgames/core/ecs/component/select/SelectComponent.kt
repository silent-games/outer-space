package com.silentgames.core.ecs.component.select

import com.artemis.Component
import com.silentgames.core.ecs.component.CloneableComponent
import com.silentgames.support.EcsComponent

@EcsComponent
class SelectComponent : CloneableComponent() {
    override fun clone(): Component = SelectComponent()
}
