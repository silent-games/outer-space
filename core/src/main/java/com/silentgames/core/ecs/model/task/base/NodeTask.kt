package com.silentgames.core.ecs.model.task.base

import com.badlogic.gdx.ai.btree.Task
import com.badlogic.gdx.ai.btree.annotation.TaskAttribute
import com.silentgames.core.ecs.model.Node

abstract class NodeTask(node: Node? = null) : TaskBoardTask() {

    @TaskAttribute(required = true)
    @JvmField
    var x: Int = node?.x ?: 0

    @TaskAttribute(required = true)
    @JvmField
    var y: Int = node?.y ?: 0

    @TaskAttribute(required = true)
    @JvmField
    var z: Int = node?.z ?: 0

    val node get() = Node(x, y, z)

    override fun copyTo(task: Task<TaskBoard>): Task<TaskBoard> {
        val moveTask = task as NodeTask
        moveTask.x = x
        moveTask.y = y
        moveTask.z = z
        return task
    }
}
