package com.silentgames.core.ecs.component.cooking

import com.artemis.Component
import com.silentgames.support.EcsComponent

@EcsComponent
class CookingComponent(val ability: Float = 0f) : Component()
