package com.silentgames.core.ecs.system.render.tile

import com.artemis.Aspect
import com.artemis.Entity
import com.badlogic.gdx.maps.tiled.TiledMap
import com.silentgames.core.ecs.component.tile.UpdatedComponent
import com.silentgames.core.ecs.system.MultiInputListener
import com.silentgames.core.model.GameState
import com.silentgames.core.settings.GameSettings
import com.silentgames.core.utils.getComponent

abstract class UpdatedTileIterationSystem(
    tiledMap: TiledMap,
    gameState: GameState,
    multiInputListener: MultiInputListener,
    gameSettings: GameSettings,
    aspect: Aspect.Builder?
) : TileIterationSystem<UpdatedComponent>(
    tiledMap,
    gameState,
    multiInputListener,
    gameSettings,
    aspect
) {

    override fun createUpdateComponent(): UpdatedComponent = UpdatedComponent()

    override fun getUpdateComponent(entity: Entity): UpdatedComponent? = entity.getComponent()
}
