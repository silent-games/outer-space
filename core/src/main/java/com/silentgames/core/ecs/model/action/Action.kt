package com.silentgames.core.ecs.model.action

import com.artemis.Entity
import com.silentgames.core.ecs.model.Node

interface Action {
    val description: String
    fun isAllowAction(selected: Entity, target: Entity): Boolean
    fun executeAction(selected: Entity, target: Entity, node: Node)
}
