package com.silentgames.core.ecs.model.task

import com.silentgames.core.ecs.component.move.GoalComponent
import com.silentgames.core.ecs.component.move.pathComponentOrNull
import com.silentgames.core.ecs.component.move.transformComponentOrNull
import com.silentgames.core.ecs.model.task.base.TargetIdTask
import com.silentgames.core.utils.add
import com.silentgames.core.utils.toNode

class MoveToTargetTask : TargetIdTask {

    constructor(id: Int) : super(id)

    constructor()

    override fun execute(): Status {
        // todo add Status.FAILED on target unreacheble
        val node = targetEntity.transformComponentOrNull?.gamePosition?.toNode()
        return when {
            node == null -> {
                Status.FAILED
            }
            status != Status.RUNNING -> {
                entity.add(GoalComponent(node))
                Status.RUNNING
            }
            entity.pathComponentOrNull == null -> {
                Status.SUCCEEDED
            }
            else -> {
                Status.RUNNING
            }
        }
    }
}
