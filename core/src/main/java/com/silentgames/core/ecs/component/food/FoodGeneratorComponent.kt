package com.silentgames.core.ecs.component.food

import com.artemis.Component
import com.silentgames.core.ecs.component.CloneableComponent
import com.silentgames.support.EcsComponent

@EcsComponent
class FoodGeneratorComponent(
    var ability: Float = 0f,
    val entity: List<CloneableComponent> = listOf(),
    var state: State = State.INACTIVE,
    progress: Float = 0f
) : Component() {

    companion object {
        const val MAX_PROGRESS: Float = 100f
    }

    var progress: Float = progress
        private set

    fun increaseProgress(speed: Float) {
        if (progress + speed > MAX_PROGRESS) {
            progress = MAX_PROGRESS
        } else {
            progress += speed
        }
    }

    fun reset() {
        progress = 0f
    }

    fun setActive() {
        state = State.ACTIVE
    }

    fun setInactive() {
        state = State.INACTIVE
    }

    enum class State {
        ACTIVE,
        INACTIVE,
    }
}
