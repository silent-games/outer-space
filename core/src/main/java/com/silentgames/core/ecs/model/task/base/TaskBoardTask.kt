package com.silentgames.core.ecs.model.task.base

import com.badlogic.gdx.ai.btree.LeafTask
import com.badlogic.gdx.ai.btree.Task

abstract class TaskBoardTask : LeafTask<TaskBoard>() {

    val entity get() = `object`.currentEntity

    val world get() = `object`.world

    override fun copyTo(task: Task<TaskBoard>): Task<TaskBoard> = task
}
