package com.silentgames.core.ecs.model

import com.badlogic.gdx.math.Vector3
import com.silentgames.core.utils.toImmutableVector2

class GamePosition(val vector3: Vector3 = Vector3(), var positionOnLevel: Int = 0) {

    val vector get() = vector3.toImmutableVector2()

    var levelNumber: Int
        get() = vector3.z.toString().substringBefore(".").toIntOrNull() ?: 0
        set(value) {
            vector3.z = value.toFloat()
        }

    fun set(gamePosition: GamePosition) {
        vector3.set(gamePosition.vector3)
        positionOnLevel = gamePosition.positionOnLevel
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GamePosition

        if (vector3 != other.vector3) return false

        return true
    }

    override fun hashCode(): Int {
        return vector3.hashCode()
    }

    fun copy() = GamePosition(vector3.cpy(), positionOnLevel)
}
