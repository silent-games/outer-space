package com.silentgames.core.ecs.system

import com.artemis.Entity
import com.artemis.systems.IntervalIteratingSystem
import com.silentgames.core.ecs.component.CloneableComponent
import com.silentgames.core.ecs.component.addTask
import com.silentgames.core.ecs.component.cooking.ProgressCookingComponent
import com.silentgames.core.ecs.component.cooking.cookingComponentOrNull
import com.silentgames.core.ecs.component.cooking.progressCookingComponent
import com.silentgames.core.ecs.component.food.FoodComponent
import com.silentgames.core.ecs.component.food.FoodGeneratorComponent
import com.silentgames.core.ecs.component.food.foodGeneratorComponentOrNull
import com.silentgames.core.ecs.model.task.AddToInventoryTask
import com.silentgames.core.utils.allOf
import com.silentgames.core.utils.remove
import ktx.log.logger
import javax.inject.Inject

private val LOG = logger<CookingSystem>()
private const val INTERVAL_SECONDS = 1f

class CookingSystem @Inject constructor() : IntervalIteratingSystem(
    allOf(ProgressCookingComponent::class),
    INTERVAL_SECONDS
) {
    override fun process(entityId: Int) {
        val entity = world.getEntity(entityId)
        val targetEntity = world.getEntity(entity.progressCookingComponent.targetId)

        val foodGeneratorComponent = targetEntity.foodGeneratorComponentOrNull
        val cookingComponent = entity.cookingComponentOrNull

        if (foodGeneratorComponent?.progress == FoodGeneratorComponent.MAX_PROGRESS) {
            foodGeneratorComponent.reset()
            entity.remove<ProgressCookingComponent>()
            targetEntity.addTask(AddToInventoryTask(this.makeFood(foodGeneratorComponent.entity)))
            LOG.debug { "ProgressCookingComponent - complete" }
        } else if (foodGeneratorComponent != null && cookingComponent != null) {
            val speed = (foodGeneratorComponent.ability + cookingComponent.ability) / 60f
            foodGeneratorComponent.increaseProgress(speed)
            LOG.debug { "progress cooking - ${foodGeneratorComponent.progress}" }
        }
    }

    private fun makeFood(prototype: List<CloneableComponent>): Entity {
        val generatedEntity = getWorld().createEntity()
        val edit = generatedEntity.edit()
        edit.add(FoodComponent())
        prototype.forEach {
            edit.add(it.clone())
        }

        return generatedEntity
    }
}
