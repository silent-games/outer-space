package com.silentgames.core.ecs.model.task

import com.badlogic.gdx.ai.btree.Task
import com.badlogic.gdx.ai.btree.annotation.TaskAttribute
import com.silentgames.core.ecs.component.inventory.InventoryItemComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.select.CanSelectedComponent
import com.silentgames.core.ecs.model.GamePosition
import com.silentgames.core.ecs.model.task.base.TargetNodeTask
import com.silentgames.core.ecs.model.task.base.TaskBoard
import com.silentgames.core.utils.add
import com.silentgames.core.utils.has
import com.silentgames.core.utils.toNode
import com.silentgames.core.utils.toVector3

class DropItemTask : TargetNodeTask {

    constructor()

    constructor(itemId: Int, gamePosition: GamePosition) : super(itemId, gamePosition.toNode()) {
        this.onLevelPosition = gamePosition.positionOnLevel
    }

    @TaskAttribute(required = true)
    @JvmField
    var onLevelPosition: Int = 0

    override fun execute(): Status {
        val item = targetEntity
        return if (item.has<InventoryItemComponent>() && RemoveFromInventoryTask.execute(item) == Status.SUCCEEDED) {
            item.add(CanSelectedComponent())
            item.transformComponent.gamePosition.set(
                GamePosition(
                    node.toVector3(),
                    onLevelPosition
                )
            )
            Status.SUCCEEDED
        } else {
            Status.FAILED
        }
    }

    override fun copyTo(task: Task<TaskBoard>): Task<TaskBoard> {
        val copyTask = task as DropItemTask
        copyTask.onLevelPosition = onLevelPosition
        return super.copyTo(task)
    }
}
