package com.silentgames.core.ecs.system.hud

import com.artemis.Aspect
import com.artemis.BaseSystem
import com.artemis.EntitySubscription
import com.artemis.World
import com.artemis.utils.IntBag
import com.badlogic.gdx.InputMultiplexer
import com.silentgames.core.ecs.component.*
import com.silentgames.core.ecs.component.ai.aiTreeBehaviourComponentOrNull
import com.silentgames.core.ecs.component.move.transformComponentOrNull
import com.silentgames.core.ecs.component.select.SelectComponent
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.system.render.debug.PathFinderDebugRenderSystem
import com.silentgames.core.model.GameState
import com.silentgames.core.model.SelectedObjectData
import com.silentgames.core.screen.base.Context
import com.silentgames.core.screen.game.WorldLoader
import com.silentgames.core.screen.game.hud.DebugHudEvent
import com.silentgames.core.screen.game.hud.GameHud
import com.silentgames.core.screen.game.hud.HudEvent
import com.silentgames.core.screen.game.hud.SelectedEntityHud
import com.silentgames.core.utils.allOf
import com.silentgames.core.utils.extractPixmap
import com.silentgames.core.utils.setSystemEnabled
import com.silentgames.core.utils.toNode
import javax.inject.Inject

class GameHudSystem @Inject constructor(
    private val context: Context,
    private val inputMultiplexer: InputMultiplexer,
    private val gameHud: GameHud,
    private val selectedEntityHud: SelectedEntityHud,
    private val gameState: GameState,
    private val worldLoader: WorldLoader
) : BaseSystem() {

    override fun initialize() {
        gameHud.onAttachContext(context)
        gameHud.onAttachInputMultiplexer(inputMultiplexer)
        gameHud.onAttach()
        getWorld().addOnSelectedListener()

        gameHud.processEvents(object : HudEvent {

            override fun onSaveClick() {
                worldLoader.save(world.aspectSubscriptionManager[Aspect.all()].entities)
            }
        })

        gameHud.processDebugEvents(object : DebugHudEvent {
            override fun onPathFinderDebugChangeState(visible: Boolean) {
                getWorld().setSystemEnabled<PathFinderDebugRenderSystem>(visible)
            }
        })
    }

    override fun dispose() {
        gameHud.onDetach()
    }

    override fun processSystem() {
        gameHud.onUpdate()
    }

    fun hide() {
        gameHud.onHide()
    }

    private fun World.addOnSelectedListener() {
        aspectSubscriptionManager.get(
            allOf(
                SelectComponent::class,
                TextureComponent::class,
                DescriptionComponent::class
            )
        )
            .addSubscriptionListener(object : EntitySubscription.SubscriptionListener {
                override fun inserted(entities: IntBag?) {
                    if (entities == null) return
                    val entity = world.getEntity(entities.get(0))

                    val textureName = entity.textureComponent.textureName
                    val descriptionComponent = entity.descriptionComponent
                    val showBteButton = entity.aiTreeBehaviourComponentOrNull != null
                    selectedEntityHud.onEntityAdded(
                        SelectedObjectData(
                            entity.id,
                            entity.transformComponentOrNull?.gamePosition?.toNode()
                                ?: Node(-1, -1, -1),
                            descriptionComponent.name,
                            descriptionComponent.description,
                            gameState.gameTextureAtlas.findRegion(textureName).extractPixmap(),
                            entity.hungerComponentOrNull,
                            showBteButton
                        )
                    )
                }

                override fun removed(entities: IntBag?) {
                    selectedEntityHud.onEntityRemoved()
                }
            })
    }
}
