package com.silentgames.core.ecs.model.task

import com.artemis.Entity
import com.silentgames.core.ecs.component.SaturationComponent
import com.silentgames.core.ecs.component.food.FoodComponent
import com.silentgames.core.ecs.component.food.foodComponent
import com.silentgames.core.ecs.component.hungerComponentOrNull
import com.silentgames.core.ecs.component.inventory.inventoryComponentOrNull
import com.silentgames.core.ecs.component.saturationComponentOrNull
import com.silentgames.core.ecs.model.task.base.TargetIdTask
import com.silentgames.core.utils.add
import com.silentgames.core.utils.has

class EatTask : TargetIdTask {

    constructor(target: Entity) : super(target.id)

    constructor()

    override fun execute(): Status {
        val item = targetEntity.inventoryComponentOrNull?.getEntityItems(world)
            ?.find { it.has<FoodComponent>() }
        val foodComponent = item?.foodComponent
        val hungerComponent = entity.hungerComponentOrNull

        return if (foodComponent != null && hungerComponent != null) {
            when {
                status != Status.RUNNING -> {
                    entity.add(
                        SaturationComponent(
                            foodComponent.energy,
                            foodComponent.saturationSpeed
                        )
                    )
                    RemoveFromInventoryTask.execute(item)
                    world.deleteEntity(item)
                    Status.RUNNING
                }
                entity.saturationComponentOrNull == null -> {
                    Status.SUCCEEDED
                }
                else -> {
                    Status.RUNNING
                }
            }
        } else {
            Status.FAILED
        }
    }
}
