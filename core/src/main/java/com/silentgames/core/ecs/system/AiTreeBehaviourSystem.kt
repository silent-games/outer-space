package com.silentgames.core.ecs.system

import com.artemis.Entity
import com.artemis.systems.EntityProcessingSystem
import com.badlogic.gdx.ai.btree.utils.BehaviorTreeLibrary
import com.badlogic.gdx.ai.btree.utils.BehaviorTreeLibraryManager
import com.silentbugs.bte.EditorBehaviourTreeLibrary
import com.silentgames.core.ecs.component.ai.AiTreeBehaviourComponent
import com.silentgames.core.ecs.component.ai.aiTreeBehaviourComponent
import com.silentgames.core.ecs.component.ai.stepProcessorATBComponentOrNull
import com.silentgames.core.ecs.component.taskQueueComponentOrNull
import com.silentgames.core.utils.allOf
import javax.inject.Inject

class AiTreeBehaviourSystem @Inject constructor() :
    EntityProcessingSystem(allOf(AiTreeBehaviourComponent::class)) {

    override fun initialize() {
        val libraryManager = BehaviorTreeLibraryManager.getInstance()
        // EditorBehaviourTreeLibrary should be used for some extra things
        val library: BehaviorTreeLibrary = EditorBehaviourTreeLibrary()
        libraryManager.library = library

//        libraryManager.createBehaviorTree("core/assets/ai/test", TaskBoard())

//        val include: Include<TaskBoard> = Include<TaskBoard>()
//        include.lazy = false
//        include.subtree = "test.actual"
//        val includeBehavior: BehaviorTree<TaskBoard> = BehaviorTree(include)
//        library.registerArchetypeTree("test", includeBehavior)
//
//
//        val behaviorTree: BehaviorTree<TaskBoard> = BehaviorTree(
//                RandomSelector(
//                        MoveTask(),
//                        MoveTask(),
//                        MoveTask(),
//                        MoveTask(),
//                )
//        )
//
//        val actualBehavior: BehaviorTree<TaskBoard> = BehaviorTree(behaviorTree)
//        library.registerArchetypeTree("test.actual", behaviorTree)
    }

    override fun process(entity: Entity) {
        val aiTreeBehaviourComponent = entity.aiTreeBehaviourComponent
        val canProcessed = entity.stepProcessorATBComponentOrNull != null
        aiTreeBehaviourComponent.setCurrentEntity(entity, getWorld())
        val taskQueueComponent = entity.taskQueueComponentOrNull
        if (canProcessed && (taskQueueComponent == null || !taskQueueComponent.hasTasks())) {
            aiTreeBehaviourComponent.step()
        }
    }
}
