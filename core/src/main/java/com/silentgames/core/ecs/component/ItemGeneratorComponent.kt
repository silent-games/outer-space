package com.silentgames.core.ecs.component

import com.artemis.Component
import com.silentgames.support.EcsComponent

@EcsComponent
class ItemGeneratorComponent(
    val entitiesList: List<List<CloneableComponent>> = listOf(),
    startProgress: Float = 0f,
    private val maxProgress: Float = 100f,
    private val generationSpeed: Float = 10f
) : Component() {

    constructor(vararg entityComponents: List<CloneableComponent>) : this(entityComponents.toList())

    var progress: Float = startProgress
        private set(value) {
            field = value
            onProgressChanged.invoke(field)
        }

    @Transient
    var onProgressChanged: ((progress: Float) -> Unit) = {}

    fun processProgress(): Boolean {
        return if (progress + generationSpeed >= maxProgress) {
            progress = maxProgress
            true
        } else {
            progress += generationSpeed
            false
        }
    }

    fun reset() {
        progress = 0f
    }

    fun isComplete() = progress >= maxProgress
}
