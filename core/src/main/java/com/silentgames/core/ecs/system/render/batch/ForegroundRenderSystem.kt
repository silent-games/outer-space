package com.silentgames.core.ecs.system.render.batch

import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.silentgames.core.model.GameState
import com.silentgames.core.parallax.ParallaxBackground
import com.silentgames.core.parallax.ParallaxLayer
import com.silentgames.core.parallax.TextureRegionParallaxLayer
import com.silentgames.core.utils.GameTexture
import com.silentgames.core.utils.getGameTextureRegion
import javax.inject.Inject

class ForegroundRenderSystem @Inject constructor(
    private val spriteBatch: SpriteBatch,
    private val gameState: GameState,
    private val camera: Camera
) : BatchEntitySystem(), BatchRenderSystem {

    companion object {
        private const val REGION_WIDTH = 30f
        private const val REGION_HEIGHT = 30f
        private const val PARALLAX_RATIO_X = .5f
        private const val PARALLAX_RATIO_Y = .5f
    }

    private val parallaxBackground: ParallaxBackground by lazy {
        ParallaxBackground().apply {
            this.addLayers(foregroundLayer)
        }
    }

    private val foregroundLayer by lazy {
        TextureRegionParallaxLayer(
            gameState.gameTextureAtlas.getGameTextureRegion(GameTexture.FOREGROUND),
            REGION_WIDTH,
            REGION_HEIGHT,
            Vector2(PARALLAX_RATIO_X, PARALLAX_RATIO_Y)
        ).apply {
            tileModeX = ParallaxLayer.TileMode.SINGLE
            tileModeY = ParallaxLayer.TileMode.SINGLE
        }
    }

    override fun draw(deltaTime: Float) {
        parallaxBackground.draw(camera as OrthographicCamera, spriteBatch)
    }
}
