package com.silentgames.core.ecs.model.action

import com.artemis.Entity
import com.badlogic.gdx.ai.btree.branch.Sequence
import com.silentgames.core.ecs.component.cooking.CookingComponent
import com.silentgames.core.ecs.component.food.FoodGeneratorComponent
import com.silentgames.core.ecs.component.food.foodGeneratorComponent
import com.silentgames.core.ecs.component.inventory.inventoryComponentOrNull
import com.silentgames.core.ecs.component.newTaskChain
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.task.CookTask
import com.silentgames.core.ecs.model.task.MoveToNodeTask
import com.silentgames.core.ecs.model.task.condition.IsNearNode
import com.silentgames.core.utils.has

object CookAction : Action {
    override val description: String = "Готовить"

    override fun isAllowAction(selected: Entity, target: Entity): Boolean {
        return selected.has<CookingComponent>() &&
            target.has<FoodGeneratorComponent>() &&
            target.foodGeneratorComponent.state == FoodGeneratorComponent.State.INACTIVE &&
            target.inventoryComponentOrNull?.isFull() == false
    }

    override fun executeAction(selected: Entity, target: Entity, node: Node) {
        selected.newTaskChain(
            // todo add automatic find right pos for cooking
            Sequence(
                MoveToNodeTask(node.copy(y = node.y - 1)),
                IsNearNode(target),
                CookTask(target)
            )
        )
    }
}
