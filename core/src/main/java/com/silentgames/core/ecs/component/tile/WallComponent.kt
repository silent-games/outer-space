package com.silentgames.core.ecs.component.tile

import com.artemis.Component
import com.silentgames.support.EcsComponent

@EcsComponent
class WallComponent : Component()
