package com.silentgames.core.ecs.system

import com.artemis.BaseSystem
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.utils.viewport.Viewport
import com.silentgames.core.screen.base.Context
import ktx.collections.gdxArrayOf
import javax.inject.Inject

class GameInputSystem @Inject constructor(
    context: Context,
    camera: OrthographicCamera,
    gameInputListener: GameInputListener,
    private val inputMultiplexer: InputMultiplexer,
    private val gameInputController: GameInputController
) : BaseSystem() {

    private val cameraInputController by lazy {
        gameInputController.apply {
            setCamera(camera)
            setGameInputListener(gameInputListener)
            setViewPort(context.stage.viewport)
        }
        gameInputController.getInputProcessor()
    }

    override fun initialize() {
        inputMultiplexer.addProcessor(cameraInputController)
    }

    override fun dispose() {
        inputMultiplexer.removeProcessor(cameraInputController)
    }

    override fun processSystem() {}
}

interface GameInputController {

    fun setViewPort(viewport: Viewport)
    fun setCamera(orthographicCamera: OrthographicCamera)
    fun setGameInputListener(gameInputListener: GameInputListener)

    fun getInputProcessor(): InputProcessor
}

abstract class SimpleGameInputController : GameInputController {

    protected lateinit var viewport: Viewport
    protected lateinit var orthographicCamera: OrthographicCamera
    protected lateinit var gameCameraInputListener: GameInputListener

    override fun setViewPort(viewport: Viewport) {
        this.viewport = viewport
    }

    override fun setCamera(orthographicCamera: OrthographicCamera) {
        this.orthographicCamera = orthographicCamera
    }

    override fun setGameInputListener(gameInputListener: GameInputListener) {
        this.gameCameraInputListener = gameInputListener
    }
}

interface GameInputListener {

    fun select(worldX: Float, worldY: Float, screenX: Float, screenY: Float): Boolean

    fun action(worldX: Float, worldY: Float, screenX: Float, screenY: Float): Boolean

    fun changeLevel()
}

interface DefaultGameInputListener : GameInputListener {
    override fun select(worldX: Float, worldY: Float, screenX: Float, screenY: Float): Boolean =
        false

    override fun action(worldX: Float, worldY: Float, screenX: Float, screenY: Float): Boolean =
        false

    override fun changeLevel() {}
}

class MultiInputListener : GameInputListener {

    private val list = gdxArrayOf<GameInputListener>()

    fun addListener(gameInputListener: GameInputListener) {
        list.add(gameInputListener)
    }

    fun removeListener(gameInputListener: GameInputListener) {
        list.removeValue(gameInputListener, true)
    }

    override fun select(worldX: Float, worldY: Float, screenX: Float, screenY: Float): Boolean {
        var selected = false
        list.forEach {
            if (it.select(worldX, worldY, screenX, screenY)) {
                selected = true
            }
        }
        return selected
    }

    override fun action(worldX: Float, worldY: Float, screenX: Float, screenY: Float): Boolean {
        var action = false
        list.forEach {
            if (it.action(worldX, worldY, screenX, screenY)) {
                action = it.action(worldX, worldY, screenX, screenY)
            }
        }
        return action
    }

    override fun changeLevel() {
        list.forEach {
            it.changeLevel()
        }
    }
}

enum class MouseButton {
    LEFT,
    RIGHT,
    MIDDLE,
    BACK,
    FORWARD
}
