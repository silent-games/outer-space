package com.silentgames.core.ecs.entity // ktlint-disable filename

import com.artemis.World
import com.silentgames.core.ecs.component.*
import com.silentgames.core.ecs.component.ai.AiTreeBehaviourComponent
import com.silentgames.core.ecs.component.cooking.CookingComponent
import com.silentgames.core.ecs.component.inventory.InventoryComponent
import com.silentgames.core.ecs.component.mining.MinerComponent
import com.silentgames.core.ecs.component.move.CanMoveComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.select.CanSelectedComponent
import com.silentgames.core.ecs.model.action.DropAction
import com.silentgames.core.ecs.model.action.EatAction
import com.silentgames.core.utils.GameTexture

fun World.addUnit(x: Int, y: Int, level: Int, name: String, description: String) {
    createEntity().edit()
        .add(CanSelectedComponent())
        .add(ActionComponent(DropAction, EatAction))
        .add(DescriptionComponent(name, description))
        .add(CanMoveComponent())
        .add(TransformComponent(x, y, level, LayerType.UNIT))
        .add(TextureComponent(GameTexture.UNIT.id))
        .add(HungerComponent())
        .add(UnitComponent())
        .add(InventoryComponent())
        .add(CookingComponent(ability = 20f))
        .add(AiTreeBehaviourComponent())
        .add(MinerComponent())
}
