package com.silentgames.core.ecs.system.render.batch

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.silentgames.core.RenderConstants
import com.silentgames.core.model.GameState
import com.silentgames.core.utils.GameTexture
import com.silentgames.core.utils.getGameTextureRegion
import javax.inject.Inject

class BackgroundRenderSystem @Inject constructor(
    private val spriteBatch: SpriteBatch,
    private val gameState: GameState,
    private val camera: OrthographicCamera
) : BatchEntitySystem() {

    override fun draw(deltaTime: Float) {
        gameState.gameTextureAtlas.getGameTextureRegion(GameTexture.BACKGROUND).run {
            val width = prepareSize(RenderConstants.WIDTH)
            val height = prepareSize(RenderConstants.HEIGHT)
            spriteBatch.draw(
                this,
                camera.position.x - width / 2,
                camera.position.y - height / 2,
                width,
                height
            )
        }
    }

    private fun prepareSize(size: Float) = if (camera.zoom < 1) size else size * camera.zoom
}
