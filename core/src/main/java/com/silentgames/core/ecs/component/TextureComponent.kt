package com.silentgames.core.ecs.component

import com.artemis.Component
import com.silentgames.core.utils.GameTexture
import com.silentgames.support.EcsComponent

@EcsComponent
data class TextureComponent(val textureName: String = "", val interval: Float = 0.1f) :
    CloneableComponent() {

    constructor(gameTexture: GameTexture, interval: Float) : this(gameTexture.id, interval)

    constructor(gameTexture: GameTexture) : this(gameTexture.id, 0.1f)

    override fun clone(): Component = this.copy()
}
