package com.silentgames.core.ecs.system.hud

import com.artemis.Entity
import com.artemis.systems.EntityProcessingSystem
import com.silentgames.core.ecs.component.ai.AiTreeBehaviourComponent
import com.silentgames.core.screen.base.Context
import com.silentgames.core.screen.game.hud.BehaviourTreeEditor
import com.silentgames.core.utils.allOf
import javax.inject.Inject

class DebugBteSystem @Inject constructor(
    private val context: Context,
    private val behaviourTreeEditor: BehaviourTreeEditor
) : EntityProcessingSystem(allOf(AiTreeBehaviourComponent::class)) {

    override fun initialize() {
        behaviourTreeEditor.initialize(context)
    }

    override fun process(entity: Entity) {
        behaviourTreeEditor.step(entity, world.delta)
    }
}
