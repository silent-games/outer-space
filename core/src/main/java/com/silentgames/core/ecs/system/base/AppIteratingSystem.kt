package com.silentgames.core.ecs.system.base

import com.artemis.Aspect
import com.artemis.Entity
import com.artemis.systems.IteratingSystem

abstract class AppIteratingSystem(aspect: Aspect.Builder?) : IteratingSystem(aspect) {

    private var firstProcess = true

    protected open fun firstProcessed() {
    }

    override fun process(entityId: Int) {
        if (firstProcess) {
            firstProcess = false
            firstProcessed()
        }
        process(world.getEntity(entityId))
    }

    abstract fun process(entity: Entity)
}
