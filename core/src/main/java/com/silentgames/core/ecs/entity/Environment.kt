package com.silentgames.core.ecs.entity

import com.artemis.EntityEdit
import com.artemis.World
import com.silentgames.core.ecs.component.*
import com.silentgames.core.ecs.component.food.FoodGeneratorComponent
import com.silentgames.core.ecs.component.inventory.InventoryComponent
import com.silentgames.core.ecs.component.mining.MiningResourceComponent
import com.silentgames.core.ecs.component.move.AllowMovementComponent
import com.silentgames.core.ecs.component.move.LevelTransitionComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.select.CanSelectedComponent
import com.silentgames.core.ecs.component.tile.CrackComponent
import com.silentgames.core.ecs.component.tile.DoorComponent
import com.silentgames.core.ecs.component.tile.TileComponent
import com.silentgames.core.ecs.component.tile.WallComponent
import com.silentgames.core.ecs.entity.inventory.getFoodComponents
import com.silentgames.core.ecs.entity.inventory.getItemComponents
import com.silentgames.core.ecs.entity.inventory.getMinedResourceComponents
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.action.*
import com.silentgames.core.utils.GameTexture

fun World.addSpace(x: Int, y: Int, level: Int) {
    createEntity().edit()
        .add(TileComponent())
        .add(AllowMovementComponent())
        .add(ActionComponent(MoveAction))
        .add(EnvironmentComponent())
        .add(DescriptionComponent("Космос", "Космос"))
        .add(CanSelectedComponent())
        .add(TransformComponent(x, y, level, LayerType.BACKGROUND))
}

fun World.addShipFloor(x: Int, y: Int, level: Int) {
    createEntity().edit()
        .add(TileComponent())
        .add(AllowMovementComponent())
        .add(ActionComponent(MoveAction))
        .addBaseEnvironmentComponents(
            x,
            y,
            level,
            "Пол",
            "Обычнй пол",
            LayerType.BACKGROUND,
            GameTexture.SPACE_SHIP_FLOOR
        )
}

fun World.addWall(x: Int, y: Int, level: Int) {
    createEntity().edit()
        .add(WallComponent())
        .addBaseEnvironmentComponents(
            x,
            y,
            level,
            "Стена",
            "Обычная стена",
            LayerType.ENVIRONMENT,
            GameTexture.WALL
        )
}

fun World.addStairs(x: Int, y: Int, level: Int, targetLevel: Int) {
    createEntity().edit()
        .add(TileComponent())
        .add(AllowMovementComponent())
        .add(ActionComponent(UseStairsAction(level, targetLevel)))
        .add(LevelTransitionComponent(listOf(Node(x, y, targetLevel))))
        .addBaseEnvironmentComponents(
            x,
            y,
            level,
            "Лестница",
            "Деревянная лестница на космическом корабле",
            LayerType.ENVIRONMENT,
            GameTexture.STAIRS
        )
}

fun World.addItemGenerator(x: Int, y: Int, level: Int) {
    createEntity().edit()
        .add(TileComponent())
        .add(ItemGeneratorComponent(getItemComponents(x, y, level)))
        .add(InventoryComponent())
        .add(ActionComponent(PickupToAction))
        .addBaseEnvironmentComponents(
            x,
            y,
            level,
            "Генератор сырья",
            "Неведомая штука",
            LayerType.ENVIRONMENT,
            GameTexture.WALL
        )
}

fun World.addFoodGenerator(x: Int, y: Int, level: Int) {
    createEntity().edit()
        .add(TileComponent())
        .add(InventoryComponent())
        .add(FoodGeneratorComponent(20f, getFoodComponents(x, y, level)))
        .add(ActionComponent(CookAction, PickupToAction))
        .addBaseEnvironmentComponents(
            x,
            y,
            level,
            "Генератор еды",
            "Производит пищу по требованию",
            LayerType.ENVIRONMENT,
            GameTexture.KITCHEN_STOVE
        )
}

fun World.addDoor(x: Int, y: Int, level: Int) {
    createEntity().edit()
        .add(DoorComponent())
        .add(ActionComponent(OpenDoorAction, CloseDoorAction))
        .addBaseEnvironmentComponents(
            x,
            y,
            level,
            "Дверь",
            "Двеееерь",
            LayerType.ENVIRONMENT,
            GameTexture.DOOR
        )
}

fun World.addResourceRedCrystal(x: Int, y: Int, level: Int) {
    addResource(
        x,
        y,
        level,
        "Астероид",
        "Космический объект с красными кристалами",
        DescriptionComponent("Красные кристалы", "Кристалы красные"),
        GameTexture.ASTEROID_V_1,
        GameTexture.CRYSTAL_RED
    )
}

fun World.addResourceAqua(x: Int, y: Int, level: Int) {
    addResource(
        x,
        y,
        level,
        "Астероид с водой",
        "Кусок камня на котором есть немного воды",
        DescriptionComponent("Замороженая вода", "Вода в кристалах"),
        GameTexture.ASTEROID_V_2,
        GameTexture.CRYSTAL_AQUA
    )
}

private fun World.addResource(
    x: Int,
    y: Int,
    level: Int,
    name: String,
    description: String,
    resourceDescription: DescriptionComponent,
    asteroidGameTexture: GameTexture,
    resourceGameTexture: GameTexture
) {
    createEntity().edit()
        .add(TileComponent())
        .add(
            MiningResourceComponent(
                addComponentList = getMinedResourceComponents(
                    x,
                    y,
                    level,
                    resourceDescription.name,
                    resourceDescription.description,
                    resourceGameTexture
                )
            )
        )
        .add(CrackComponent(scale = 0.7f))
        .add(HealthComponent(50f))
        .add(ActionComponent(MiningAction))
        .addBaseEnvironmentComponents(
            x,
            y,
            level,
            name,
            description,
            LayerType.ENVIRONMENT,
            asteroidGameTexture
        )
}

fun EntityEdit.addBaseEnvironmentComponents(
    x: Int,
    y: Int,
    level: Int,
    name: String,
    description: String,
    layerType: LayerType,
    texture: GameTexture
) {
    add(EnvironmentComponent())
    add(DescriptionComponent(name, description))
    add(CanSelectedComponent())
    add(TextureComponent(texture.id))
    add(TransformComponent(x, y, level, layerType))
}
