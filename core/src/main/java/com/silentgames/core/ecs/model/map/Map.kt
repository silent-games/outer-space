package com.silentgames.core.ecs.model.map

import com.silentgames.core.ecs.model.Node
import com.silentgames.core.settings.GameBoardSize

class Map<T>(val gameBoardSize: GameBoardSize, private val array: Array<T?>) {

    fun set(x: Int, y: Int, z: Int, data: T) {
        set(Node(x, y, z), data)
    }

    fun set(node: Node, data: T) {
        val index = getIndex(node)
        array[index] = data
    }

    fun get(x: Int, y: Int, z: Int): Cell<T?> = get(Node(x, y, z))

    fun get(node: Node): Cell<T?> {
        val index = getIndex(node)
        return if (array.size > index) {
            Cell(node, array[index])
        } else {
            Cell(node, null)
        }
    }

    fun getSize() = gameBoardSize.size

    fun getIndex(node: Node): Int {
        val nodeIndexOnZeroLevel = node.x * gameBoardSize.x + node.y
        val countNodesOnPreviousLevels = gameBoardSize.x * gameBoardSize.y * node.z
        return nodeIndexOnZeroLevel + countNodesOnPreviousLevels
    }
}
