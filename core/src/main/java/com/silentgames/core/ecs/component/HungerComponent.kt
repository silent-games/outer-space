package com.silentgames.core.ecs.component

import com.artemis.Component
import com.silentgames.support.EcsComponent

@EcsComponent
class HungerComponent(
    val maxSatiety: Float = 100f,
    satiety: Float = maxSatiety,
    private val hungerSpeed: Float = 1f
) : CloneableComponent() {

    var satiety: Float = satiety
        private set(value) {
            field = value
            onSatietyChanged.invoke(field)
        }

    @Transient
    var onSatietyChanged: ((satiety: Float) -> Unit) = {}

    fun reduceSatiety() {
        if (satiety - hungerSpeed > 0) {
            satiety -= hungerSpeed
        } else {
            satiety = 0f
        }
    }

    fun increaseSatiety(speed: Float) {
        if (satiety + speed > maxSatiety) {
            satiety = maxSatiety
        } else {
            satiety += speed
        }
    }

    override fun clone(): Component = HungerComponent(maxSatiety, satiety, hungerSpeed)
}
