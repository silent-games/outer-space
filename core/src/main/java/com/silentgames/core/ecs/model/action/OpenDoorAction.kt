package com.silentgames.core.ecs.model.action

import com.artemis.Entity
import com.badlogic.gdx.ai.btree.branch.Sequence
import com.silentgames.core.ecs.component.UnitComponent
import com.silentgames.core.ecs.component.newTaskChain
import com.silentgames.core.ecs.component.tile.DoorComponent
import com.silentgames.core.ecs.component.tile.doorComponent
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.task.MoveToNodeTask
import com.silentgames.core.ecs.model.task.OpenDoorTask
import com.silentgames.core.ecs.model.task.condition.IsNearNode
import com.silentgames.core.utils.has

object OpenDoorAction : Action {

    override val description: String = "Открыть дверь"

    override fun isAllowAction(selected: Entity, target: Entity): Boolean {
        return selected.has<UnitComponent>() &&
            target.has<DoorComponent>() &&
            (
                target.doorComponent.state == DoorComponent.State.CLOSE ||
                    target.doorComponent.state == DoorComponent.State.CLOSING
                )
    }

    override fun executeAction(selected: Entity, target: Entity, node: Node) {
        selected.newTaskChain(
            // todo add automatic find right pos for opening door
            Sequence(
                MoveToNodeTask(node.copy(y = node.y - 1)),
                IsNearNode(target),
                OpenDoorTask(target)
            )
        )
    }
}
