package com.silentgames.core.ecs.component.ai

import com.artemis.Component
import com.artemis.Entity
import com.artemis.World
import com.badlogic.gdx.ai.btree.BehaviorTree
import com.badlogic.gdx.ai.btree.utils.BehaviorTreeLibraryManager
import com.silentgames.core.ecs.model.task.base.TaskBoard
import com.silentgames.support.EcsComponent

@EcsComponent
class AiTreeBehaviourComponent : Component() {

    var behaviorTree: BehaviorTree<TaskBoard> =
        BehaviorTreeLibraryManager.getInstance().createBehaviorTree("core/assets/ai/test4")
        private set

    fun setCurrentEntity(entity: Entity, world: World) {
        if (behaviorTree.`object` == null) {
            behaviorTree.`object` = TaskBoard(entity, world)
        }
    }

    fun step() {
        behaviorTree.step()
    }
}
