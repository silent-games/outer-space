package com.silentgames.core.ecs.system.render.debug

import com.artemis.Entity
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.silentgames.core.RenderConstants
import com.silentgames.core.ecs.component.debug.PathFinderDebugComponent
import com.silentgames.core.ecs.component.debug.pathFinderDebugComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.system.render.batch.BatchSortedIterationSystem
import com.silentgames.core.model.GameState
import com.silentgames.core.utils.GameTexture
import com.silentgames.core.utils.allOf
import javax.inject.Inject

class PathFinderDebugRenderSystem @Inject constructor(
    private val spriteBatch: SpriteBatch,
    private val gameState: GameState
) : BatchSortedIterationSystem(
    allOf(TransformComponent::class, PathFinderDebugComponent::class),
    compareBy {
        prepareLevelPosition(
            it.transformComponent.gamePosition.levelNumber,
            it.transformComponent.gamePosition.positionOnLevel
        )
    }
) {
    companion object {
        private fun prepareLevelPosition(levelNumber: Int, positionOnLevel: Int): Float =
            "$levelNumber.$positionOnLevel".toFloat()
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        gameState.gameTextureAtlas.findRegion(GameTexture.PATH_DEBUG.id).run {
            if (texture == null) {
                return
            }
            entity.pathFinderDebugComponent.checkNodes.filter { it.z == gameState.currentLevel }
                .forEach {
                    spriteBatch.draw(
                        this,
                        it.x.toFloat(),
                        it.y.toFloat(),
                        RenderConstants.UNIT_SIZE_IN_TILES_X / 2,
                        0f,
                        RenderConstants.UNIT_SIZE_IN_TILES_X,
                        RenderConstants.UNIT_SIZE_IN_TILES_Y,
                        1.0f,
                        1.0f,
                        0.0f
                    )
                }
        }
    }
}
