package com.silentgames.core.ecs.component.move

import com.artemis.Component
import com.silentgames.core.ecs.component.CloneableComponent
import com.silentgames.core.ecs.model.Node
import com.silentgames.support.EcsComponent

@EcsComponent
data class PathComponent(val path: List<Node> = listOf(), private var localCurrentNodeId: Int = 0) :
    CloneableComponent() {

    val nextNode: Node?
        get() = if (localCurrentNodeId + 1 >= path.size) path.lastOrNull() else path.get(
            localCurrentNodeId + 1
        )

    val currentNode: Node? get() = path.getOrNull(localCurrentNodeId)

    val currentNodeId get() = localCurrentNodeId

    fun increaseCurrentNode() {
        localCurrentNodeId++
    }

    override fun clone(): Component = this.copy()
}
