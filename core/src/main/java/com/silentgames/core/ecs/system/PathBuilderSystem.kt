package com.silentgames.core.ecs.system

import com.artemis.systems.IteratingSystem
import com.badlogic.gdx.ai.pfa.Connection
import com.badlogic.gdx.ai.pfa.DefaultConnection
import com.badlogic.gdx.ai.pfa.DefaultGraphPath
import com.badlogic.gdx.ai.pfa.Heuristic
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph
import com.badlogic.gdx.utils.Array
import com.silentgames.core.ecs.component.debug.PathFinderDebugComponent
import com.silentgames.core.ecs.component.debug.pathFinderDebugComponent
import com.silentgames.core.ecs.component.move.*
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.map.GameMap
import com.silentgames.core.utils.*
import ktx.collections.gdxArrayOf
import ktx.collections.isNotEmpty
import ktx.collections.toGdxArray
import ktx.log.logger
import javax.inject.Inject

private val LOG = logger<PathBuilderSystem>()

class PathBuilderSystem @Inject constructor(
    private val gameMap: GameMap
) : IteratingSystem(
    allOf(TransformComponent::class, GoalComponent::class)
) {

    private val metrics by lazy { IndexedAStarPathFinder.Metrics<Node>() }

    private val indexedAStarPathFinder by lazy {
        IndexedAStarPathFinder(GameIndexedGraph(gameMap), metrics)
    }

    private val gameHeuristic = GameHeuristic()

    private val path = DefaultGraphPath<Node>()

    override fun process(entityId: Int) {
        val entity = getWorld().getEntity(entityId)

        val startNode = entity.transformComponent.gamePosition.toNode()
        val goalNode = entity.goalComponent.target

        LOG.debug { "startNode $startNode goalNode $goalNode" }

        path.clear()

        metrics.setCallback(object : IndexedAStarPathFinder.Metrics.Callback<Node> {
            override fun onVisited(node: Node) {
                if (entity.has<PathFinderDebugComponent>()) {
                    entity.pathFinderDebugComponent.also {
                        it.path = path.nodes.toMutableList()
                        it.addCheckNode(node)
                    }
                } else {
                    entity.add(
                        PathFinderDebugComponent(
                            startNode = startNode,
                            endNode = goalNode,
                            path = path.nodes.toMutableList(),
                            checkNodes = mutableListOf(node)
                        )
                    )
                }
            }

            override fun onReset() {
                entity.remove<PathFinderDebugComponent>()
            }
        })

        indexedAStarPathFinder.searchNodePath(startNode, goalNode, gameHeuristic, path)

        if (path.nodes.isNotEmpty()) {
            path.nodes.forEach {
                LOG.debug { "Node $it" }
            }
            entity.add(PathComponent(path.nodes.toList()))
        } else {
            entity.pathComponentOrNull?.currentNode?.let {
                entity.add(PathComponent(listOf(it)))
            }
        }
        entity.remove<GoalComponent>()
    }
}

class GameIndexedGraph(
    private val gameMap: GameMap
) : IndexedGraph<Node> {

    override fun getConnections(fromNode: Node): Array<Connection<Node>> {
        val cell = gameMap.terrainMap.get(fromNode).data ?: return gdxArrayOf()
        return cell.connectionList.mapNotNull {
            if (gameMap.terrainMap.get(it).data?.allowMove == true) {
                DefaultConnection(fromNode, it)
            } else {
                null
            }
        }.toGdxArray()
    }

    override fun getIndex(node: Node): Int = gameMap.terrainMap.getIndex(node)

    override fun getNodeCount(): Int = gameMap.terrainMap.getSize()
}

class GameHeuristic : Heuristic<Node> {

    override fun estimate(node: Node, endNode: Node): Float {
        return endNode.toVector3().sub(node.toVector3()).len()
    }
}
