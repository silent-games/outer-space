package com.silentgames.core.ecs.system

import com.artemis.systems.IntervalIteratingSystem
import com.silentgames.core.ecs.component.ItemGeneratorComponent
import com.silentgames.core.ecs.component.addTask
import com.silentgames.core.ecs.component.inventory.InventoryItemComponent
import com.silentgames.core.ecs.component.inventory.inventoryComponent
import com.silentgames.core.ecs.component.itemGeneratorComponent
import com.silentgames.core.ecs.model.task.AddToInventoryTask
import com.silentgames.core.utils.allOf
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch
import ktx.async.RenderingScope
import ktx.log.logger
import javax.inject.Inject

private val LOG = logger<ItemGenerationSystem>()
private const val INTERVAL_SECONDS = 100f

class ItemGenerationSystem @Inject constructor() :
    IntervalIteratingSystem(allOf(ItemGeneratorComponent::class), INTERVAL_SECONDS),
    CoroutineScope by RenderingScope() {

    override fun process(entityId: Int) {
        val entity = getWorld().getEntity(entityId)
        launch {
            val itemGeneratorComponent = entity.itemGeneratorComponent
            if (itemGeneratorComponent.processProgress()) {
                itemGeneratorComponent.reset()
                if (!entity.inventoryComponent.isFull()) {
                    val generatedEntity = getWorld().createEntity()
                    itemGeneratorComponent.entitiesList.forEach { listComponent ->
                        val edit = generatedEntity.edit()
                        edit.add(InventoryItemComponent())
                        listComponent.forEach {
                            edit.add(it.clone())
                        }
                        entity.addTask(AddToInventoryTask(generatedEntity))
                    }
                }
                LOG.debug { "itemGenerationSystem - complete" }
            }
            LOG.debug { "progress - ${itemGeneratorComponent.progress}" }
        }
    }

    override fun dispose() {
        this.coroutineContext.cancelChildren()
    }
}
