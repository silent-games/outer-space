package com.silentgames.core.ecs.system.render.tile

import com.artemis.Entity
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTile
import com.silentgames.core.ecs.component.EnvironmentComponent
import com.silentgames.core.ecs.component.TextureComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.textureComponent
import com.silentgames.core.ecs.component.tile.TileComponent
import com.silentgames.core.ecs.component.tile.UpdatedComponent
import com.silentgames.core.ecs.system.MultiInputListener
import com.silentgames.core.model.GameState
import com.silentgames.core.settings.GameSettings
import com.silentgames.core.utils.allOf
import com.silentgames.core.utils.remove
import javax.inject.Inject

open class TileSystem @Inject constructor(
    tiledMap: TiledMap,
    gameState: GameState,
    gameSettings: GameSettings,
    multiInputListener: MultiInputListener
) : UpdatedTileIterationSystem(
    tiledMap,
    gameState,
    multiInputListener,
    gameSettings,
    allOf(
        TransformComponent::class,
        TextureComponent::class,
        EnvironmentComponent::class,
        TileComponent::class
    )
) {

    override fun createTiledMapTile(entity: Entity, deltaTime: Float): TiledMapTile {
        return entity.textureComponent.getTiledMapTile()
    }

    private fun TextureComponent.getTiledMapTile(): TiledMapTile {
        return gameState.gameTextureAtlas.getTiledMapTile(this)
            ?: throw KotlinNullPointerException("Not found texture with name: " + this.textureName)
    }

    override fun removed(entityId: Int) {
        world.getEntity(entityId).apply {
            remove(transformComponent.gamePosition)
            remove<UpdatedComponent>()
        }
    }
}
