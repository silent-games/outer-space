package com.silentgames.core.ecs.component

import com.artemis.Component
import com.silentgames.support.EcsComponent

@EcsComponent
class EnvironmentComponent : CloneableComponent() {
    override fun clone(): Component = EnvironmentComponent()
}
