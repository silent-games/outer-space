package com.silentgames.core.ecs.component.move

import com.artemis.Component
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.silentgames.core.ecs.component.CloneableComponent
import com.silentgames.core.ecs.entity.LayerType
import com.silentgames.core.ecs.model.GamePosition
import com.silentgames.support.EcsComponent

@EcsComponent
class TransformComponent(
    val gamePosition: GamePosition = GamePosition(Vector3(), 0),
    val speed: Float = 4f,
    val scale: Vector2 = Vector2(1.0f, 1.0f),
    var rotation: Float = 0.0f,
    var direction: Direction = Direction.DOWN
) : CloneableComponent() {

    constructor(
        x: Int,
        y: Int,
        levelNumber: Int,
        layerType: LayerType,
        speed: Float = 4f,
        scale: Vector2 = Vector2(1.0f, 1.0f),
        rotation: Float = 0.0f,
        direction: Direction = Direction.DOWN
    ) : this(
        GamePosition(Vector3(x.toFloat(), y.toFloat(), levelNumber.toFloat()), layerType.order),
        speed,
        scale,
        rotation,
        direction
    )

    enum class Direction {
        RIGHT, LEFT, TOP, DOWN
    }

    override fun clone(): Component =
        TransformComponent(gamePosition.copy(), speed, scale.cpy(), rotation, direction)
}
