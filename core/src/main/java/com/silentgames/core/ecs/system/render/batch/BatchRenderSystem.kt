package com.silentgames.core.ecs.system.render.batch

interface BatchRenderSystem {

    var onUpdateBegin: (() -> Unit)?

    var onUpdateEnd: (() -> Unit)?
}
