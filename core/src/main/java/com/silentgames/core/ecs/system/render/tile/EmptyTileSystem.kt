package com.silentgames.core.ecs.system.render.tile

import com.artemis.Entity
import com.silentgames.core.ecs.component.EnvironmentComponent
import com.silentgames.core.ecs.component.TextureComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.tile.TileComponent
import com.silentgames.core.ecs.component.tile.UpdatedComponent
import com.silentgames.core.ecs.component.tile.updatedComponentOrNull
import com.silentgames.core.ecs.system.base.AppIteratingSystem
import com.silentgames.core.utils.*
import javax.inject.Inject

class EmptyTileSystem @Inject constructor() : AppIteratingSystem(
    allOf(
        TransformComponent::class,
        EnvironmentComponent::class,
        TileComponent::class
    ).excludeOf(TextureComponent::class)
) {

    override fun firstProcessed() {
        entities.forEach {
            it.add(UpdatedComponent())
        }
    }

    override fun process(entity: Entity) {
        val updated = entity.updatedComponentOrNull
        if (updated != null && !updated.processed) {
            updated.processed = true
        } else if (updated != null && updated.processed) {
            entity.remove(updated)
        }
    }
}
