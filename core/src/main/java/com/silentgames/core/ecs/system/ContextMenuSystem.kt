package com.silentgames.core.ecs.system

import com.artemis.BaseSystem
import com.artemis.Entity
import com.artemis.World
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.silentgames.core.ecs.component.ActionComponent
import com.silentgames.core.ecs.component.actionComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.newTaskChain
import com.silentgames.core.ecs.component.select.SelectComponent
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.action.Action
import com.silentgames.core.ecs.model.action.MoveAction
import com.silentgames.core.ecs.model.task.MoveToNodeTask
import com.silentgames.core.model.ContentItem
import com.silentgames.core.model.GameState
import com.silentgames.core.screen.base.Context
import com.silentgames.core.screen.game.hud.ContentItemAction
import com.silentgames.core.utils.*
import javax.inject.Inject

class ContextMenuSystem @Inject constructor(
    private val context: Context,
    private val gameState: GameState,
    private val multiInputListener: MultiInputListener,
    private val contentItemAction: ContentItemAction
) : BaseSystem(), DefaultGameInputListener {

    override fun initialize() {
        contentItemAction.onAttachContext(context)
        multiInputListener.addListener(this)
    }

    override fun dispose() {
        multiInputListener.removeListener(this)
    }

    override fun processSystem() {}

    override fun select(worldX: Float, worldY: Float, screenX: Float, screenY: Float): Boolean {
        contentItemAction.changeVisibility(false)
        return false
    }

    override fun action(worldX: Float, worldY: Float, screenX: Float, screenY: Float): Boolean {
        val nowSelected = world.getEntitiesFor(oneOf(SelectComponent::class)).firstOrNull()
        return if (nowSelected != null) {
            nowSelected.processAction(worldX.toInt(), worldY.toInt(), screenX, screenY)
        } else {
            contentItemAction.changeVisibility(false)
            false
        }
    }

    private fun Entity.processAction(
        worldX: Int,
        worldY: Int,
        screenX: Float,
        screenY: Float
    ): Boolean {
        val entitiesOnPosition = world.getEntitiesOnPosition(worldX, worldY, gameState.currentLevel)
        val node = Node(worldX, worldY, gameState.currentLevel)
        val screenPos = Vector2(screenX, screenY)

        val actionList = entitiesOnPosition.mapNotNull { entity ->
            if (entity.has<ActionComponent>()) {
                entity.actionComponent.actionTypeList.mapNotNull { it.toContentItem(this, entity) }
            } else {
                null
            }
        }.flatten()

        when {
            actionList.size > 1 -> {
                contentItemAction.create(node, screenPos, this, actionList)
            }
            actionList.has { it.action is MoveAction } -> {
                contentItemAction.changeVisibility(false)
                this.newTaskChain(MoveToNodeTask(node))
            }
            actionList.size == 1 -> {
                contentItemAction.create(node, screenPos, this, actionList)
            }
            else -> {
                contentItemAction.changeVisibility(false)
                return false
            }
        }
        return true
    }

    private fun ContentItemAction.create(
        node: Node,
        screenPos: Vector2,
        selected: Entity,
        actionList: List<ContentItem>
    ) {
        this.changeVisibility(true)
        this.create(screenPos.toScreenPosition(), actionList) { contentItem ->
            contentItem.action.executeAction(selected, contentItem.entity, node)
            this.changeVisibility(false)
        }
    }

    private fun Action.toContentItem(selected: Entity, target: Entity) =
        if (this.isAllowAction(selected, target)) {
            ContentItem(this, target)
        } else {
            null
        }

    private fun Vector2.toScreenPosition(): Vector2 {
        return context.stage.camera.unproject(Vector3(this, 0f)).toVector2()
    }

    private fun World.getEntitiesOnPosition(x: Int, y: Int, z: Int) =
        this.getEntitiesFor(allOf(TransformComponent::class))
            .filter {
                it.transformComponent.gamePosition.vector.x.toInt() == x &&
                    it.transformComponent.gamePosition.vector.y.toInt() == y &&
                    it.transformComponent.gamePosition.levelNumber == z
            }
}
