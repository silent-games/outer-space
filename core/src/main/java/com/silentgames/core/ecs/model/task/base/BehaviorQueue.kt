package com.silentgames.core.ecs.model.task.base

import com.badlogic.gdx.ai.btree.BehaviorTree
import com.badlogic.gdx.ai.btree.Task
import java.util.*

class BehaviorQueue<T>(taskList: List<Task<T>> = listOf()) : BehaviorTree<T>() {

    constructor(task: Task<T>) : this(listOf(task))

    constructor(vararg tasks: Task<T>) : this(tasks.toList())

    private val taskList: LinkedList<Task<T>> = LinkedList(taskList)
    private var runningTask: Task<T>? = null

    override fun addChildToTask(child: Task<T>): Int {
        return addTask(child)
    }

    fun addTask(vararg tasks: Task<T>): Int {
        tasks.forEach {
            it.setControl(this)
        }
        taskList.addAll(tasks)
        return taskList.lastIndex
    }

    fun newTaskChain(vararg tasks: Task<T>): Int {
        runningTask?.cancel()
        taskList.forEach {
            it.cancel()
        }
        runningTask = null
        taskList.clear()
        return addTask(*tasks)
    }

    override fun getChildCount(): Int = taskList.size

    override fun getChild(i: Int): Task<T> {
        return taskList[i]
    }

    override fun step() {
        val runningTask = runningTask
        if (runningTask != null) {
            when (runningTask.status) {
                Status.FRESH -> {
                    runningTask.start()
                    if (runningTask.checkGuard(this)) runningTask.run() else runningTask.fail()
                }
                Status.RUNNING -> runningTask.run()
                else -> {
                    this.runningTask = null
                }
            }
        } else if (taskList.isNotEmpty()) {
            this.runningTask = taskList.poll()
        }
    }

    override fun reset() {
        taskList.clear()
        runningTask = null
        super.reset()
    }

    override fun copyTo(task: Task<T>?): Task<T> {
        val branch: BehaviorQueue<T> = task as BehaviorQueue<T>
        taskList.forEach {
            branch.taskList.add(it.cloneTask())
        }
        branch.runningTask = runningTask?.cloneTask()
        return super.copyTo(task)
    }
}
