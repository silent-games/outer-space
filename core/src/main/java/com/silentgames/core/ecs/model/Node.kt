package com.silentgames.core.ecs.model

data class Node(
    val x: Int = 0,
    val y: Int = 0,
    val z: Int = 0
)
