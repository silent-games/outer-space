package com.silentgames.core.ecs.component.inventory

import com.artemis.Component
import com.artemis.World
import com.silentgames.core.ecs.component.CloneableComponent
import com.silentgames.support.EcsComponent

@EcsComponent
data class InventoryItemComponent(@JvmField var ownerId: Int? = null) : CloneableComponent() {
    override fun clone(): Component = this.copy()

    fun getOwnerEntity(world: World) = ownerId?.let { world.getEntity(it) }
}
