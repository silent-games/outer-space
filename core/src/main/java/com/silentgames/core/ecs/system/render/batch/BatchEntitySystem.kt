package com.silentgames.core.ecs.system.render.batch

import com.artemis.BaseSystem

abstract class BatchEntitySystem : BaseSystem(), BatchRenderSystem {

    override var onUpdateBegin: (() -> Unit)? = null
    override var onUpdateEnd: (() -> Unit)? = null

    override fun begin() {
        onUpdateBegin?.invoke()
    }

    override fun end() {
        onUpdateEnd?.invoke()
    }

    override fun processSystem() {
        draw(world.delta)
    }

    abstract fun draw(deltaTime: Float)
}
