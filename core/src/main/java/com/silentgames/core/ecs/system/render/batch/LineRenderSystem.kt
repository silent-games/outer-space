package com.silentgames.core.ecs.system.render.batch

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.silentgames.core.ecs.component.move.PathComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.move.pathComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.select.SelectComponent
import com.silentgames.core.model.GameState
import com.silentgames.core.utils.GameTexture
import com.silentgames.core.utils.allOf
import com.silentgames.core.utils.getGameTextureRegion
import com.silentgames.core.utils.toVector2
import ktx.collections.toGdxArray
import ktx.math.toMutable
import space.earlygrey.shapedrawer.ShapeDrawer
import javax.inject.Inject

class LineRenderSystem @Inject constructor(
    spriteBatch: SpriteBatch,
    private val gameState: GameState
) : BatchIterationSystem(
    allOf(PathComponent::class, TransformComponent::class, SelectComponent::class)
) {

    companion object {
        private const val LINE_WIDTH = 0.1f
    }

    private val drawer: ShapeDrawer by lazy {
        ShapeDrawer(
            spriteBatch,
            gameState.gameTextureAtlas.getGameTextureRegion(GameTexture.ROTE)
        )
    }

    private val centerVector = Vector2(0.5f, 0.5f)

    override fun process(entityId: Int) {
        val entity = world.getEntity(entityId)
        val pathComponent = entity.pathComponent

        val route = pathComponent.path
        val currentNodeId = pathComponent.currentNodeId
        val gamePosition = entity.transformComponent.gamePosition
        val position = gamePosition.vector.toMutable()

        if (route.isEmpty()) return

        val remainingPath = route.toList()
            .subList(currentNodeId, route.size)
            .filter { gameState.currentLevel == it.z }
            .map { it.toVector2().add(centerVector) }
            .toGdxArray()
            .apply {
                if (this.size > 0 && gameState.currentLevel == gamePosition.levelNumber) {
                    set(0, position.add(centerVector))
                }
            }

        drawer.path(remainingPath, LINE_WIDTH, true)
    }
}
