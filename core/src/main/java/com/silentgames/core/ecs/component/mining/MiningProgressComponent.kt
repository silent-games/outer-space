package com.silentgames.core.ecs.component.mining

import com.artemis.Component
import com.silentgames.support.EcsComponent

@EcsComponent
class MiningProgressComponent(val targetId: Int = -1) : Component()
