package com.silentgames.core.ecs.model.task.base

import com.badlogic.gdx.ai.btree.Task
import com.badlogic.gdx.ai.btree.annotation.TaskAttribute

abstract class TargetIdTask(id: Int? = null) : TaskBoardTask() {

    @TaskAttribute(required = true)
    @JvmField
    var id: Int = id ?: -1

    val targetEntity get() = world.getEntity(this.id)

    override fun copyTo(task: Task<TaskBoard>): Task<TaskBoard> {
        val copyTask = task as TargetIdTask
        copyTask.id = id
        return task
    }
}
