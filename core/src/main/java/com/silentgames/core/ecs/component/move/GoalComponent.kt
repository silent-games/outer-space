package com.silentgames.core.ecs.component.move

import com.artemis.Component
import com.silentgames.core.ecs.component.CloneableComponent
import com.silentgames.core.ecs.model.Node
import com.silentgames.support.EcsComponent

@EcsComponent
class GoalComponent(val target: Node = Node(0, 0, 0)) : CloneableComponent() {
    override fun clone(): Component = GoalComponent(target.copy())
}
