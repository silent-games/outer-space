package com.silentgames.core.ecs.component.tile

import com.artemis.Component
import com.silentgames.core.ecs.system.render.tile.TileIterationSystem
import com.silentgames.support.EcsComponent

@EcsComponent
class UpdatedComponent(override var processed: Boolean = false) :
    Component(),
    TileIterationSystem.Updated
