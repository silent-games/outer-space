package com.silentgames.core.ecs.system.render.tile

import com.artemis.Entity
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTile
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.tile.CrackComponent
import com.silentgames.core.ecs.component.tile.UpdatedCrackComponent
import com.silentgames.core.ecs.component.tile.crackComponent
import com.silentgames.core.ecs.model.GamePosition
import com.silentgames.core.ecs.system.MultiInputListener
import com.silentgames.core.model.GameState
import com.silentgames.core.settings.GameSettings
import com.silentgames.core.utils.*
import ktx.collections.lastIndex
import javax.inject.Inject

class CracksSystem @Inject constructor(
    tiledMap: TiledMap,
    gameState: GameState,
    gameSettings: GameSettings,
    multiInputListener: MultiInputListener
) : TileIterationSystem<UpdatedCrackComponent>(
    tiledMap,
    gameState,
    multiInputListener,
    gameSettings,
    allOf(
        TransformComponent::class,
        CrackComponent::class
    )
) {

    override fun processTile(entity: Entity) {
        val gamePosition = entity.transformComponent.gamePosition.copy().apply {
            positionOnLevel++
        }
        if (entity.crackComponent.state != CrackComponent.State.REMOVED) {
            add(entity, gamePosition)
        } else {
            remove(gamePosition)
        }
    }

    override fun createUpdateComponent(): UpdatedCrackComponent = UpdatedCrackComponent()

    override fun getUpdateComponent(entity: Entity): UpdatedCrackComponent? = entity.getComponent()

    private fun add(entity: Entity, gamePosition: GamePosition) {
        val cell = TiledMapTileLayer.Cell().apply {
            tile = createTiledMapTile(entity, world.delta)
        }
        setupCellOnLayer(cell, gamePosition)
    }

    override fun createTiledMapTile(entity: Entity, deltaTime: Float): TiledMapTile {
        return getTiledMapTile(entity.crackComponent)
    }

    private fun getTiledMapTile(crackComponent: CrackComponent): TiledMapTile {
        val regions = gameState.gameTextureAtlas.getGameTextureRegions(GameTexture.CRACKS)
        if (crackComponent.frameId < 0 || crackComponent.frameId > regions.lastIndex) {
            crackComponent.setIdle()
        }
        crackComponent.nextFrameIfNeeded(regions.size)
        val region = regions.get(crackComponent.getRealFrameId(regions.size))
        val scale = crackComponent.scale
        val resultRegion = if (scale != 1f) {
            TextureRegion(Texture(region.extractPixmap(scale)))
        } else {
            region
        }
        return StaticTiledMapTile(resultRegion)
    }

    override fun removed(entityId: Int) {
        remove(
            world.getEntity(entityId).transformComponent.gamePosition.copy().apply {
                positionOnLevel++
            }
        )
    }
}
