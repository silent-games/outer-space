package com.silentgames.core.ecs.model.event

import com.artemis.Entity

class UnitMoveEvent(val entity: Entity)
