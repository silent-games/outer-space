package com.silentgames.core.ecs.model.action

import com.artemis.Entity
import com.silentgames.core.ecs.component.move.CanMoveComponent
import com.silentgames.core.ecs.component.move.LevelTransitionComponent
import com.silentgames.core.ecs.component.move.levelTransitionComponentOrNull
import com.silentgames.core.ecs.component.move.transformComponentOrNull
import com.silentgames.core.ecs.component.newTaskChain
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.task.MoveToNodeTask
import com.silentgames.core.utils.has

class UseStairsAction(private val direction: Direction = Direction.UP) : Action {

    companion object {
        private fun getStairsDirection(level: Int, targetLevel: Int): Direction =
            if (level > targetLevel) {
                Direction.DOWN
            } else {
                Direction.UP
            }
    }

    constructor(level: Int, targetLevel: Int) : this(getStairsDirection(level, targetLevel))

    override val description: String = direction.description

    enum class Direction(val description: String) {
        UP("Подняться вверх"),
        DOWN("Спуститься вниз"),
    }

    override fun isAllowAction(selected: Entity, target: Entity): Boolean {
        return selected.has<CanMoveComponent>() && target.has<LevelTransitionComponent>()
    }

    override fun executeAction(selected: Entity, target: Entity, node: Node) {
        getTargetNode(selected, target)?.let {
            selected.newTaskChain(MoveToNodeTask(it))
        }
    }

    private fun getTargetNode(selected: Entity, target: Entity): Node? {
        val currentLevel = selected.transformComponentOrNull?.gamePosition?.levelNumber
            ?: return null
        val sortedLevelTransitionList =
            target.levelTransitionComponentOrNull?.targetList?.sortedBy { it.z }
        return when (direction) {
            Direction.UP -> sortedLevelTransitionList?.find { it.z > currentLevel }
            Direction.DOWN -> sortedLevelTransitionList?.find { it.z < currentLevel }
        }
    }
}
