package com.silentgames.core.ecs.system.render.tile

import com.artemis.Entity
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTile
import com.badlogic.gdx.utils.Array
import com.silentgames.core.ecs.component.EnvironmentComponent
import com.silentgames.core.ecs.component.TextureComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.textureComponent
import com.silentgames.core.ecs.component.tile.WallComponent
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.system.MultiInputListener
import com.silentgames.core.ecs.system.render.tile.WallTileSystem.AdjacentWallPosition.*
import com.silentgames.core.model.GameState
import com.silentgames.core.settings.GameSettings
import com.silentgames.core.utils.allOf
import com.silentgames.core.utils.entities
import com.silentgames.core.utils.getAroundOnLevelPositionList
import ktx.collections.gdxArrayOf
import ktx.collections.map
import ktx.math.ImmutableVector2
import javax.inject.Inject
import kotlin.collections.set

class WallTileSystem @Inject constructor(
    tiledMap: TiledMap,
    gameState: GameState,
    gameSettings: GameSettings,
    multiInputListener: MultiInputListener
) : UpdatedTileIterationSystem(
    tiledMap,
    gameState,
    multiInputListener,
    gameSettings,
    allOf(
        WallComponent::class,
        TransformComponent::class,
        TextureComponent::class,
        EnvironmentComponent::class
    )
) {

    private val mapRegion = HashMap<String, TextureRegion>()
    private val mapRegions = HashMap<String, Array<TextureRegion>>()

    override fun createTiledMapTile(entity: Entity, deltaTime: Float): TiledMapTile {
        val entityVector = entity.transformComponent.gamePosition.vector
        val node = entityVector.toNode()

        val list = node.getAroundOnLevelPositionList(width, height)
        val filteredList = entities
            .filter { it.transformComponent.gamePosition.levelNumber == gameState.currentLevel }
            .filter { list.contains(it.transformComponent.gamePosition.vector.toNode()) }
            .map { it.transformComponent.gamePosition.vector.toNode() }
        return entity.textureComponent.getTiledMapTile(node, filteredList)
    }

    private fun ImmutableVector2.toNode() = Node(this.x.toInt(), this.y.toInt(), 0)

    private fun List<Node>.getWallPostfix(currentNode: Node): Postfix {
        val list = this.toAdjacentWallPositionList(currentNode)
        return when {
            list.containsAll(*values()) -> Postfix.CROSS
            list.containsAll(TOP, BOTTOM, RIGHT) -> Postfix.CORNER_BRANCH_RIGHT
            list.containsAll(TOP, BOTTOM, LEFT) -> Postfix.CORNER_BRANCH_LEFT
            list.containsAll(LEFT, RIGHT, TOP) -> Postfix.CORNER_BRANCH_TOP
            list.containsAll(LEFT, RIGHT, BOTTOM) -> Postfix.CORNER_BRANCH_BOTTOM
            list.containsAll(TOP, BOTTOM) || list.containsAll(BOTTOM) -> Postfix.VERTICAL
            list.containsAll(TOP) -> Postfix.VERTICAL_END
            list.containsAll(TOP, LEFT) -> Postfix.CORNER_TOP_LEFT
            list.containsAll(TOP, RIGHT) -> Postfix.CORNER_TOP_RIGHT
            list.containsAll(BOTTOM, LEFT) -> Postfix.CORNER_BOTTOM_LEFT
            list.containsAll(BOTTOM, RIGHT) -> Postfix.CORNER_BOTTOM_RIGHT
            list.containsAll(LEFT) || list.containsAll(RIGHT) || list.containsAll(
                LEFT,
                RIGHT
            ) -> Postfix.HORIZONTAL
            else -> Postfix.CROSS
        }
    }

    private enum class Postfix(val text: String) {
        VERTICAL("_vertical"),
        VERTICAL_END("_vertical_end"),
        HORIZONTAL("_horizontal"),
        CROSS("_cross"),
        CORNER_TOP_LEFT("_corner_top"),
        CORNER_TOP_RIGHT("_corner_top"),
        CORNER_BOTTOM_LEFT("_corner_bottom"),
        CORNER_BOTTOM_RIGHT("_corner_bottom"),
        CORNER_BRANCH_TOP("_branch_top"),
        CORNER_BRANCH_BOTTOM("_branch_bottom"),
        CORNER_BRANCH_LEFT("_branch_left"),
        CORNER_BRANCH_RIGHT("_branch_left"),
    }

    private fun List<AdjacentWallPosition>.containsAll(vararg pos: AdjacentWallPosition): Boolean {
        return if (pos.size != this.size) false else this.containsAll(pos.toList())
    }

    private fun List<Node>.toAdjacentWallPositionList(currentNode: Node): List<AdjacentWallPosition> {
        return this.mapNotNull { it.toAdjacentWallPosition(currentNode) }
    }

    private fun Node.toAdjacentWallPosition(currentNode: Node): AdjacentWallPosition? {
        val x = currentNode.x
        val y = currentNode.y
        return when {
            compare(x, y + 1) -> TOP
            compare(x, y - 1) -> BOTTOM
            compare(x - 1, y) -> LEFT
            compare(x + 1, y) -> RIGHT
            else -> null
        }
    }

    private fun Node.compare(x: Int, y: Int): Boolean {
        return this.x == x && this.y == y
    }

    enum class AdjacentWallPosition {
        TOP,
        BOTTOM,
        LEFT,
        RIGHT
    }

    private fun TextureComponent.getTiledMapTile(
        currentNode: Node,
        filteredNodes: List<Node>
    ): TiledMapTile {
        val postfix = filteredNodes.getWallPostfix(currentNode)
        val textureName = this.textureName + postfix.text
        val regions = postfix.getTextureRegions(textureName) ?: gdxArrayOf()
        val region = postfix.getTextureRegion(textureName)

        return getTiledMapTile(regions, region, this.interval)
            ?: throw KotlinNullPointerException("Not found texture with name: $textureName")
    }

    private fun Postfix.getTextureRegions(textureName: String): Array<out TextureRegion>? {
        return if (this == Postfix.CORNER_TOP_RIGHT ||
            this == Postfix.CORNER_BOTTOM_RIGHT ||
            this == Postfix.CORNER_BRANCH_RIGHT
        ) {
            mapRegions[textureName] ?: mapRegions.putRegions(textureName)
        } else {
            gameState.gameTextureAtlas.findRegions(textureName)
        }
    }

    private fun Postfix.getTextureRegion(textureName: String): TextureRegion? {
        return if (this == Postfix.CORNER_TOP_RIGHT ||
            this == Postfix.CORNER_BOTTOM_RIGHT ||
            this == Postfix.CORNER_BRANCH_RIGHT
        ) {
            mapRegion[textureName] ?: mapRegion.putRegion(textureName)
        } else {
            gameState.gameTextureAtlas.findRegion(textureName)
        }
    }

    private fun HashMap<String, Array<TextureRegion>>.putRegions(textureName: String): Array<TextureRegion>? {
        val texture = gameState.gameTextureAtlas.findRegions(textureName) ?: return null
        val region = texture.map { TextureRegion(it).apply { flip(true, false) } }
        this[textureName] = region
        return region
    }

    private fun HashMap<String, TextureRegion>.putRegion(textureName: String): TextureRegion? {
        val texture = gameState.gameTextureAtlas.findRegion(textureName) ?: return null
        val region = TextureRegion(texture).apply { flip(true, false) }
        this[textureName] = region
        return region
    }
}
