package com.silentgames.core.ecs.model.task

import com.artemis.Entity
import com.silentgames.core.ecs.component.inventory.InventoryItemComponent
import com.silentgames.core.ecs.component.inventory.inventoryComponent
import com.silentgames.core.ecs.component.inventory.inventoryItemComponent
import com.silentgames.core.ecs.model.task.base.TargetIdTask
import com.silentgames.core.utils.has

class RemoveFromInventoryTask : TargetIdTask {

    constructor(item: Entity) : super(item.id)

    constructor(id: Int) : super(id)

    constructor()

    override fun execute(): Status = execute(targetEntity)

    companion object {
        fun execute(item: Entity): Status {
            return if (item.has<InventoryItemComponent>()) {
                val removed = item.inventoryItemComponent.getOwnerEntity(item.world)
                    ?.inventoryComponent?.items?.remove(item.id) ?: false
                item.inventoryItemComponent.ownerId = null
                if (removed) {
                    Status.SUCCEEDED
                } else {
                    Status.FAILED
                }
            } else {
                Status.FAILED
            }
        }
    }
}
