package com.silentgames.core.ecs.entity

enum class LayerType(val order: Int) {
    BACKGROUND(0),
    ENVIRONMENT(1),
    ITEM(2),
    UNIT(3)
}
