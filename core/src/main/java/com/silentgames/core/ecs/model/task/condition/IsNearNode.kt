package com.silentgames.core.ecs.model.task.condition

import com.artemis.Entity
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.task.base.NodeTask
import com.silentgames.core.utils.getAroundOnLevelPositionList
import com.silentgames.core.utils.toNode

class IsNearNode : NodeTask {

    constructor(target: Entity) : super(target.transformComponent.gamePosition.toNode())

    constructor(node: Node) : super(node)

    constructor()

    override fun execute(): Status =
        if (entity.isNear(node)) {
            Status.SUCCEEDED
        } else {
            Status.FAILED
        }

    private fun Entity.isNear(node: Node) = this.transformComponent.gamePosition.toNode()
        .getAroundOnLevelPositionList().contains(node)
}
