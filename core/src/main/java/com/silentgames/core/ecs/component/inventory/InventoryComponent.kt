package com.silentgames.core.ecs.component.inventory

import com.artemis.Component
import com.artemis.World
import com.silentgames.support.EcsComponent

@EcsComponent
class InventoryComponent(val items: MutableList<Int> = mutableListOf(), val maxItems: Int = 10) :
    Component() {
    fun isFull() = items.size == maxItems
    fun isEmpty() = items.isEmpty()

    fun getEntityItems(world: World) = items.map { world.getEntity(it) }
}
