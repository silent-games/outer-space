package com.silentgames.core.ecs.model.action

import com.artemis.Entity
import com.badlogic.gdx.ai.btree.branch.Sequence
import com.silentgames.core.ecs.component.HungerComponent
import com.silentgames.core.ecs.component.SaturationComponent
import com.silentgames.core.ecs.component.food.FoodComponent
import com.silentgames.core.ecs.component.inventory.InventoryComponent
import com.silentgames.core.ecs.component.inventory.inventoryComponent
import com.silentgames.core.ecs.component.newTaskChain
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.task.EatTask
import com.silentgames.core.utils.has

object EatAction : Action {
    override val description: String = "Съесть"

    override fun isAllowAction(selected: Entity, target: Entity): Boolean {
        return selected.has<HungerComponent>() &&
            !selected.has<SaturationComponent>() &&
            target.has<InventoryComponent>() &&
            target.inventoryComponent.getEntityItems(selected.world)
            .find { it.has<FoodComponent>() } != null
    }

    override fun executeAction(selected: Entity, target: Entity, node: Node) {
        selected.newTaskChain(
            Sequence(EatTask(target))
        )
    }
}
