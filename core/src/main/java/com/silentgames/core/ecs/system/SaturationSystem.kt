package com.silentgames.core.ecs.system

import com.artemis.systems.IntervalIteratingSystem
import com.silentgames.core.ecs.component.HungerComponent
import com.silentgames.core.ecs.component.SaturationComponent
import com.silentgames.core.ecs.component.hungerComponent
import com.silentgames.core.ecs.component.saturationComponent
import com.silentgames.core.utils.allOf
import com.silentgames.core.utils.remove
import ktx.log.logger
import javax.inject.Inject

private val LOG = logger<SaturationSystem>()
private const val INTERVAL_SECONDS = 1f

class SaturationSystem @Inject constructor() : IntervalIteratingSystem(
    allOf(SaturationComponent::class, HungerComponent::class),
    INTERVAL_SECONDS
) {
    override fun process(entityId: Int) {
        val entity = world.getEntity(entityId)
        val hungerComponent = entity.hungerComponent
        val saturationComponent = entity.saturationComponent

        if (saturationComponent.energy > 0 && hungerComponent.satiety < hungerComponent.maxSatiety) {
            saturationComponent.reduceEnergy()
            hungerComponent.increaseSatiety(saturationComponent.saturationSpeed)
            LOG.debug { "progress eating - ${hungerComponent.satiety}" }
        } else {
            entity.remove<SaturationComponent>()
            LOG.debug { "SaturationSystem complete" }
        }
    }
}
