package com.silentgames.core.ecs.model.map

import com.silentgames.core.ecs.model.Node

class Cell<T>(val position: Node, val data: T)
