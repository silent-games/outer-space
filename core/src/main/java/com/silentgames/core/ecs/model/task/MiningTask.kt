package com.silentgames.core.ecs.model.task

import com.silentgames.core.ecs.component.mining.MiningProgressComponent
import com.silentgames.core.ecs.component.mining.minerComponentOrNull
import com.silentgames.core.ecs.component.mining.miningProgressComponentOrNull
import com.silentgames.core.ecs.component.mining.miningResourceComponentOrNull
import com.silentgames.core.ecs.model.task.base.TargetIdTask
import com.silentgames.core.utils.add
import com.silentgames.core.utils.remove

class MiningTask : TargetIdTask {

    constructor(id: Int) : super(id)

    constructor()

    override fun execute(): Status {
        val minerComponent = entity.minerComponentOrNull
        val miningResourceComponent = targetEntity.miningResourceComponentOrNull
        return when {
            minerComponent == null || miningResourceComponent == null -> {
                Status.FAILED
            }
            status != Status.RUNNING -> {
                entity.add(MiningProgressComponent(id))
                Status.RUNNING
            }
            entity.miningProgressComponentOrNull == null -> {
                Status.SUCCEEDED
            }
            else -> {
                Status.RUNNING
            }
        }
    }

    override fun end() {
        entity.remove<MiningProgressComponent>()
        super.end()
    }
}
