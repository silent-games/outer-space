package com.silentgames.core.ecs.model.task.condition

import com.artemis.Entity
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.move.transformComponentOrNull
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.task.base.TargetIdTask
import com.silentgames.core.utils.getAroundOnLevelPositionList
import com.silentgames.core.utils.toNode

class IsNearTarget : TargetIdTask {

    constructor(target: Entity) : super(target.id)

    constructor(id: Int) : super(id)

    constructor()

    override fun execute(): Status {
        val node = targetEntity.transformComponentOrNull?.gamePosition?.toNode()
        return if (node != null && entity.isNear(node)) {
            Status.SUCCEEDED
        } else {
            Status.FAILED
        }
    }

    private fun Entity.isNear(node: Node) =
        this.transformComponent.gamePosition.toNode()
            .getAroundOnLevelPositionList().contains(node) ||
            this.transformComponent.gamePosition.toNode() == node
}
