package com.silentgames.core.ecs.model.task

import com.artemis.Entity
import com.silentgames.core.ecs.component.tile.DoorComponent
import com.silentgames.core.ecs.component.tile.UpdatedComponent
import com.silentgames.core.ecs.component.tile.doorComponentOrNull
import com.silentgames.core.ecs.model.task.base.TargetIdTask
import com.silentgames.core.utils.add

class OpenDoorTask : TargetIdTask {

    constructor(target: Entity) : super(target.id)

    constructor(id: Int) : super(id)

    constructor()

    override fun execute(): Status {
        val doorComponent = targetEntity.doorComponentOrNull
        return if (doorComponent != null) {
            when (doorComponent.state) {
                DoorComponent.State.CLOSE, DoorComponent.State.CLOSING -> {
                    targetEntity.add(UpdatedComponent())
                    doorComponent.setOpening()
                    return Status.RUNNING
                }
                DoorComponent.State.OPEN -> {
                    Status.SUCCEEDED
                }
                DoorComponent.State.OPENING -> {
                    Status.RUNNING
                }
            }
        } else {
            Status.FAILED
        }
    }
}

class CloseDoorTask : TargetIdTask {

    constructor(target: Entity) : super(target.id)

    constructor(id: Int) : super(id)

    constructor()

    override fun execute(): Status {
        val doorComponent = targetEntity.doorComponentOrNull
        return if (doorComponent != null) {
            when (doorComponent.state) {
                DoorComponent.State.OPEN, DoorComponent.State.OPENING -> {
                    targetEntity.add(UpdatedComponent())
                    doorComponent.setClosing()
                    return Status.RUNNING
                }
                DoorComponent.State.CLOSE -> {
                    Status.SUCCEEDED
                }
                DoorComponent.State.CLOSING -> {
                    Status.RUNNING
                }
            }
        } else {
            Status.FAILED
        }
    }
}
