package com.silentgames.core.ecs.component.mining

import com.artemis.Component
import com.silentgames.core.ecs.component.CloneableComponent
import com.silentgames.support.EcsComponent

@EcsComponent
class MiningResourceComponent(
    val difficulty: Float = 0.1f,
    val addComponentList: List<CloneableComponent> = listOf()
) : Component()
