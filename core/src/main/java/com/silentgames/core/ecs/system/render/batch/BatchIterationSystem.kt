package com.silentgames.core.ecs.system.render.batch

import com.artemis.Aspect
import com.artemis.systems.IteratingSystem

abstract class BatchIterationSystem(
    aspect: Aspect.Builder
) : IteratingSystem(aspect), BatchRenderSystem {

    override var onUpdateBegin: (() -> Unit)? = null
    override var onUpdateEnd: (() -> Unit)? = null

    override fun begin() {
        onUpdateBegin?.invoke()
    }

    override fun end() {
        onUpdateEnd?.invoke()
    }
}
