package com.silentgames.core.ecs.model.action

import com.artemis.Entity
import com.silentgames.core.ecs.component.inventory.InventoryComponent
import com.silentgames.core.ecs.component.inventory.inventoryComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.component.newTaskChain
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.task.DropItemTask
import com.silentgames.core.utils.has

object DropAction : Action {

    override val description: String = "Выбросить"

    override fun isAllowAction(selected: Entity, target: Entity): Boolean {
        return target.has<InventoryComponent>() &&
            !target.inventoryComponent.isEmpty()
    }

    override fun executeAction(selected: Entity, target: Entity, node: Node) {
        selected.newTaskChain(
            DropItemTask(
                target.inventoryComponent.items.first(),
                selected.transformComponent.gamePosition
            )
        )
    }
}
