package com.silentgames.core.ecs.component.tile

import com.artemis.Component
import com.silentgames.support.EcsComponent

@EcsComponent
class DoorComponent(
    var state: State = State.CLOSE,
    val changeStateTime: Float = 1f,
    var changeProgressTime: Float = changeStateTime,
    var frameId: Int = 0
) : Component() {

    fun setOpening() {
        resetProgressTime()
        state = State.OPENING
    }

    fun setClosing() {
        resetProgressTime()
        state = State.CLOSING
    }

    fun setOpen() {
        resetProgressTime()
        state = State.OPEN
    }

    fun setClose() {
        resetProgressTime()
        state = State.CLOSE
    }

    fun setNextState() {
        if (state == State.OPENING) {
            setOpen()
        } else if (state == State.CLOSING) {
            setClose()
        }
    }

    fun resetProgressTime() {
        changeProgressTime = 0f
    }

    enum class State {
        OPEN,
        OPENING,
        CLOSING,
        CLOSE
    }
}
