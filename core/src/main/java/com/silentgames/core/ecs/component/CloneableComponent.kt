package com.silentgames.core.ecs.component

import com.artemis.Component

abstract class CloneableComponent() : Component() {

    abstract fun clone(): Component
}
