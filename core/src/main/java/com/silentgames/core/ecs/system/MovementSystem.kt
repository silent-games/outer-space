package com.silentgames.core.ecs.system

import com.artemis.Entity
import com.artemis.systems.EntityProcessingSystem
import com.silentgames.core.ecs.component.move.PathComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.move.pathComponent
import com.silentgames.core.ecs.component.move.transformComponent
import com.silentgames.core.ecs.model.event.GameEvents
import com.silentgames.core.ecs.model.event.UnitMoveEvent
import com.silentgames.core.utils.allOf
import com.silentgames.core.utils.remove
import com.silentgames.core.utils.toVector2
import com.silentgames.core.utils.toVector3
import javax.inject.Inject

class MovementSystem @Inject constructor(
    private val gameEvents: GameEvents
) : EntityProcessingSystem(
    allOf(
        PathComponent::class,
        TransformComponent::class
    )
) {

    companion object {
        private const val MIN_DISTANCE_TO_TARGET = 0.01
    }

    override fun process(entity: Entity) {
        val transformComponent = entity.transformComponent
        val pathComponent = entity.pathComponent

        val destination = pathComponent.path.lastOrNull()?.toVector3()
        val nearestTarget = pathComponent.nextNode
        val position = transformComponent.gamePosition.vector3
        val speed = transformComponent.speed * world.getDelta()

        if (destination == null || nearestTarget == null) return

        if (position == destination) {
            entity.remove<PathComponent>()
            transformComponent.direction = TransformComponent.Direction.DOWN
        } else {
            if (position == nearestTarget.toVector3()) {
                pathComponent.increaseCurrentNode()
            } else {
                val direction = nearestTarget.toVector3().cpy().sub(position)
                val distance = direction.cpy()
                direction.nor()
                val velocity = direction.cpy().scl(speed)
                val angle: Float = velocity.toVector2().angleDeg()
                if (distance.len2() > MIN_DISTANCE_TO_TARGET) {
                    position.add(velocity)
                    transformComponent.direction = getDirection(angle)
                } else {
                    position.set(nearestTarget.toVector3())
                }
            }
        }
        gameEvents.onUnitMove.emit(UnitMoveEvent(entity))
    }

    private fun getDirection(angle: Float): TransformComponent.Direction {
        return when (angle) {
            in 0f..79f, in 280f..360f -> TransformComponent.Direction.RIGHT
            in 80f..109f -> TransformComponent.Direction.TOP
            in 110f..259f -> TransformComponent.Direction.LEFT
            in 260f..279f -> TransformComponent.Direction.DOWN
            else -> TransformComponent.Direction.DOWN
        }
    }
}
