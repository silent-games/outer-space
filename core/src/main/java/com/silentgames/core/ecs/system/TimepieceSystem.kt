package com.silentgames.core.ecs.system

import com.artemis.BaseSystem
import com.badlogic.gdx.ai.GdxAI
import javax.inject.Inject

class TimepieceSystem @Inject constructor() : BaseSystem() {
    override fun processSystem() {
        GdxAI.getTimepiece().update(world.delta)
    }
}
