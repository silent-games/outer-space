package com.silentgames.core.ecs.model.task

import com.silentgames.core.ecs.component.move.GoalComponent
import com.silentgames.core.ecs.component.move.pathComponentOrNull
import com.silentgames.core.ecs.model.Node
import com.silentgames.core.ecs.model.task.base.NodeTask
import com.silentgames.core.utils.add

class MoveToNodeTask : NodeTask {

    constructor(node: Node) : super(node)

    constructor()

    override fun execute(): Status {
        // todo add Status.FAILED
        return when {
            status != Status.RUNNING -> {
                entity.add(GoalComponent(node))
                Status.RUNNING
            }
            entity.pathComponentOrNull == null -> {
                Status.SUCCEEDED
            }
            else -> {
                Status.RUNNING
            }
        }
    }
}
