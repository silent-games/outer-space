package com.silentgames.core.ecs.model.task

import com.artemis.Entity
import com.silentgames.core.ecs.component.cooking.ProgressCookingComponent
import com.silentgames.core.ecs.component.cooking.cookingComponentOrNull
import com.silentgames.core.ecs.component.food.FoodGeneratorComponent
import com.silentgames.core.ecs.component.food.foodGeneratorComponent
import com.silentgames.core.ecs.component.food.foodGeneratorComponentOrNull
import com.silentgames.core.ecs.model.task.base.TargetIdTask
import com.silentgames.core.utils.add
import com.silentgames.core.utils.has
import com.silentgames.core.utils.remove

class CookTask : TargetIdTask {

    constructor(target: Entity) : super(target.id)

    constructor()

    override fun execute(): Status {
        val foodGeneratorComponent = targetEntity.foodGeneratorComponentOrNull
        val cookingComponent = entity.cookingComponentOrNull

        return if (foodGeneratorComponent != null && cookingComponent != null) {
            when (foodGeneratorComponent.state) {
                FoodGeneratorComponent.State.INACTIVE -> {
                    entity.add(ProgressCookingComponent(targetEntity.id))
                    targetEntity.foodGeneratorComponent.setActive()
                    Status.RUNNING
                }
                FoodGeneratorComponent.State.ACTIVE -> {
                    if (entity.has<ProgressCookingComponent>()) {
                        Status.RUNNING
                    } else {
                        targetEntity.foodGeneratorComponent.setInactive()
                        Status.SUCCEEDED
                    }
                }
            }
        } else {
            Status.FAILED
        }
    }

    override fun end() {
        entity.remove<ProgressCookingComponent>()
        targetEntity.foodGeneratorComponent.setInactive()
        super.end()
    }
}
