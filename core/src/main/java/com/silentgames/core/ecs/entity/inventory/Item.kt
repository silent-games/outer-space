package com.silentgames.core.ecs.entity.inventory

import com.artemis.World
import com.silentgames.core.ecs.component.ActionComponent
import com.silentgames.core.ecs.component.DescriptionComponent
import com.silentgames.core.ecs.component.ResourceComponent
import com.silentgames.core.ecs.component.TextureComponent
import com.silentgames.core.ecs.component.inventory.InventoryItemComponent
import com.silentgames.core.ecs.component.move.AllowMovementComponent
import com.silentgames.core.ecs.component.move.TransformComponent
import com.silentgames.core.ecs.component.select.CanSelectedComponent
import com.silentgames.core.ecs.entity.LayerType
import com.silentgames.core.ecs.model.action.PickupAction
import com.silentgames.core.utils.GameTexture

fun World.addItem(x: Int, y: Int, level: Int) {
    val edit = createEntity().edit()
    getItemComponents(x, y, level).forEach {
        edit.add(it)
    }
}

fun getItemComponents(x: Int, y: Int, level: Int) =
    getBaseItemComponents(
        x,
        y,
        level,
        "Предмет",
        "Обычный Предмет",
        LayerType.ITEM,
        GameTexture.SHOOT_GUN
    )

fun getFoodComponents(x: Int, y: Int, level: Int) =
    getBaseItemComponents(
        x,
        y,
        level,
        "Еда",
        "Обычная еда",
        LayerType.ITEM,
        GameTexture.FOOD
    )

fun getMinedResourceComponents(
    x: Int,
    y: Int,
    level: Int,
    name: String,
    description: String,
    texture: GameTexture
) =
    getBaseItemComponents(
        x,
        y,
        level,
        name,
        description,
        LayerType.ITEM,
        texture
    ).apply {
        add(ResourceComponent())
    }

fun getBaseItemComponents(
    x: Int,
    y: Int,
    level: Int,
    name: String,
    description: String,
    layerType: LayerType,
    texture: GameTexture
) = mutableListOf(
    ActionComponent(PickupAction),
    DescriptionComponent(name, description),
    CanSelectedComponent(),
    InventoryItemComponent(),
    TextureComponent(texture.id),
    TransformComponent(x, y, level, layerType),
    AllowMovementComponent()
)
