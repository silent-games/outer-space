package com.silentgames.core

object RenderConstants {

    const val UNIT_SIZE_IN_TILES_X = 1f
    const val UNIT_SIZE_IN_TILES_Y = 2f

    /** game zoom settings */
    const val maxZoomValue = 15f
    const val minZoomValue = 0.5f
    const val zoomScaleFactor = 0.1f
    const val defaultZoom = 1.5f

    /** The units to translate the camera when moved */
    const val translateUnits = 20f

    /** game board size in pixels(screen solution) */
    const val VIRTUAL_W = 1280
    const val VIRTUAL_H = 720

    /** size in game coordinates (one tile is 1 value) */
    const val WIDTH = 25f
    const val HEIGHT = 15f

    /** size in screen coordinates (in pixels) */
    const val UNIT_SIZE = 64

    /** scale for game objects. Make one object as UNIT_SIZE pixels */
    const val UNIT_SCALE = 1.0f / UNIT_SIZE
}
