package com.silentgames.core.di

import com.artemis.managers.WorldSerializationManager
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.maps.tiled.TiledMap
import com.silentgames.core.RenderConstants
import com.silentgames.core.ecs.model.event.GameEvents
import com.silentgames.core.ecs.system.GameInputListener
import com.silentgames.core.ecs.system.MultiInputListener
import com.silentgames.core.model.GameState
import dagger.Module
import dagger.Provides
import ktx.assets.async.AssetStorage

@Module
class GameModule {

    @Provides
    @GameScope
    fun provideGameState(assetStorage: AssetStorage): GameState = GameState(assetStorage)

    @Provides
    @GameScope
    fun provideWorldSerializationManager(): WorldSerializationManager = WorldSerializationManager()

    @Provides
    @GameScope
    fun provideGameAction(): GameEvents = GameEvents()

    @Provides
    @GameScope
    fun provideTiledMap(): TiledMap = TiledMap()

    @Provides
    @GameScope
    fun provideMultiInputListener(): MultiInputListener = MultiInputListener()

    @Provides
    @GameScope
    fun provideGameInputListener(multiInputListener: MultiInputListener): GameInputListener =
        multiInputListener

    @Provides
    @GameScope
    fun provideCamera(orthographicCamera: OrthographicCamera): Camera = orthographicCamera

    @Provides
    @GameScope
    fun provideOrthographicCamera(): OrthographicCamera =
        OrthographicCamera(RenderConstants.WIDTH, RenderConstants.HEIGHT).apply {
            position.set(RenderConstants.WIDTH / 2, RenderConstants.HEIGHT / 2, 0f)
        }
}
