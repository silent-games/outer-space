package com.silentgames.core.di

import com.silentgames.core.screen.game.hud.ContentItemAction
import com.silentgames.core.screen.game.hud.GameHud
import com.silentgames.core.screen.game.hud.SelectedEntityHud
import dagger.Module
import dagger.Provides

@Module
class HudModule(
    private val contentItemAction: ContentItemAction,
    private val gameHud: GameHud,
    private val selectedEntityHud: SelectedEntityHud
) {

    @Provides
    @GameScope
    fun provideContentItemHud(): ContentItemAction = contentItemAction

    @Provides
    @GameScope
    fun provideGameHud(): GameHud = gameHud

    @Provides
    @GameScope
    fun provideSelectedEntityHud(): SelectedEntityHud = selectedEntityHud
}
