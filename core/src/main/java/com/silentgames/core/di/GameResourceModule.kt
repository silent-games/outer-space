package com.silentgames.core.di

import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.FitViewport
import com.silentgames.core.RenderConstants.VIRTUAL_H
import com.silentgames.core.RenderConstants.VIRTUAL_W
import com.silentgames.core.screen.base.Context
import com.silentgames.core.screen.base.OuterSpaceGame
import com.silentgames.core.ui.createUiSkin
import dagger.Module
import dagger.Provides
import ktx.assets.async.AssetStorage

@Module
class GameResourceModule(private val game: OuterSpaceGame) {

    @Provides
    @GameScope
    fun provideInputMultiplexer(stage: Stage): InputMultiplexer = InputMultiplexer(stage)

    @Provides
    @GameScope
    fun provideSpriteBatch(): SpriteBatch = SpriteBatch()

    @Provides
    @GameScope
    fun provideStage(spriteBatch: SpriteBatch): Stage =
        Stage(FitViewport(VIRTUAL_W.toFloat(), VIRTUAL_H.toFloat()), spriteBatch)

    @Provides
    @GameScope
    fun provideAssets(): AssetStorage = AssetStorage()

    @Provides
    @GameScope
    fun provideContext(
        assets: AssetStorage,
        spriteBatch: SpriteBatch,
        stage: Stage
    ): Context = Context(assets, assets.createUiSkin(), spriteBatch, stage, game)
}
