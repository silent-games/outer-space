package com.silentgames.core.di

import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.scenes.scene2d.Stage
import com.silentgames.core.screen.base.Context
import com.silentgames.core.screen.game.OuterSpaceGameScreen
import dagger.Component
import ktx.assets.async.AssetStorage
import javax.inject.Scope

@GameScope
@Component(
    modules = [
        GameResourceModule::class,
        GameSettingsComponent::class,
        GameModule::class,
        GameInputControllerModule::class,
        HudModule::class,
        DebugHudModule::class
    ]
)
interface GameDiComponent {

    @Component.Builder
    interface Builder {
        fun setGameResourcesModule(gameResourcesModule: GameResourceModule): Builder
        fun setGameInputControllerModule(gameInputControllerModule: GameInputControllerModule): Builder
        fun setHudModule(hudModule: HudModule): Builder
        fun setDebugHudModule(debugHudModule: DebugHudModule): Builder
        fun build(): GameDiComponent
    }

    fun getOuterSpaceGameScreen(): OuterSpaceGameScreen

    fun getAssetStorage(): AssetStorage

    fun getStage(): Stage

    fun getInputMultiplexer(): InputMultiplexer

    fun getContext(): Context
}

@Scope
@Retention
annotation class GameScope
