package com.silentgames.core.di

import com.silentgames.core.ecs.model.map.GameMap
import com.silentgames.core.ecs.model.map.Map
import com.silentgames.core.settings.GameBoardSize
import com.silentgames.core.settings.GameSettings
import dagger.Module
import dagger.Provides

@Module
class GameSettingsComponent {

    @Provides
    @GameScope
    fun provideGameMap(gameBoardSize: GameBoardSize): GameMap =
        GameMap(Map(gameBoardSize, arrayOfNulls(gameBoardSize.size)))

    @Provides
    @GameScope
    fun provideGameSettings(gameBoardSize: GameBoardSize): GameSettings =
        GameSettings(gameBoardSize)

    @Provides
    @GameScope
    fun provideGameBoardSize(): GameBoardSize = GameBoardSize(400, 400, 2)
}
