package com.silentgames.core.di

import com.silentgames.core.ecs.system.GameInputController
import dagger.Module
import dagger.Provides

@Module
class GameInputControllerModule(private val gameInputController: GameInputController) {

    @Provides
    @GameScope
    fun provideGameInputController(): GameInputController = gameInputController
}
