package com.silentgames.core.di

import com.silentgames.core.screen.game.hud.BehaviourTreeEditor
import dagger.Module
import dagger.Provides

@Module
class DebugHudModule(
    private val behaviourTreeEditor: BehaviourTreeEditor
) {

    @Provides
    @GameScope
    fun provideBehaviourTreeEditor(): BehaviourTreeEditor = behaviourTreeEditor
}
