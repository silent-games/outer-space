package com.silentgames.core.ui

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Container
import com.badlogic.gdx.scenes.scene2d.utils.Drawable

class AppContainer<T : Actor>(actor: T) : Container<T>(actor) {

    override fun setBackground(background: Drawable) {
        setBackground(background, false)
    }

    override fun getPrefWidth(): Float {
        return prefWidthValue.get()
    }

    override fun getPrefHeight(): Float {
        return prefHeightValue.get()
    }
}
