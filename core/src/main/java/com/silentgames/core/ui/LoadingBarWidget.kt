package com.silentgames.core.ui

import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup
import ktx.actors.plusAssign

class LoadingBarWidget(
    skin: Skin,
    private val bar: Image = Image(skin[Images.BAR_GREEN])
) : WidgetGroup(
    Image(skin[Images.BAR_BACKGROUND]),
    bar
) {
    init {
        with(bar) {
            setPosition(19f, 22f)
            setSize(530f, 43f)
            scaleX = 0f
        }
    }

    fun scaleTo(percentage: Float, scaleDuration: Float = 0.1f) {
        bar.run {
            clearActions()
            this += scaleTo(MathUtils.clamp(percentage, 0f, 1f), 1f, scaleDuration)
        }
    }

    override fun getPrefHeight(): Float = 85f
    override fun getPrefWidth(): Float = 600f
}
