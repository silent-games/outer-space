package com.silentgames.core.ui

import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.Drawable
import com.silentgames.core.utils.Font
import com.silentgames.core.utils.TextureAtlasAssets
import com.silentgames.core.utils.get
import ktx.assets.async.AssetStorage
import ktx.scene2d.Scene2DSkin
import ktx.style.label
import ktx.style.skin

enum class Images(val imageName: String) {
    BAR_BACKGROUND("bar"),
    BAR_GREEN("bar_green"),
    UI_BLOCK("ui_block"),
    SPACE_BACKGROUND("bg_space")
}

enum class TextColor(val colorName: String) {
    WHITE("white"),
    RED("red")
}

operator fun Skin.get(image: Images): Drawable = this.getDrawable(image.imageName)
fun Skin.getImageRegion(image: Images): TextureRegion = this.getRegion(image.imageName)

fun AssetStorage.createUiSkin(): Skin {
    val assetStorage = this
    Scene2DSkin.defaultSkin = skin(assetStorage[TextureAtlasAssets.UI]) { skin ->
        // fonts
        add(Font.SMALL.fontName, assetStorage[Font.SMALL])
        add(Font.REGULAR.fontName, assetStorage[Font.REGULAR])
        add(Font.LARGE.fontName, assetStorage[Font.LARGE])

        // default label style
        label { font = skin.getFont(Font.REGULAR.fontName) }

        /* // default textButton style
        textButton {
            down = skin[Images.BUTTON_RECT_DOWN]
            up = skin[Images.BUTTON_RECT_UP]
            font = skin.getFont(FontType.DEFAULT.skinKey)
        }

        // checkbox
        checkBox {
            checkboxOn = skin[Images.BUTTON_CHECK]
            checkboxOff = skin[Images.BUTTON_UNCHECK]
            font = skin.getFont(FontType.DEFAULT.skinKey)
        }

        // image button
        imageButton {
            down = skin[Images.BUTTON_ROUND_DOWN]
            up = skin[Images.BUTTON_ROUND_UP]
        }
        imageButton(ImageButtonStyles.ATTACK.name) {
            down = skin[Images.BUTTON_ROUND_DOWN]
            up = skin[Images.BUTTON_ROUND_UP]
            imageUp = skin[Images.IMAGE_ATTACK]
            imageDown = imageUp
        }
        imageButton(ImageButtonStyles.JUMP.name) {
            down = skin[Images.BUTTON_ROUND_DOWN]
            up = skin[Images.BUTTON_ROUND_UP]
            imageUp = skin[Images.IMAGE_JUMP]
            imageDown = imageUp
        }
        imageButton(ImageButtonStyles.FIREBALL.name) {
            down = skin[Images.BUTTON_ROUND_DOWN]
            up = skin[Images.BUTTON_ROUND_UP]
            imageUp = skin[Images.IMAGE_FIREBALL]
            imageDown = imageUp
        }

        // default touchpad style
        touchpad {
            knob = skin[Images.KNOB]
            background = skin[Images.TOUCHPAD]
        }

        // default scroll pane
        scrollPane {
            vScrollKnob = skin[Images.SCROLL_KNOB]
            vScroll = skin[Images.VSCROLL]
        }*/
    }
    return Scene2DSkin.defaultSkin
}

fun AssetStorage.createGameTextureAtlas(): TextureAtlas {
    return this[TextureAtlasAssets.GAME_TEXTURE]
}
