package com.silentgames.core.utils

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.utils.I18NBundle
import ktx.assets.async.AssetStorage
import ktx.freetype.async.loadFreeTypeFont
import kotlin.math.min

// texture atlas
enum class TextureAtlasAssets(val filePath: String) {
    GAME_TEXTURE("atlas/game.atlas"),
    UI("atlas/ui.atlas")
}

fun AssetStorage.loadAsync(asset: TextureAtlasAssets) = loadAsync<TextureAtlas>(asset.filePath)
operator fun AssetStorage.get(asset: TextureAtlasAssets) = get<TextureAtlas>(asset.filePath)

// tiled map
enum class MapAssets(val filePath: String) {
    INTRO("map/intro.tmx"),
    MAP_1("map/map1.tmx"),
    MAP_2("map/map2.tmx"),
    TEST_MAP("map/testmap.tmx"),
    MAIN_MENU("map/menuMap.tmx"),
    GAME_OVER("map/gameover.tmx")
}

fun AssetStorage.loadAsync(asset: MapAssets) = loadAsync<TiledMap>(asset.filePath)
operator fun AssetStorage.get(asset: MapAssets) = get<TiledMap>(asset.filePath)

enum class I18nAssets(val filePath: String) {
    DEFAULT("ui/i18n")
}

fun AssetStorage.loadAsync(asset: I18nAssets) = loadAsync<I18NBundle>(asset.filePath)
operator fun AssetStorage.get(asset: I18nAssets) = get<I18NBundle>(asset.filePath)

enum class Font(val fontName: String, val fontFileName: String) {

    SMALL("small-font", "fonts/Roboto-Bold.ttf") {
        override val freeTypeFontParameter: FreeTypeFontGenerator.FreeTypeFontParameter.() -> Unit =
            {
                characters = fontChars
                color = Color.WHITE
                borderColor = Color.valueOf("ffffff80")
                borderWidth = .5f
                size = dimension / 46
            }
    },
    LARGE("large-font", "fonts/Merriweather-Bold.ttf") {
        override val freeTypeFontParameter: FreeTypeFontGenerator.FreeTypeFontParameter.() -> Unit =
            {
                characters = fontChars
                color = Color.WHITE
                borderColor = Color.valueOf("00000080")
                borderWidth = 4f
                size = dimension / 26
            }
    },
    REGULAR("regular-font", "fonts/Roboto-Bold.ttf") {
        override val freeTypeFontParameter: FreeTypeFontGenerator.FreeTypeFontParameter.() -> Unit =
            {
                characters = fontChars
                color = Color.WHITE
                borderColor = Color.valueOf("ffffff80")
                borderWidth = 1f
                shadowColor = Color.valueOf("00000080")
                shadowOffsetX = 3
                shadowOffsetY = 3
                size = dimension / 40
            }
    };

    companion object {
        private const val fontChars =
            "абвгдеёжзийклмнопрстуфхцчшщъыьэюяabcdefghijklmnopqrstuvwxyzАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@|\\/?-+±=()*&.;:,{}\"´`'<>"
    }

    val dimension: Int = min(Gdx.graphics.width, Gdx.graphics.height)

    abstract val freeTypeFontParameter: FreeTypeFontGenerator.FreeTypeFontParameter.() -> Unit
}

suspend fun AssetStorage.loadAsync(font: Font) =
    loadFreeTypeFont(font.fontFileName, font.freeTypeFontParameter)

operator fun AssetStorage.get(font: Font) = get<BitmapFont>(font.fontFileName)
