package com.silentgames.core.utils

import com.artemis.Aspect
import com.artemis.BaseEntitySystem
import com.artemis.Entity
import com.badlogic.gdx.utils.Array
import ktx.collections.gdxArrayOf

abstract class SortedIteratingSystem(
    aspect: Aspect.Builder,
    private val comparator: Comparator<Entity>
) : BaseEntitySystem(aspect) {

    private val sortedEntities: Array<Entity> = gdxArrayOf()
    private var shouldSort = true

    fun forceSort() {
        shouldSort = true
    }

    private fun sort() {
        if (shouldSort) {
            sortedEntities.sort(comparator)
            shouldSort = false
        }
    }

    override fun processSystem() {
        sort()
        for (i in 0 until sortedEntities.size) {
            processEntity(sortedEntities[i], world.delta)
        }
    }

    protected abstract fun processEntity(entity: Entity, deltaTime: Float)

    override fun inserted(entityId: Int) {
        sortedEntities.add(world.getEntity(entityId))
        shouldSort = true
    }

    override fun removed(entityId: Int) {
        sortedEntities.removeValue(world.getEntity(entityId), true)
        shouldSort = true
    }

    override fun dispose() {
        sortedEntities.clear()
        shouldSort = false
    }
}
