/**
MIT License

Copyright (c) 2016 Shaun Reich <sreich02@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

@file:Suppress("NOTHING_TO_INLINE")

package com.silentgames.core.utils

import com.artemis.*
import com.artemis.utils.Bag
import com.artemis.utils.IntBag
import ktx.collections.GdxArray
import ktx.collections.gdxArrayOf
import kotlin.reflect.KClass

fun IntBag.toMutableList(): MutableList<Int> {
    val list = mutableListOf<Int>()
    this.forEach { list.add(it) }
    return list
}

val IntBag.indices: IntRange get() = 0 until size()
val <T : Any> Bag<T>.indices: IntRange get() = 0 until size()

inline fun IntBag.forEach(action: (Int) -> Unit) {
    for (i in indices) action(this.get(i))
}

inline fun IntBag.forEachIndex(action: (Int) -> Unit) {
    for (i in indices) action(i)
}

inline fun <R> IntBag.map(transform: (Int) -> R): GdxArray<R> {
    gdxArrayOf<Int>().map(transform)
    return mapTo(GdxArray(this.size()), transform)
}

inline fun <R> IntBag.mapNotNull(transform: (Int) -> R): GdxArray<R> {
    gdxArrayOf<Int>().mapNotNull(transform)
    return mapTo(GdxArray(this.size()), transform)
}

inline fun <R, C : GdxArray<in R>> IntBag.mapTo(destination: C, transform: (Int) -> R): C {
    this.forEach {
        destination.add(transform(it))
    }
    return destination
}

fun BaseSystem.getWorld(): World? {
    return try {
        val worldField = BaseSystem::class.java.getDeclaredField("world")
        worldField.isAccessible = true
        worldField.get(this) as World
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }
}

val BaseEntitySystem.entities: GdxArray<Entity>
    get() {
        val world = getWorld() ?: return gdxArrayOf()
        return this.entityIds.map { world.getEntity(it) }
    }

fun World.getEntitiesFor(builder: Aspect.Builder): GdxArray<Entity> {
    return aspectSubscriptionManager.get(builder).entities.mapNotNull { getEntity(it) }
}

fun EntitySubscription.getEntitiesFor(world: World): GdxArray<Entity> {
    return entities.map { world.getEntity(it) }
}

inline fun <reified C : Component> Entity.getComponentLazy(create: () -> C): C {
    var lazyComponent = this.getComponent(C::class.java)
    if (lazyComponent == null) {
        lazyComponent = create.invoke()
        this.edit().add(lazyComponent)
    }
    return lazyComponent
}

val Entity.components get() = this.getComponents(Bag())

fun Entity.add(component: Component) = this.edit().add(component)

inline fun <reified T : Component> Entity.remove() = this.edit().remove(T::class.java)
fun <T : Component> Entity.remove(type: KClass<T>) = this.edit().remove(type.java)
fun Entity.remove(component: Component) = this.edit().remove(component)

inline fun <reified T : Component> Entity.has(): Boolean = getComponent<T>() != null
fun <T : Component> Entity.has(type: KClass<T>): Boolean = getComponent(type) != null

inline fun <reified T : Component> Entity.hasNot(): Boolean = !has<T>()
fun <T : Component> Entity.hasNot(type: KClass<T>): Boolean = !has(type)

inline fun <reified T : Component> Entity.getComponent(): T? = getComponent(T::class.java)
fun <T : Component> Entity.getComponent(type: KClass<T>): T? = getComponent(type.java)

fun allOf(vararg components: KClass<out Component>): Aspect.Builder =
    Aspect.all(*toJavaClassArray(components))

fun Aspect.Builder.allOf(vararg components: KClass<out Component>): Aspect.Builder =
    this.all(*toJavaClassArray(components))

fun oneOf(vararg components: KClass<out Component>): Aspect.Builder =
    Aspect.one(*toJavaClassArray(components))

fun Aspect.Builder.oneOf(vararg components: KClass<out Component>): Aspect.Builder =
    this.one(*toJavaClassArray(components))

fun excludeOf(vararg components: KClass<out Component>): Aspect.Builder =
    Aspect.exclude(*toJavaClassArray(components))

fun Aspect.Builder.excludeOf(vararg components: KClass<out Component>): Aspect.Builder =
    this.exclude(*toJavaClassArray(components))

private fun toJavaClassArray(components: Array<out KClass<out Component>>): Array<Class<out Component>> =
    Array(components.size) { index -> components[index].java }

inline fun <reified T : BaseSystem> World.setSystemEnabled(value: Boolean) {
    getSystem(T::class.java).isEnabled = value
}
