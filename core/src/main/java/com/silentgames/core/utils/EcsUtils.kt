package com.silentgames.core.utils // ktlint-disable filename

import com.artemis.BaseSystem
import com.artemis.WorldConfigurationBuilder
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.silentgames.core.ecs.system.render.batch.BatchRenderSystem

fun WorldConfigurationBuilder.withBatchRenderSystems(
    batch: Batch,
    camera: Camera,
    vararg renderSystems: BatchRenderSystem
) {
    renderSystems.forEach { system ->
        if (system is BaseSystem) {
            system.onUpdateBegin = {
                if (renderSystems.first { it is BaseSystem && it.isEnabled } == system) {
                    batch.color = Color.WHITE
                    camera.update()
                    batch.projectionMatrix = camera.combined
                    batch.begin()
                }
            }
            system.onUpdateEnd = {
                if (renderSystems.last { it is BaseSystem && it.isEnabled } == system) {
                    batch.end()
                }
            }

            this.with(system)
        } else {
            throw IllegalStateException("BatchRenderSystem: $system  must be is EntitySystem")
        }
    }
}
