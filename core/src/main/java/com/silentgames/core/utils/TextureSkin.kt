package com.silentgames.core.utils

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.utils.Array

enum class GameTexture(val id: String) {
    DOOR("door"),
    WALL("wall"),
    ROTE("route"),
    BACKGROUND("background"),
    FOREGROUND("foreground"),
    SPACE_SHIP_FLOOR("space_ship_floor"),
    CRACKS("сracks"),
    FOOD("food"),
    KITCHEN_STOVE("kitchen_stove"),

    // Credits https://opengameart.org/users/funwithpixels
    ASTEROID_V_1("resources_asteroid_v1"),
    ASTEROID_V_2("resources_asteroid_v2"),
    CRYSTAL_RED("resources_red_crysta_cluster"),
    CRYSTAL_AQUA("resources_aqua_crystal_rock"),

    // No license need replace
    UNIT("unit_front"),
    STAIRS("stairs"),
    SHOOT_GUN("shoot_gun"),

    // DEBUG
    PATH_DEBUG("path_debug"),
}

fun TextureAtlas.getGameTextureRegions(gameTexture: GameTexture): Array<TextureAtlas.AtlasRegion> =
    this.findRegions(gameTexture.id)

fun TextureAtlas.getGameTextureRegion(gameTexture: GameTexture): TextureRegion =
    this.findRegion(gameTexture.id)

fun TextureAtlas.getGameTextureSprite(gameTexture: GameTexture): Sprite =
    this.createSprite(gameTexture.id)
