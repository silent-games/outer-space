package com.silentgames.core.utils

import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow

fun <T> flow(type: FlowType): KotlinFlow<T> =
    when (type) {
        FlowType.LISTENER -> object : KotlinFlow<T> {
            val mutableSharedFlow = MutableSharedFlow<T>(
                extraBufferCapacity = 1,
                onBufferOverflow = BufferOverflow.DROP_OLDEST
            )

            override fun observe(): Flow<T> = mutableSharedFlow.asSharedFlow()
            override fun emit(event: T) {
                mutableSharedFlow.tryEmit(event)
            }
        }
        FlowType.REPLAY -> object : KotlinFlow<T> {
            val mutableSharedFlow = MutableSharedFlow<T>(
                replay = 1,
                onBufferOverflow = BufferOverflow.DROP_OLDEST
            )

            override fun observe(): Flow<T> = mutableSharedFlow.asSharedFlow()
            override fun emit(event: T) {
                mutableSharedFlow.tryEmit(event)
            }
        }
        FlowType.EVENT -> object : KotlinFlow<T> {
            val mutableSharedFlow = MutableSharedFlow<Event<T>>(
                replay = 1,
                onBufferOverflow = BufferOverflow.DROP_OLDEST
            )

            override fun observe(): Flow<T> = mutableSharedFlow.observeIfNotHandled()
            override fun emit(event: T) {
                mutableSharedFlow.tryEmit(Event(event))
            }
        }
    }

enum class FlowType {
    /*
    Don't store events, emit after subscribe in real time
     */
    LISTENER,

    /*
    Store last event, emit on subscribe
     */
    REPLAY,

    /*
    Store last event, emit if not handled
     */
    EVENT
}

interface KotlinFlow<T> {

    fun observe(): Flow<T>

    fun emit(event: T)
}

fun KotlinFlow<Unit>.call() = emit(Unit)
