package com.silentgames.core.utils

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapNotNull

open class Event<out T>(private val content: T) {

    var hasBeenHandled = false
        private set // Allow external read but not write

    /**
     * Returns the content and prevents its use again.
     */
    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}

class EmptyEvent : Event<Any>(Any())

fun <T> Flow<Event<T>>.observeIfNotHandled() = this.mapNotNull { it.getContentIfNotHandled() }
