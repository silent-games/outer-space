package com.silentgames.core.utils

import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.TextureData
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.scenes.scene2d.Stage
import com.silentgames.core.ecs.model.GamePosition
import com.silentgames.core.ecs.model.Node
import ktx.math.ImmutableVector2

fun Vector3.toVector2() = Vector2(x, y)

fun Vector3.toImmutableVector2() = ImmutableVector2(x, y)

fun GamePosition.toNode() = Node(vector.x.toInt(), vector.y.toInt(), levelNumber)

fun Node.toVector2(): Vector2 = Vector2(x.toFloat(), y.toFloat())

fun Node.toVector3() = Vector3(x.toFloat(), y.toFloat(), z.toFloat())

fun Stage.resize(width: Int, height: Int, centerCamera: Boolean) {
    viewport.update(width, height, centerCamera)
    render()
}

fun <T> List<T>.has(predicate: (T) -> Boolean) = this.find(predicate) != null

fun Stage.render() {
    act()
    draw()
}

fun Node.getAroundOnLevelPositionList(totalX: Int, totalY: Int): List<Node> {
    return getAroundOnLevelPositionList().filter { it.x < totalX && it.y < totalY }
}

fun Node.getAroundOnLevelPositionList(): List<Node> {
    val positionList = mutableListOf<Node>()
    for (i in 0..2) {
        for (j in 0..2) {
            val x = this.x + j - 1
            val y = this.y + i - 1
            if ((
                x != this.x ||
                    y != this.y
                ) &&
                x >= 0 &&
                y >= 0
            ) {
                positionList.add(Node(x, y, this.z))
            }
        }
    }
    return positionList
}

fun TextureRegion.extractPixmap(): Pixmap {
    val textureData: TextureData = this.texture.textureData
    if (!textureData.isPrepared) {
        textureData.prepare()
    }
    val pixmap = Pixmap(
        this.regionWidth,
        this.regionHeight,
        textureData.format
    )
    pixmap.drawPixmap(
        textureData.consumePixmap(), // The other Pixmap
        0, // The target x-coordinate (top left corner)
        0, // The target y-coordinate (top left corner)
        this.regionX, // The source x-coordinate (top left corner)
        this.regionY, // The source y-coordinate (top left corner)
        this.regionWidth, // The width of the area from the other Pixmap in pixels
        this.regionHeight // The height of the area from the other Pixmap in pixels
    )
    return pixmap
}

fun TextureRegion.extractPixmap(scale: Float): Pixmap = extractPixmap(scale, scale)

fun TextureRegion.extractPixmap(scaleX: Float, scaleY: Float): Pixmap {
    val newmap = extractPixmap()
    val scaled = Pixmap(
        (scaleX * this.regionWidth).toInt(),
        (scaleY * this.regionHeight).toInt(),
        newmap.format
    )

    scaled.filter = Pixmap.Filter.NearestNeighbour

    val x = newmap.width - scaled.width
    val y = newmap.height - scaled.height

    scaled.drawPixmap(newmap, 0, 0, newmap.width, newmap.height, 0, 0, scaled.width, scaled.height)

    val result = newmap.apply {
        fill()
        filter = Pixmap.Filter.NearestNeighbour
        drawPixmap(
            scaled,
            0,
            0,
            scaled.width,
            scaled.height,
            x / 2,
            y / 2,
            scaled.width,
            scaled.height
        )
    }

    scaled.dispose()

    return result
}
