buildscript {
    repositories {
        gradlePluginPortal()
        mavenLocal()
        mavenCentral()
        maven(url = uri("https://plugins.gradle.org/m2/"))
        maven(url = uri("https://oss.sonatype.org/content/repositories/snapshots/"))
        maven(url = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
        google()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:7.3.1")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${properties["version.kotlin"]}")
        classpath("com.mobidevelop.robovm:robovm-gradle-plugin:${properties["version.roboVM"]}")
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        gradlePluginPortal()
        mavenLocal()
        mavenCentral()
        google()
        maven(url = uri("https://oss.sonatype.org/content/repositories/snapshots/"))
        maven(url = uri("https://oss.sonatype.org/content/repositories/releases/"))
        maven(url = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
        maven(url = uri("https://jitpack.io"))
    }
}
