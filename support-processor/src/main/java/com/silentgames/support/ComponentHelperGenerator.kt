package com.silentgames.support

import com.artemis.ComponentMapper
import com.artemis.Entity
import com.artemis.World
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import java.util.*
import javax.lang.model.element.Element
import javax.lang.model.element.TypeElement

object ComponentHelperGenerator {
    fun generate(packageName: String, element: TypeElement): FileSpec {
        return FileSpec.builder(packageName, element.simpleName.toString() + "Helper")
            .addImport("com.artemis", "World", "Entity", "ComponentMapper")
            .addProperty(element.getComponentMapperProperty())
            .addProperty(element.getEntityComponentProperty())
            .addProperty(element.getComponentPropertyOrNull())
            .build()
    }

    private fun Element.getComponentMapperProperty(): PropertySpec {
        val mapper = ComponentMapper::class.asClassName().parameterizedBy(classType)
        return PropertySpec.builder(propertyNameFromClassName + "Mapper", mapper)
            .receiver(World::class)
            .getter(
                FunSpec.getterBuilder()
                    .addStatement("return this.getMapper(${this.simpleName}::class.java)")
                    .build()
            )
            .build()
    }

    private fun Element.getEntityComponentProperty(): PropertySpec {
        return PropertySpec.builder(propertyNameFromClassName, classType)
            .receiver(Entity::class)
            .getter(
                FunSpec.getterBuilder()
                    .addStatement(
                        "return this.getComponent(${this.simpleName}::class.java) ?: throw KotlinNullPointerException(%S)",
                        exceptionText
                    )
                    .build()
            )
            .build()
    }

    private fun Element.getComponentPropertyOrNull(): PropertySpec {
        return PropertySpec.builder(
            propertyNameFromClassName + "OrNull",
            classType.copy(nullable = true)
        )
            .receiver(Entity::class)
            .getter(
                FunSpec.getterBuilder()
                    .addStatement("return this.getComponent(${this.simpleName}::class.java)")
                    .build()
            )
            .build()
    }

    private val Element.propertyNameFromClassName
        get() = this.simpleName.toString().decapitalize(Locale.ROOT)

    @Suppress("DEPRECATION")
    private val Element.classType
        get() = this.asType().asTypeName()

    private val Element.exceptionText get() = "Trying to access a ${this.simpleName} which is null"
}
