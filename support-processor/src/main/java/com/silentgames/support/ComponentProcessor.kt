package com.silentgames.support

import com.google.auto.service.AutoService
import java.io.File
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.Processor
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.TypeElement
import javax.lang.model.util.Elements

@AutoService(Processor::class)
class ComponentProcessor : AbstractProcessor() {

    override fun getSupportedAnnotationTypes(): Set<String> =
        setOf(EcsComponent::class.java.canonicalName)

    override fun getSupportedSourceVersion(): SourceVersion = SourceVersion.latestSupported()

    override fun process(p0: MutableSet<out TypeElement>, roundEnv: RoundEnvironment): Boolean {
        generateComponentHelperClass(
            roundEnv.getElementsAnnotatedWith(EcsComponent::class.java)
        )
        return true
    }

    private fun generateComponentHelperClass(typeElements: MutableSet<out Element>) {
        if (typeElements.isEmpty()) {
            return
        }
        typeElements.forEach {
            val packageName = packageName(processingEnv.elementUtils, it)
            val file = ComponentHelperGenerator.generate(packageName, it as TypeElement)
            val options = processingEnv.options
            val generatedPath = options["kapt.kotlin.generated"] ?: ""
            val path = generatedPath.replace(
                "(.*)tmp(/kapt/debug/)kotlinGenerated".toRegex(),
                "$1generated/source$2"
            )
            file.writeTo(File(path))
        }
    }

    private fun packageName(elementUtils: Elements, typeElement: Element): String {
        val pkg = elementUtils.getPackageOf(typeElement)
        if (pkg.isUnnamed) {
            throw Exception(typeElement.simpleName.toString())
        }
        return pkg.qualifiedName.toString()
    }
}
